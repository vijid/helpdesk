﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.ModelBinding;


namespace HelpDesk.Engineer
{
    public partial class TicketStatusReport : System.Web.UI.Page
    {
        Int32 userId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] != null)
            {
                userId = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "engineer")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Engineer/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Engineer/Default.aspx");
            }
            if (!IsPostBack)
            {
                getstatus();
            }
        }

        ithelpdeskEntities db = new ithelpdeskEntities();

        public void getstatus()
        {
            try
            {
                var status = db.StatusTables.Where(x => x.IsActive == true).ToList();

                StatusDDL.Items.Clear();
                StatusDDL.Items.Insert(0, new ListItem("All Ticket Details", "0"));
                StatusDDL.DataSource = status;
                StatusDDL.DataValueField = "StatusId";
                StatusDDL.DataTextField = "StatusName";
                StatusDDL.DataBind();
            }
            catch { }
        }
        public IEnumerable<TicketTable> gettickets([Control]int? StatusDDL)
        {
            userId = Convert.ToInt32(Session["UserId"]);
            var eng_det = db.EngineerTables.FirstOrDefault(x => x.EngineerId == userId);
            int unitid = Convert.ToInt32(eng_det.UnitId);

            if (StatusDDL == 0)
            {
                return db.TicketTables.Where(x => x.UsersTable.UnitId == unitid).OrderByDescending(x => x.TicketId).ToList();
            }
            else
            {
                return db.TicketTables.Where(x => x.UsersTable.UnitId == unitid && x.StatusId == StatusDDL).OrderByDescending(x => x.TicketId).ToList();
            }
        }

        protected void ViewTicketListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                HiddenField ticketid_hf = (HiddenField)e.Item.FindControl("hfticketid");
                Label StatusRemarksLbl = (Label)e.Item.FindControl("lblStatusRemark");
                Label createdate_lbl = (Label)e.Item.FindControl("lblcreatedate");
                Label closeddate_lbl = (Label)e.Item.FindControl("lblcloseddate");
                Label Duration_Label = (Label)e.Item.FindControl("durationLbl");
                Int32 ticketid = Convert.ToInt32(ticketid_hf.Value);

                DateTime startTime = Convert.ToDateTime(createdate_lbl.Text);
                DateTime endTime = DateTime.Now;
                if (closeddate_lbl.Text != "-")
                {
                    endTime = Convert.ToDateTime(closeddate_lbl.Text);
                }

                TimeSpan span = endTime.Subtract(startTime);
                string days = "";
                string hours = "";
                string mins = "";
                string sec = "";

                if (span.Days > 0)
                {
                    days = span.Days.ToString() + " Days ";
                }
                if (span.Hours > 0)
                {
                    hours = span.Hours.ToString() + " Hours ";
                }
                if (span.Minutes > 0)
                {
                    mins = span.Minutes.ToString() + " Minutes ";
                }
                if (span.Seconds > 0)
                {
                    sec = span.Seconds.ToString() + " Seconds ";
                }
                Duration_Label.Text = days + hours + mins + sec;

                var ticket_det = db.TicketTables.SingleOrDefault(x => x.TicketId == ticketid);
                if (ticket_det != null)
                {
                    StatusRemarksLbl.Text = ticket_det.StatusId == 0 ? ticket_det.ClosedDate == null ? "Pending" : "Closed" : ticket_det.TicketProcessTables.FirstOrDefault(x => x.TicketId == ticket_det.TicketId && x.StatusId == ticket_det.StatusId).Remarks;
                }
            }
            catch { }
        }
    }
}