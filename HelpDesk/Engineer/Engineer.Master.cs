﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Engineer
{
    public partial class Engineer : System.Web.UI.MasterPage
    {
        int enggid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] != null)
            {
                enggid = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "engineer")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Engineer/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Engineer/Default.aspx");
            }
        }

        protected void logoutBtn_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            Response.Redirect("~/Engineer/Default.aspx");
        }
    }
}