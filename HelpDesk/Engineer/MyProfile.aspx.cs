﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Engineer
{
    public partial class MyProfile : System.Web.UI.Page
    {
        int userid = 0;

        ithelpdeskEntities db = new ithelpdeskEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "engineer")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Engineer/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Engineer/Default.aspx");
            }

            if (!IsPostBack)
            {
                getengineerprofile();
            }
        }

        public void getengineerprofile()
        {
            var adengg_det = db.EngineerTables.FirstOrDefault(x => x.EngineerId == userid);
            txtengineername.Text = adengg_det.EngineerName.ToString();
            txtemailid.Text = adengg_det.EmailId.ToString();
            txtcontactno.Text = adengg_det.ContactNo.ToString();
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            string enggname = txtengineername.Text;
            string email = txtemailid.Text;
            string contactno = txtcontactno.Text;
            var adeng_del = db.EngineerTables.FirstOrDefault(x => x.EngineerId == userid);
            adeng_del.EngineerName = enggname;
            adeng_del.EmailId = email;
            adeng_del.ContactNo = contactno;

            var eng_det = db.AdminTables.FirstOrDefault(x => x.EngineerId == adeng_del.EngineerId);
            eng_det.AdminName = enggname;
            eng_det.EmailId = email;
            eng_det.ContactNo = contactno;

            db.SaveChanges();
            Clear();
            ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('My Profile Updated Successfully!!'); });", true);
            getengineerprofile();
        }

        public void Clear()
        {
            txtengineername.Text = "";
            txtemailid.Text = "";
            txtcontactno.Text = "";
        }
    }
}