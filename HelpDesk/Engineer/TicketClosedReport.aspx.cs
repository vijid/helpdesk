﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.ModelBinding;

namespace HelpDesk.Engineer
{
    public partial class TicketClosedReport : System.Web.UI.Page
    {
        Int32 userId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] != null)
            {
                userId = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "engineer")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Engineer/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Engineer/Default.aspx");
            }
            getticketdetails();
        }

        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<TicketTable> gettickets([Control]int? TickettypeDDL)
        {
            userId = Convert.ToInt32(Session["UserId"]);
            var eng_det = db.EngineerTables.FirstOrDefault(x => x.EngineerId == userId);
            int unitid = Convert.ToInt32(eng_det.UnitId);

            if (TickettypeDDL == 1)
            {
                return db.TicketTables.Where(x => x.UsersTable.UnitId == unitid && x.IsClosed == true && x.CreatedDate.Value.Date == x.ClosedDate.Value.Date).OrderByDescending(x => x.TicketId).ToList();
            }
            else if (TickettypeDDL == 2)
            {
                return db.TicketTables.Where(x => x.UsersTable.UnitId == unitid && x.IsClosed == true && x.CreatedDate.Value.Date.AddDays(2) == x.ClosedDate.Value.Date).OrderByDescending(x => x.TicketId).ToList();
            }
            else if (TickettypeDDL == 3)
            {
                return db.TicketTables.Where(x => x.UsersTable.UnitId == unitid && x.IsClosed == true && x.CreatedDate.Value.Date.AddDays(2) < x.ClosedDate.Value.Date).OrderByDescending(x => x.TicketId).ToList();
            }
            else
            {
                return db.TicketTables.Where(x => x.UsersTable.UnitId == unitid && x.IsClosed == true).OrderByDescending(x => x.TicketId).ToList();
            }
        }

        public void getticketdetails()
        {
            try
            {
              
                userId = Convert.ToInt32(Session["UserId"]);
                var eng_det = db.EngineerTables.FirstOrDefault(x => x.EngineerId == userId);
                int unitid = Convert.ToInt32(eng_det.UnitId);

                int ProblemType = Convert.ToInt32(TickettypeDDL.SelectedValue);

                var TicketDetails = (from tck in db.TicketTables
                                     where tck.UsersTable.UnitId == unitid && tck.IsClosed == true
                                     select new
                                     {
                                         ticketId = tck.TicketId,
                                         TicketNo = tck.TicketNo,
                                         UserName = tck.UsersTable.Name,
                                         PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                         AssetNo = tck.AssetTable.AssetNo,
                                         ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                         Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                         Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                         StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                         CreatedDate = tck.CreatedDate,
                                         AttendedDate = tck.AttendedDate,
                                         AttendedBy = tck.AttendedBy != 0 ? db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName : "-",
                                         ClosedDate = tck.ClosedDate
                                     }).OrderByDescending(x => x.ticketId).ToList();

                if (ProblemType == 1)
                {
                    TicketDetails = TicketDetails.Where(x => x.CreatedDate.Value.Date == x.ClosedDate.Value.Date).ToList();
                }
                else if (ProblemType == 2)
                {
                    TicketDetails = TicketDetails.Where(x => x.CreatedDate.Value.Date.AddDays(2) >= x.ClosedDate.Value.Date).ToList();
                }
                else if (ProblemType == 3)
                {
                    TicketDetails = TicketDetails.Where(x => x.CreatedDate.Value.Date.AddDays(2) < x.ClosedDate.Value.Date).ToList();
                }
                ViewTicketListView.DataSource = TicketDetails;
                ViewTicketListView.DataBind();
            }
            catch { }
        }

        protected void TickettypeDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                getticketdetails();
            }
            catch { }
        }

        protected void ViewTicketListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                Label createdate_lbl = (Label)e.Item.FindControl("lblcreatedate");
                Label closeddate_lbl = (Label)e.Item.FindControl("lblcloseddate");
                Label Duration_Label = (Label)e.Item.FindControl("durationLbl");
                DateTime startTime = Convert.ToDateTime(createdate_lbl.Text);
                DateTime endTime = DateTime.Now;
                if (closeddate_lbl.Text != "-")
                {
                    endTime = Convert.ToDateTime(closeddate_lbl.Text);
                }

                TimeSpan span = endTime.Subtract(startTime);
                //string year = "";
                //string month = "";
                string days = "";
                string hours = "";
                string mins = "";
                string sec = "";

                if (span.Days > 0)
                {
                    days = span.Days.ToString() + " Days ";
                }
                if (span.Hours > 0)
                {
                    hours = span.Hours.ToString() + " Hours ";
                }
                if (span.Minutes > 0)
                {
                    mins = span.Minutes.ToString() + " Minutes ";
                }
                if (span.Seconds > 0)
                {
                    sec = span.Seconds.ToString() + " Seconds ";
                }
                Duration_Label.Text = days + hours + mins + sec;
            }
            catch { }
        }
    }
}