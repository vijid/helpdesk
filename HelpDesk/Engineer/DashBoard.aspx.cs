﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace HelpDesk.Engineer
{
    public partial class DashBoard : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "engineer")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Engineer/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Engineer/Default.aspx");
            }
        }

        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<TicketTable> getTickets()
        {
            userid = Convert.ToInt32(Session["UserId"]);
            var eng_det = db.EngineerTables.FirstOrDefault(x => x.EngineerId == userid);
            int unitid = Convert.ToInt32(eng_det.UnitId);
            return db.TicketTables.Where(x => x.UsersTable.UnitId == unitid).OrderByDescending(x => x.TicketId).ThenBy(x=>x.TicketNo).ToList();
        }

        protected void TicketListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowView")
                {
                    int ticketid = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect("~/Engineer/EditingCalls.aspx?ticketid=" + ticketid + "");
                }
            }
            catch { }
        }

        protected void TicketListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                HiddenField ticketid_hf = (HiddenField)e.Item.FindControl("hfticketid");
                Label createdate_lbl = (Label)e.Item.FindControl("lblcreatedate");
                Label CreatedBy_lbl = (Label)e.Item.FindControl("lblAttendedBy");
                Label StatusRemarksLbl = (Label)e.Item.FindControl("lblStatusRemark");
                Label closeddate_lbl = (Label)e.Item.FindControl("lblcloseddate");
                Label Duration_Label = (Label)e.Item.FindControl("durationLbl");
                DateTime currentdate = DateTime.Now.Date;

                if (currentdate == Convert.ToDateTime(createdate_lbl.Text).Date)
                {
                    HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                    Cell.Style.Add("background-color", "#99CCFF");
                }
                if (currentdate.AddDays(-1) == Convert.ToDateTime(createdate_lbl.Text).Date)
                {
                    HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                    Cell.Style.Add("background-color", "#FF0000");
                }

                if (Convert.ToDateTime(createdate_lbl.Text).Date <= currentdate.AddDays(-2))
                {
                    HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                    Cell.Style.Add("background-color", "#FFD58A");
                }
                DateTime startTime = Convert.ToDateTime(createdate_lbl.Text);
                DateTime endTime = DateTime.Now;
                if (closeddate_lbl.Text != "-")
                {
                    endTime = Convert.ToDateTime(closeddate_lbl.Text);
                }

                TimeSpan span = endTime.Subtract(startTime);
                string days = "";
                string hours = "";
                string mins = "";
                string sec = "";

                if (span.Days > 0)
                {
                    days = span.Days.ToString() + " Days ";
                }
                if (span.Hours > 0)
                {
                    hours = span.Hours.ToString() + " Hours ";
                }
                if (span.Minutes > 0)
                {
                    mins = span.Minutes.ToString() + " Minutes ";
                }
                if (span.Seconds > 0)
                {
                    sec = span.Seconds.ToString() + " Seconds ";
                }
                Duration_Label.Text = days + hours + mins + sec;

                Int32 ticketid = Convert.ToInt32(ticketid_hf.Value);
                var ticket_det = db.TicketTables.SingleOrDefault(x => x.TicketId == ticketid);
                if (ticket_det != null)
                {
                    if (ticket_det.IsClosed != null)
                    {
                        if (ticket_det.IsClosed == true)
                        {
                            HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                            Cell.Style.Add("background-color", "#56c1bd");
                        }
                    }

                    CreatedBy_lbl.Text = ticket_det.AttendedBy != 0 ? (ticket_det.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == ticket_det.AttendedBy).EngineerName) : "-";
                    StatusRemarksLbl.Text = ticket_det.StatusId == 0 ? ticket_det.ClosedDate == null ? "Pending" : "Closed" : ticket_det.TicketProcessTables.FirstOrDefault(x => x.TicketId == ticket_det.TicketId && x.StatusId == ticket_det.StatusId).Remarks;
                }
            }
            catch { }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch { }
        }
    }
}