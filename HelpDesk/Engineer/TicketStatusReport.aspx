﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Engineer/Engineer.Master" AutoEventWireup="true" CodeBehind="TicketStatusReport.aspx.cs" Inherits="HelpDesk.Engineer.TicketStatusReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            $('#tb-responsive').DataTable({
                "sDom":
                '<"well well-sm"<"row"<"col-md-4 clearfix"l><"col-md-8 clearfix"fT>r>>' +
                't' +
                '<"row"<"col-md-5 clearfix"i><"col-md-7 clearfix"p>>',
                tableTools: {
                    "sSwfPath": "../../assets/lib/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
                }
            });
            $(document).ready(function () {
                $('table.display').dataTable();
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page_bar clearfix">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Ticket Status Reports</h1>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="Ticketlistupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Ticketlistprogress" AssociatedUpdatePanelID="Ticketlistupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="row form-horizontal">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Select Status</label>
                                <div class="col-xs-4">
                                    <asp:DropDownList ID="StatusDDL" data-placeholder="All Ticket Details" AutoPostBack="true"
                                        AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                        <asp:ListItem Value="0">All Ticket Details</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table id="tb-responsive" class="table">
                                        <thead>
                                            <tr>
                                                <th class="sub_col">S.No. </th>
                                                <th class="sub_col">User Name</th>
                                                <th class="sub_col">Part Name</th>
                                                <th class="sub_col">Problem Details</th>
                                                <th class="sub_col">Remarks</th>
                                                <th class="sub_col">Created Date</th>
                                                <th class="sub_col">Attended Date</th>
                                                <th class="sub_col">Closed Date</th>
                                                <th class="sub_col">Status</th>
                                                <th class="sub_col">Status Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:ListView ID="ViewTicketListView" runat="server" ItemType="HelpDesk.TicketTable" SelectMethod="gettickets" OnItemDataBound="ViewTicketListView_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="sub_col">
                                                            <%#:Container.DataItemIndex+1 %>
                                                            <asp:HiddenField ID="hfticketid" runat="server" Value='<%#:Item.TicketId %>' />
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#:Item.UsersTable.Name %>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#:Item.PartMasterId!=499?Item.ProblemMasterTable==null?"-":Item.PartMasterTable.PartName:Item.PartEntryText%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#:Item.ProblemId!=499?Item.ProblemMasterTable==null?"-":Item.ProblemMasterTable.Description:Item.ProblemText%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#:Item.Remarks %>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%--<%#:Convert.ToDateTime(Item.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt") %>--%>
                                                            <asp:Label ID="lblcreatedate" runat="server" Text='<%#:Convert.ToDateTime(Item.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt") %>'></asp:Label>
                                                        </td>
                                                        <td class="sub_col"><%#:Item.AttendedDate !=null?Convert.ToDateTime(Item.AttendedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-"%></td>
                                                        <td class="sub_col">
                                                            <%--<%#:Convert.ToDateTime(Item.ClosedDate).ToString("dd-MMM-yyyy hh:mm tt") %>--%>
                                                            <asp:Label ID="lblcloseddate" runat="server" Text='<%#:Item.ClosedDate !=null? Convert.ToDateTime(Item.ClosedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-"%>'></asp:Label>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#:Item.StatusId==0 ? Item.ClosedDate==null?"Pending":"Closed" : Item.StatusTable.StatusName %> By
                                                            <asp:Label ID="durationLbl" runat="server"></asp:Label>
                                                        </td>
                                                        <td class="sub_col">
                                                            <asp:Label ID="lblStatusRemark" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
