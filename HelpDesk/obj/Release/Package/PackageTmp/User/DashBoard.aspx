﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="HelpDesk.User.DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            $('#prod_table').DataTable({
                "sDom":
                    '<"well well-sm"<"row"<"col-md-4 clearfix"l><"col-md-8 clearfix"fT>r>>' +
                    't' +
                    '<"row"<"col-md-5 clearfix"i><"col-md-7 clearfix"p>>',
                tableTools: {
                    "sSwfPath": "../../assets/lib/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
                }
            });
            $(document).ready(function () {
                $('table.display').dataTable();
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageTicketupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageTicketprogress" AssociatedUpdatePanelID="ManageTicketupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="page_title">Manage Ticket</h1>
                    </div>
                    <div class="col-md-2 text-right">
                        <a class="btn btn-success" runat="server" id="CreateTck" href="~/User/TicketCreation.aspx">Create Ticket</a>
                    </div>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table info_table" id="prod_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Ticket No</th>
                                                <th class="sub_col">Part Name</th>
                                                <th class="sub_col">Asset No</th>
                                                <th class="sub_col">Problem Details</th>
                                                <th class="sub_col">Remarks</th>
                                                <th class="sub_col">Status</th>
                                                <th class="sub_col">Created Date</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="TicketListView" ItemType="HelpDesk.TicketTable" SelectMethod="getTicket" runat="server" OnItemCommand="TicketListView_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.TicketNo%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.PartMasterId!=499?Item.PartMasterTable.PartName:Item.PartEntryText%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.AssetId!=0? Item.AssetTable.AssetNo: "-"%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.ProblemId!=499?Item.ProblemMasterTable.Description:Item.ProblemText%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.Remarks%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.StatusId==0 ? "-" : Item.StatusTable.StatusName %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Convert.ToDateTime(Item.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt") %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%--<a href='~/User/ViewDetails.aspx?TickId="<%#:Item.TicketId%>"' id="ViewDetailsLnk" runat="server" class ="btn btn-inverse btn-sm"><span class="fa fa-pencil-square-o fa-lg"></span>View Details</a>--%>
                                                        <asp:LinkButton ID="lkbtnedit" Enabled='<%#Item.AttendedBy!=0?false:true %>' runat="server" class="btn btn-inverse btn-sm" CommandName="RowEdit" CommandArgument='<%#:Item.TicketId%>'><span class="fa fa-eye fa-lg"></span>View Details</asp:LinkButton>
                                                        <%--<asp:LinkButton ID="lkbtndelete" runat="server" class="btn btn-danger btn-sm" CommandName="RowDelete" CommandArgument='<%#:Item.AssetId%>' OnClientClick="javascript:return confirm('Are you sure you want to delete this record?')"><span class="fa fa-pencil-square-o fa-lg"></span>Delete</asp:LinkButton>--%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">

    <div class="modal fade" id="Edit_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>View Details</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="editTicketUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="EditUpdateProgress" AssociatedUpdatePanelID="editTicketUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="AssetNo">Asset No</label>
                                                <asp:Label ID="AssetNoLbl" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="PartType">Part Type</label>
                                                <asp:Label ID="PartTypeLbl" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="Recurring">ReSubmitted</label>
                                                <asp:Label ID="isRecurringLbl" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="CreatedType">Created Date</label>
                                                <asp:Label ID="CreatedDateLbl" runat="server" CssClass="form-control"></asp:Label>
                                            </div>

                                        </div>
                                        <div class="col-lg-6">

                                            <div class="form-group">
                                                <label for="problemType">Problem Type</label>
                                                <asp:Label ID="ProblemTypeLbl" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="Remarks">Remarks</label>
                                                <asp:Label ID="EditRemarkslbl" runat="server" aria-multiline="true" CssClass="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="AttendedBy">Attended By</label>
                                                <asp:Label ID="AttendedByLbl" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="AttendedDate">Attended Date</label>
                                                <asp:Label ID="AttendedDateLbl" runat="server" CssClass="form-control"></asp:Label>
                                            </div>
                                            <%-- <div class="form-group">
                                                <label for="ActionTaken">Action Taken</label>
                                                <asp:Label ID="ActionTakenLbl" runat="server" CssClass="form-control"></asp:Label>
                                            </div>  --%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <%--<asp:Button ID="updateBtn" CssClass="btn btn-success" runat="server" Text="Update" OnClick="updateBtn_Click" OnClientClick="return ValidationUpdate()" />--%>
                                        <input type="reset" class="btn btn-success" value="Ok" data-dismiss="modal" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
