﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="MyProfile.aspx.cs" Inherits="HelpDesk.User.MyProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function validate() {
            var Name = document.getElementById('<%=txtname.ClientID%>').value;
            var Designation = document.getElementById('<%=txtDesignation.ClientID%>').value;
            var Department = document.getElementById('<%=txtDepartment.ClientID%>').value;
            var emailId = document.getElementById('<%=txtEmailId.ClientID%>').value;
            if (Name == "" || Name == null || Designation == "" || Designation == null || emailId == "" || emailId == null || Password == "" || Password == null) {
                alert("Kindly Enter All Fields");
                return false;
            }

            var x = document.getElementById("<% = txtEmailId.ClientID %>").value;
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                alert("Kindly Enter valid Email Id");
                return false;
            }
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page_bar clearfix">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">My Profile</h1>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="col-lg-offset-3 col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <asp:UpdatePanel ID="adduserprofileUpd" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="prgAdd" runat="server" AssociatedUpdatePanelID="adduserprofileUpd">
                                    <ProgressTemplate>
                                        <div class="update"></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <div class="form-group">
                                    <label for="Name">Name</label>
                                    <asp:TextBox ID="txtname" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="Designation">Designation</label>
                                    <asp:TextBox ID="txtDesignation" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="Department">Department</label>
                                    <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="EmailId">EmailId</label>
                                    <asp:TextBox ID="txtEmailId" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:Button ID="updateBtn" CssClass="btn btn-success" runat="server" Text="Update" OnClientClick="return validate()" OnClick="updateBtn_Click" />
                                    <input type="reset" class="btn btn-default" value="Cancel" data-dismiss="modal" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
