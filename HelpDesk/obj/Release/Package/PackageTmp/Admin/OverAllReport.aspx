﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="OverAllReport.aspx.cs" Inherits="HelpDesk.Admin.OverAllReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%=ResolveClientUrl("~/assets/js/jquery.maskedinput.js")%>"></script>
    <script type="text/javascript">

        function Validation() {
            if (document.getElementById('<%=UnitDDl.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Unit Name");
                return false;
            }
            <%--if (document.getElementById('<%=TypeDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Type Name");
                return false;
            }--%>
            return true;
        }
        function pageLoad() {
            $('#dt_reportTable').DataTable({
                "sDom":
                    '<"well well-sm"<"row"<"col-md-4 clearfix"l><"col-md-8 clearfix"fT>r>>' +
                    't' +
                    '<"row"<"col-md-5 clearfix"i><"col-md-7 clearfix"p>>',
                tableTools: {
                    "sSwfPath": "../../assets/lib/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
                }
            });
            $(document).ready(function () {
                $('table.display').dataTable();
            });


            $(document).ready(
         function () {
             $('#<%=fromdateTimeTxt.ClientID %>').click(
                function () {
                    $("#<%=fromdateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
                });

             $('#<%=fromdateTimeTxt.ClientID %>').keydown(
              function (e) {
                  var code = e.keyCode || e.which;
                  if (code === 9) {
                      $("#<%=fromdateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
                      $("#<%=ToDateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
                  }
              });
         });
          $(document).ready(
     function () {
         $('#<%=ToDateTimeTxt.ClientID %>').click(
            function () {
                $("#<%=ToDateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
            });

         $('#<%=ToDateTimeTxt.ClientID %>').keydown(
              function (e) {
                  var code = e.keyCode || e.which;
                  if (code === 9) {
                      $("#<%=ToDateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
                  }
              });
     });
      }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page_bar clearfix">
        <div class="row">
            <div class="col-md-10">
                <h1 class="page_title">Consolidated Call Report</h1>
            </div>
            <div class="col-md-2 text-right">
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="Ticketlistupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Ticketlistprogress" AssociatedUpdatePanelID="Ticketlistupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div class="page_content">

                <div class="panel panel-default">
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <label>Select Unit Name</label>
                            <asp:DropDownList ID="UnitDDl" data-placeholder="Select Unit Name"
                                AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                <asp:ListItem Selected="True" Value="0">All Units</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <label>From Date Time(Ex: 01/01/2016 06:00)</label>
                            <asp:TextBox ID="fromdateTimeTxt" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy hh:mm"></asp:TextBox>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <label>To Date Time(Ex: 01/01/2016 23:00)</label>
                            <asp:TextBox ID="ToDateTimeTxt" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy hh:mm"></asp:TextBox>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <label>&nbsp;</label>
                            <div id="btn" class="Input-group">
                                <asp:Button ID="SearchButton" CssClass="btn btn-success" runat="server" Text="Search" OnClick="SearchButton_Click"/>
                                <asp:Button ID="CancelBtn" CssClass="btn btn-default" runat="server" Text="Cancel" OnClick="CancelBtn_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page_content">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="panel panel-default">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table id="dt_reportTable" class="table">
                                        <thead>
                                            <tr>
                                                <th class="sub_col">Reported Calls</th>
                                                <th class="sub_col">Complete & NotClosed Calls</th>
                                                <th class="sub_col">Complete & closed Calls</th>
                                                <th class="sub_col">Pending Calls</th>
                                                <th class="sub_col">UnAttended Calls</th>
                                                <th class="sub_col">Re-Occur InProgress</th>
                                                <th class="sub_col">Re-Occur Completed</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:ListView ID="ViewStatusListView" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="sub_col">
                                                            <%#Eval("ReportedCalls")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("CompletedCalls")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("Completeclosed")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("PendingCalls")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("UnAttendedCalls")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("ReOccurInProgress")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("ReOccurCompleted")%>
                                                        </td>                                                        
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
