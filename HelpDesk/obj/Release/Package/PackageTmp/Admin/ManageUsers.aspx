﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ManageUsers.aspx.cs" Inherits="HelpDesk.Admin.ManageUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            $(document).ready(function () {
                $('#prod_table').DataTable();
            });
        }

    </script>
    <script type="text/javascript">
        function Validate() {
            var name = document.getElementById('<%=EditNameTxt.ClientID%>').value;
              var Designation = document.getElementById('<%=EditDesignationTxt.ClientID%>').value;
              var Dept = document.getElementById('<%=EditDeptTxt.ClientID%>').value;
            var email = document.getElementById('<%=EditEmailTxt.ClientID%>').value;
            var username = document.getElementById('<%=EditUserNametxt.ClientID%>').value;
              var password1 = document.getElementById('<%=EditPasswordTxt.ClientID%>').value;
              var CompAssetno = document.getElementById('<%=EditAssetNoTxt.ClientID%>').value;
              var PrinterAssetNo = document.getElementById('<%=EditPrinterNoTxt.ClientID%>').value;
              if (name == '' && Designation == '' && Dept == '' && email == '' && username == '' && password1 == '' && password2 == '' && CompAssetno == '' && PrinterAssetNo == '') {
                  alert('Kindly Enter All Fields');
                  return false;
              }
              if (name == '') {
                  alert('Kindly Enter Name');
                  return false;
              }
              if (Designation == '') {
                  alert('Kindly Enter Designation');
                  return false;
              }
              if (Dept == '') {
                  alert('Kindly Enter Department');
                  return false;
              }

              if (document.getElementById('<%=UnitNameDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Unit Name");
                return false;
            }
            if (CompAssetno == '') {
                alert('Kindly Enter Computer Asset No');
                return false;
            }
            if (document.getElementById('<%=PrinterDDL.ClientID%>').selectedIndex == -1) {
                alert("Kindly Select Printer Name");
                return false;
            }
            if (username == "")
            {
                alert("Kindly Enter the Username")
            }
            if (email == '') {
                alert('Kindly Enter Email Id');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageUserupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageUserprogress" AssociatedUpdatePanelID="ManageUserupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="page_title">Manage User</h1>
                    </div>
                    <%--<div class="col-md-2 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#bs_modal_regular">Add Asset</a>
                    </div>--%>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">Select Unit Name</label>
                                        <div class="col-xs-4">
                                            <asp:DropDownList ID="UnitDDl" data-placeholder="Select Unit Name" AutoPostBack="true"
                                                AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                <asp:ListItem Value="-1">All Unit</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table info_table" id="prod_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Name</th>
                                                <th class="sub_col">Designation</th>
                                                <th class="sub_col">Department</th>
                                                <th class="sub_col">Email Id</th>
                                                <th class="sub_col">User Name</th>
                                                <th class="sub_col">Password</th>
                                                <th class="sub_col">Unit Name</th>
                                                <th class="sub_col">Comp.Asset No</th>
                                                <th class="sub_col">Printer No</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="UserListView" ItemType="HelpDesk.UsersTable" SelectMethod="getUsers" runat="server"
                                            OnItemDataBound="UserListView_ItemDataBound" OnItemCommand="UserListView_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.Name%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.Designation%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.Department%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.EmailId!=null?Item.EmailId:"-"%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.UserName %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:Label ID="PasswordLbl" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="PasswordHdn" runat="server" Value="<%#:Item.UserId %>" />
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.UnitTable.UnitName %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.AssetNo%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.PrinterAssetNo%>
                                                    </td>
                                                    <td class="sub_col" style="width:150px;">
                                                        <asp:LinkButton ID="lkbedit" runat="server" class="btn btn-inverse btn-sm" CommandName="RowUpdate" CommandArgument='<%#:Item.UserId %>'><span class="fa fa-pencil-square-o fa-lg"></span>Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtnedit" runat="server" class="btn btn-danger btn-sm" CommandName="RowEdit" CommandArgument='<%#:Item.UserId%>'><span class="fa fa-unlock-alt fa-lg"></span><%#:Item.IsActive == true?"Disable":"Enable" %></asp:LinkButton>
                                                        <%--<asp:LinkButton ID="lkbtndelete" runat="server" class="btn btn-danger btn-sm" CommandName="RowDelete" CommandArgument='<%#:Item.UserId%>' OnClientClick="javascript:return confirm('Are you sure you want to delete this record?')"><span class="fa fa-pencil-square-o fa-lg"></span>Delete</asp:LinkButton>--%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
    <div class="modal fade" id="Edit_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Edit Unit Details</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="editUnitUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="editUnitUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="register_name">Name</label>
                                                <asp:TextBox ID="EditNameTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_designation">Designation</label>
                                                <asp:TextBox ID="EditDesignationTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_dept">Department</label>
                                                <asp:TextBox ID="EditDeptTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_name">Unit</label>
                                                <asp:DropDownList ID="UnitNameDDL" data-placeholder="Select An Unit" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Selected="True" Text="Select An Unit" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_name">Computer Asset No.</label>
                                                <asp:TextBox ID="EditAssetNoTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 p-r-0">
                                            <div class="form-group">
                                                <label for="register_name">Printer Type</label>
                                                <asp:DropDownList ID="PrinterDDL" data-placeholder="Select A Printer" AppendDataBoundItems="true"
                                                    AutoPostBack="true" OnSelectedIndexChanged="PrinterDDL_SelectedIndexChanged"
                                                    CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Selected="True" Text="Select A Printer" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_name">Printer Asset No</label>
                                                <asp:TextBox ID="EditPrinterNoTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_email">Email Id</label>
                                                <asp:TextBox ID="EditEmailTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                                <asp:HiddenField ID="userhidden" runat="server" />
                                            </div>
                                            <div class="form-group">
                                                <label for="register_UserName">User name</label>
                                                <asp:TextBox ID="EditUserNametxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label for="register_password">Password</label>
                                                <asp:TextBox ID="EditPasswordTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="updateBtn" CssClass="btn btn-success" runat="server" Text="Update" OnClick="updateBtn_Click" OnClientClick="return Validate()" />
                                        <input type="reset" class="btn btn-default" value="Cancel" data-dismiss="modal" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
