﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ManageUnitMaster.aspx.cs" Inherits="HelpDesk.Admin.ManageUnitMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function pageLoad() {
            $(document).ready(function () {
                $('#prod_table').DataTable();
            });
        }
    </script>

    <script type="text/javascript">
        function Validation() {
            var unitname = document.getElementById('<%=UnitNameText.ClientID%>').value;
            var address = document.getElementById('<%=UnitAddresstext.ClientID%>').value;
            if (unitname == '' && address == '') {
                alert('Kindly Enter All Fields');
                return false;
            }
            if (unitname == '') {
                alert('Kindly Enter Unit Name');
                return false;
            }

            if (address == '') {
                alert('Kindly Enter Unit Address');
                return false;
            }
            return true;
        }
    </script>
    <script type="text/javascript">
        function ValidationUpdate() {
            var unitname = document.getElementById('<%=EditUnitText.ClientID%>').value;
            var address = document.getElementById('<%=EditUnitAddressText.ClientID%>').value;
            if (unitname == '' && address == '') {
                alert('Kindly Enter All Fields');
                return false;
            }
            if (unitname == '') {
                alert('Kindly Enter Unit Name');
                return false;
            }

            if (address == '') {
                alert('Kindly Enter Unit Address');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageUnitUpdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageUnitprogress" AssociatedUpdatePanelID="ManageUnitUpdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="page_title">Manage Unit</h1>
                    </div>
                    <div class="col-md-2 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#bs_modal_regular">Add Unit Details</a>
                    </div>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table info_table" id="prod_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Unit Name</th>
                                                <th class="sub_col">Unit Address</th>
                                                <th class="sub_col">IsActive</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="UnitListView" ItemType="HelpDesk.UnitTable" SelectMethod="GetUnit" runat="server" OnItemCommand="UnitListView_ItemCommand" OnItemDataBound="UnitListView_ItemDataBound">
                                            <ItemTemplate>
                                                <tr id="RowVal" runat="server">
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                        <asp:HiddenField ID="hfunitid" runat="server" Value='<%#:Item.UnitId %>' />
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.UnitName%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.UnitAddress%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Convert.ToBoolean(Item.IsActive) ==true?"True":"False" %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:LinkButton ID="lkbtnedit" runat="server" class="btn btn-inverse btn-sm" CommandName="RowEdit" CommandArgument='<%#:Item.UnitId%>'><span class="fa fa-pencil-square-o fa-lg"></span>Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtndelete" runat="server" class="btn btn-danger btn-sm" CommandName="RowDelete" CommandArgument='<%#:Item.UnitId%>'><span class="fa fa-unlock-alt fa-lg"></span><%#:Item.IsActive == true?"Disable":"Enable" %></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
    <div class="modal fade" id="bs_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Add Unit Details</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="AddUnitUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="AddUpdateProgress" AssociatedUpdatePanelID="AddUnitUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="EngineerName">Unit Name</label>
                                                <asp:TextBox runat="server" ID="UnitNameText" CssClass="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label for="Contactno">Unit Address</label>
                                                <asp:TextBox runat="server" ID="UnitAddresstext" CssClass="form-control" TextMode="MultiLine" />
                                            </div>
                                            <%--<div class="form-group">
                                                <label for="Unit_type">Select Active Type</label>
                                                <asp:DropDownList ID="IsActiveDDL" data-placeholder="Select Unit" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Selected="True" Text="True" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="False" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="saveBtn" CssClass="btn btn-success" runat="server" Text="Save" OnClick="saveBtn_Click" OnClientClick="return Validation()" />
                                        <asp:Button ID="cancelbutton" CssClass="btn btn-default" Text="Cancel" data-dismiss="modal" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Edit_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Edit Unit Details</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="editUnitUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="EditUpdateProgress" AssociatedUpdatePanelID="editUnitUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="EngineerName">Unit Name</label>
                                                <asp:TextBox runat="server" ID="EditUnitText" CssClass="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label for="Contactno">Unit Address</label>
                                                <asp:TextBox runat="server" ID="EditUnitAddressText" CssClass="form-control" TextMode="MultiLine" />
                                                <asp:HiddenField ID="Unithidden" runat="server" />
                                            </div>
                                            <div class="form-group">
                                                <label for="Unit_type">Select Active Type</label>
                                                <asp:DropDownList ID="EditIsActiveDDL" data-placeholder="Select Unit" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Text="True" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="False" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="updateBtn" CssClass="btn btn-success" runat="server" Text="Update" OnClick="updateBtn_Click" OnClientClick="return ValidationUpdate()" />
                                        <input type="reset" class="btn btn-default" value="Cancel" data-dismiss="modal" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
