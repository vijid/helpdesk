﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Engineer/Engineer.Master" AutoEventWireup="true" CodeBehind="TicketClosedReport.aspx.cs" Inherits="HelpDesk.Engineer.TicketClosedReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            $('#dt_reportTable').DataTable({
                "sDom":
                    '<"well well-sm"<"row"<"col-md-4 clearfix"l><"col-md-8 clearfix"fT>r>>' +
                    't' +
                    '<"row"<"col-md-5 clearfix"i><"col-md-7 clearfix"p>>',
                tableTools: {
                    "sSwfPath": "../../assets/lib/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
                }
            });
            $(document).ready(function () {
                $('table.display').dataTable();
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page_bar clearfix">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Problem Solved Reports</h1>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="Ticketlistupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Ticketlistprogress" AssociatedUpdatePanelID="Ticketlistupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="panel panel-default">
                        <div class="row form-horizontal">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Select Status</label>
                                <div class="col-xs-4">
                                    <asp:DropDownList ID="TickettypeDDL" data-placeholder="All Ticket Details" AutoPostBack="true" OnSelectedIndexChanged="TickettypeDDL_SelectedIndexChanged"
                                        AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                        <asp:ListItem Value="0">All Ticket Details</asp:ListItem>
                                        <asp:ListItem Value="1">Problem solved  - same day</asp:ListItem>
                                        <asp:ListItem Value="2">Problem solved within 2 days </asp:ListItem>
                                        <asp:ListItem Value="3">Problem solved more than 2 days</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table id="dt_reportTable" class="table">
                                        <thead>
                                            <tr>
                                                <th class="sub_col">S.No.</th>
                                                <th class="sub_col">User Name</th>
                                                <th class="sub_col">Part Name</th>
                                                <th class="sub_col">Problem Details</th>
                                                <th class="sub_col">Remarks</th>
                                                <th class="sub_col">Status</th>
                                                <th class="sub_col">Created Date</th>
                                                <th class="sub_col">Attended Date</th>
                                                <th class="sub_col">Closed Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:ListView ID="ViewTicketListView" runat="server">
                                                <%--ItemType="HelpDesk.TicketTable" SelectMethod="gettickets"--%>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="sub_col">
                                                            <%#:Container.DataItemIndex+1 %>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("UserName")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("PartName")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("ProblemDetails")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("Remarks")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("Status")%>
                                                        </td>
                                                        <td class="sub_col">
                                                            <%#Eval("CreatedDate")%>
                                                        </td>
                                                        <td class="sub_col"><%#Eval("AttendedDate")%></td>
                                                        <td class="sub_col"><%#Eval("ClosedDate")%></td>
                                                    </tr>
                                                </ItemTemplate>                                                
                                            </asp:ListView>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
