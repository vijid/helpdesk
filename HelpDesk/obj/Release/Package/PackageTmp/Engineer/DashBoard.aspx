﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Engineer/Engineer.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="HelpDesk.Engineer.DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            $('#prod_table').DataTable({
                "sDom":
                '<"well well-sm"<"row"<"col-md-4 clearfix"l><"col-md-8 clearfix"fT>r>>' +
                't' +
                '<"row"<"col-md-5 clearfix"i><"col-md-7 clearfix"p>>',
                tableTools: {
                    "sSwfPath": "../../assets/lib/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
                }
            });
            $(document).ready(function () {
                $('table.display').dataTable();
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageAssetupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageAssetprogress" AssociatedUpdatePanelID="ManageAssetupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page_title">Ticket Details</h1>
                    </div>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table info_table" id="prod_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Ticket No</th>
                                                <th class="sub_col">User Name</th>
                                                <th class="sub_col">Part Name</th>
                                                <th class="sub_col">Problem Details</th>
                                                <th class="sub_col">Created Date</th>
                                                <th class="sub_col">Attended Date</th>
                                                <th class="sub_col">Attended By</th>
                                                <th class="sub_col">Closed Date</th>
                                                <th class="sub_col">Status</th>
                                                <th class="sub_col">View Details</th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="TicketListView" ItemType="HelpDesk.TicketTable" SelectMethod="getTickets" runat="server" OnItemCommand="TicketListView_ItemCommand" OnItemDataBound="TicketListView_ItemDataBound">
                                            <ItemTemplate>
                                                <tr id="RowVal" runat="server">
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                        <asp:HiddenField ID="hfticketid" runat="server" Value='<%#:Item.TicketId %>' />
                                                    </td>
                                                    <td class="sub_col">
                                                         <%#:Item.TicketNo%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.UsersTable.Name%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.PartMasterId!=499?Item.PartMasterTable.PartName:Item.PartEntryText%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.ProblemId!=499?Item.ProblemId==0?"":Item.ProblemMasterTable.Description:Item.ProblemText%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:Label ID="lblcreatedate" runat="server" Text='<%#:Convert.ToDateTime(Item.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt") %>'></asp:Label>
                                                    </td>
                                                     <td class="sub_col"><%#:Item.AttendedDate!=null? Convert.ToDateTime(Item.AttendedDate).ToString("dd-MMM-yyyy hh:mm tt"):"-"%></td>
                                                        <td class="sub_col">
                                                            <asp:Label ID="lblAttendedBy" runat="server"></asp:Label></td>
                                                        <td class="sub_col"><%#:Item.ClosedDate !=null? Convert.ToDateTime(Item.ClosedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-"%></td>
                                                    <td class="sub_col">
                                                        <%#:Item.StatusId==0 ? "-" : Item.StatusTable.StatusName %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:LinkButton ID="linkview" runat="server" CommandName="RowView" CommandArgument='<%#:Item.TicketId %>' ForeColor="#111">View</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
