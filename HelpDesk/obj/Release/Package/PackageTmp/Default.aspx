﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HelpDesk.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <title>Login Page</title>
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <!-- jQuery -->
    <script src="<%=ResolveClientUrl("~/assets/js/jquery.min.js")%>"></script>

    <!-- bootstrap framework -->
    <link href="<%=ResolveClientUrl("~/assets/bootstrap/css/bootstrap.min.css")%>" rel="stylesheet" />
    <link href="<%=ResolveClientUrl("~/assets/css/style.css")%>" rel="stylesheet" media="screen" />
    <!-- google webfonts -->
    <link href="<%=ResolveClientUrl("~/assets/icons/font-awesome/css/font-awesome.min.css")%>" rel="stylesheet" media="screen" />
    <link href="<%=ResolveClientUrl("~/assets/css/login.css")%>" rel="stylesheet" />
    <link href="<%=ResolveClientUrl("~/assets/jalert/jAlert-v2-min.css")%>" rel="stylesheet" />
    <!-- google webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400&amp;subset=latin-ext,latin' rel='stylesheet' type='text/css' />
    <link href="<%=ResolveClientUrl("~/assets/css/login.css")%>" rel="stylesheet" media="screen" />
    <link href="<%=ResolveClientUrl("~/assets/jalert/jAlert-v2-min.css")%>" rel="stylesheet" />
    <!-- jQuery -->
   <%-- <script src="<%=ResolveClientUrl("~/assets/js/jquery.min.js")%>"></script>--%>
    <!-- bootstrap js plugins -->
    <script src="<%=ResolveClientUrl("~/assets/bootstrap/js/bootstrap.min.js")%>"></script>

    <script src="<%=ResolveClientUrl("~/assets/jalert/jAlert-v2-min.js")%>"></script>

    <link href="<%=ResolveClientUrl("~/assets/Login/style.css")%>" rel="stylesheet" />
    <%-- <script>
        $(document).ready(function () {
            $('.open_register_form').click(function (e) {
                e.preventDefault();
                $('#login_form').removeClass().addClass('animated fadeOutDown');
                setTimeout(function () {
                    $('#login_form').removeClass().hide();
                    $('#register_form').show().addClass('animated fadeInUp');
                }, 700);
            })
            $('.open_login_form').click(function (e) {
                e.preventDefault();
                $('#register_form').removeClass().addClass('animated fadeOutDown');
                setTimeout(function () {
                    $('#register_form').removeClass().hide();
                    $('#login_form').show().addClass('animated fadeInUp');
                }, 700);
            })
        });

    </script>--%>

    <style type="text/css">
        .imprequire {
            font-size: 16px;
            color: red;
            font-weight: 100;
        }
    </style>

    <script type="text/javascript">

        function pageLoad() {
            $(document).ready(function () {
                $("#SignUpCPasswrdTxt").keyup(checkPasswordMatch);
            });
            function checkPasswordMatch() {
                var password = $("#SignUpPasswordTxt").val();
                var confirmPassword = $("#SignUpCPasswrdTxt").val();

                if (password != confirmPassword)
                    $("#PasswordDonotLbl").html("Passwords do not match!");
                else
                    $("#PasswordDonotLbl").html(" ");
            }
        }

        function Validate() {
            var name = document.getElementById('<%=SignUpNameTxt.ClientID%>').value;
            var Designation = document.getElementById('<%=SignUpDesignationTxt.ClientID%>').value;
            var Dept = document.getElementById('<%=SignUpDeptTxt.ClientID%>').value;
            var email = document.getElementById('<%=SignUpEmailTxt.ClientID%>').value;
            var username = document.getElementById('<%=SignupUsernametxt.ClientID%>').value;
            var password1 = document.getElementById('<%=SignUpPasswordTxt.ClientID%>').value;
            var password2 = document.getElementById('<%=SignUpCPasswrdTxt.ClientID%>').value;
            var CompAssetno = document.getElementById('<%=SignUpAssetNoTxt.ClientID%>').value;
            var PrinterAssetNo = document.getElementById('<%=SignUpPrinterTxt.ClientID%>').value;
            if (name == '' && Designation == '' && Dept == '' && username == '' && password1 == '' && password2 == '' && CompAssetno == '' && PrinterAssetNo == '') {
                alert('Kindly Enter All Fields');
                return false;
            }
            if (name == '') {
                alert('Kindly Enter Name');
                return false;
            }
            if (Designation == '') {
                alert('Kindly Enter Designation');
                return false;
            }
            if (Dept == '') {
                alert('Kindly Enter Department');
                return false;
            }

            if (document.getElementById('<%=UnitDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Unit Name");
                return false;
            }
            if (CompAssetno == '') {
                alert('Kindly Enter Computer Asset No');
                return false;
            }
            if (document.getElementById('<%=PrinterDDL.ClientID%>').selectedIndex == -1) {
                alert("Kindly Select Printer Name");
                return false;
            }

            if (email != '') {
                var x = document.getElementById("<% = SignUpEmailTxt.ClientID %>").value;
                var atpos = x.indexOf("@");
                var dotpos = x.lastIndexOf(".");
                if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                    alert("Kindly Enter valid Email Id");
                    return false;
                }
            }
            if (username == '') {
                alert('Kindly Enter Username');
                return false;
            }
            if (password1 == '') {
                alert('Kindly Enter Password');
                return false;
            }
            if (password2 == '') {
                alert('Kindly Enter Confirm Password');
                return false;
            }
            //if (password1 != password2) {
            //    alert("Yours passwords do not match");
            //    return false;
            //}
            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="userLoginScriptManager" runat="server"></asp:ScriptManager>
        <header class="navbar navbar-fixed-top" role="banner">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="logo_style">Help Desk</div>
                    <%-- <a runat="server" href="Default.aspx" class="navbar-brand">
                        <img src="assets/img/blank.gif" alt="StayPro" /></a>--%>
                </div>
            </div>
        </header>
        <div class="container-fluid">
            <div class="container">
                <div class="col-lg-12">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <a class="hiddenanchor" id="toregister"></a>
                        <a class="hiddenanchor" id="tologin"></a>
                        <div id="wrapper" class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-8">
                                    <div id="login" class="animate form">
                                        <asp:UpdatePanel ID="loginUpdatePanel" runat="server">
                                            <ContentTemplate>
                                                <div id="login_form">
                                                    <h1 class="login_heading">Login</h1>
                                                    <div class="form-group">
                                                        <label for="login_username">UserName</label>
                                                        <asp:TextBox ID="LoginUserTxt" runat="server" placeholder="Username" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="login_password">Password</label>
                                                        <asp:TextBox ID="LoginPasswordTxt" runat="server" placeholder="Password" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                                        <%-- <span class="help-block"><a href="#">Forgot password?</a></span>--%>
                                                        <span class="help-block"><a href="#toregister" class="open_register_form pull-right to_register" style="color: #428bca; font-size: 16px;">New User? Register Here</a></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-12">
                                                            <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="loginUpdatePanel" runat="server">
                                                                <ProgressTemplate>
                                                                    <div class="loader">
                                                                        <center>
                          <img runat="server" src="~/assets/loader.gif" />
                                  </center>
                                                                    </div>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </div>
                                                    </div>
                                                    <div class="submit_section">
                                                        <asp:Button ID="LoginSubmitBtn" runat="server" CssClass="btn btn-lg btn-user-login btn-block" Text="Continue" OnClick="LoginSubmitBtn_Click" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="col-lg-2"></div>
                            </div>
                            <div class="col-lg-12">
                                <div id="register" class="animate form">
                                    <div class="register-block row">
                                        <div id="register_form">
                                            <h1 class="login_heading">Register</h1>
                                            <asp:UpdatePanel ID="SignupLoginupd" runat="server">
                                                <ContentTemplate>
                                                    <asp:UpdateProgress ID="Signupprg" runat="server" AssociatedUpdatePanelID="SignupLoginupd">
                                                        <ProgressTemplate>
                                                            <div class="update"></div>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                    <div class="col-lg-12 p-l-0 p-r-0">
                                                        <div class="col-lg-6 p-l-0">
                                                            <div class="form-group">
                                                                <label for="register_name">Name <span class="imprequire">*</span></label>
                                                                <asp:TextBox ID="SignUpNameTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_designation">Designation <span class="imprequire">*</span></label>
                                                                <asp:TextBox ID="SignUpDesignationTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_dept">Department <span class="imprequire">*</span></label>
                                                                <asp:TextBox ID="SignUpDeptTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_name">Unit <span class="imprequire">*</span></label>
                                                                <asp:DropDownList ID="UnitDDL" data-placeholder="Select An Unit" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                                    <asp:ListItem Selected="True" Text="Select An Unit" Value="0"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_name">Computer Asset No. <span class="imprequire">*</span></label>
                                                                <asp:TextBox ID="SignUpAssetNoTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 p-r-0">
                                                            <div class="form-group">
                                                                <label for="register_name">Printer Type <span class="imprequire">*</span></label>
                                                                <asp:DropDownList ID="PrinterDDL" data-placeholder="Select A Printer" AppendDataBoundItems="true"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="PrinterDDL_SelectedIndexChanged"
                                                                    CssClass="form-control chosen-select" runat="server">
                                                                    <asp:ListItem Selected="True" Text="Select A Printer" Value="0"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_name">Printer Asset No</label>
                                                                <asp:TextBox ID="SignUpPrinterTxt" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_email">Email Id </label>
                                                                <asp:TextBox ID="SignUpEmailTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_Username">UserName <span class="imprequire">*</span></label>
                                                                <asp:TextBox ID="SignupUsernametxt" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_password">Password <span class="imprequire">*</span></label>
                                                                <asp:TextBox ID="SignUpPasswordTxt" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="register_cpassword">Confirm Password <span class="imprequire">*</span></label>
                                                                <asp:TextBox ID="SignUpCPasswrdTxt" runat="server" TextMode="Password" CssClass="form-control" onChange="checkPasswordMatch();"></asp:TextBox>
                                                                <%--<div class="imprequire" id="PasswordDonotLbl"></div>--%>
                                                                <asp:Label ID="PasswordDonotLbl" runat="server" CssClass="imprequire"></asp:Label>
                                                            </div>
                                                            <div class="form-group">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        &nbsp;
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="col-lg-12 p-r-0 p-l-0">
                                                <div class="col-lg-9 pull-left p-r-0 p-l-0" style="margin-top: -6px;">
                                                    <span class="help-block"><a href="#tologin" class="open_login_form to_register" style="color: #428bca; font-size: 16px;">Already have an account?  Login</a></span>
                                                </div>
                                                <div class="col-lg-3 pull-right p-r-0">
                                                    <asp:Button ID="SignUpSubmitBtn" runat="server" CssClass="btn btn-user-login btn-block signup-button" Text="Sign Up" OnClick="SignUpSubmitBtn_Click" OnClientClick="return Validate()" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>

                <div class="col-lg-12">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <div class="login_container row">
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>

        <%-- <footer>
            <div class="container-fluid">
                <div class="copyright">
                    Copyright &copy; <%=DateTime.Now.Year %> Help Desk.
                </div>
            </div>
        </footer>--%>
        <nav class="navbar navbar-default navbar-fixed-bottom">
            <div class="container-fluid">
                <div class="copyright">
                    Copyright &copy; <%=DateTime.Now.Year %> Help Desk.
                </div>
            </div>
        </nav>
    </form>
</body>
</html>
