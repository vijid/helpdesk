﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HelpDesk.Repositories
{
    public class ReturnRepositories
    {

        public static int GetCookieVendorId()
        {
            if (HttpContext.Current.Request.Cookies["userid"] != null)
            {
                System.Collections.Specialized.NameValueCollection UserInfoCookieCollection;
                UserInfoCookieCollection = HttpContext.Current.Request.Cookies["userid"].Values;
                string vid = HttpContext.Current.Server.HtmlEncode(UserInfoCookieCollection["uid"]);
                vid = Encryptr.Decrypt(vid);
                return Convert.ToInt32(vid);
            }
            return 0;
        }

        public static string GetCookieVendorName()
        {
            if (HttpContext.Current.Request.Cookies["userid"] != null)
            {
                System.Collections.Specialized.NameValueCollection UserInfoCookieCollection;
                UserInfoCookieCollection = HttpContext.Current.Request.Cookies["userid"].Values;
                string vid = HttpContext.Current.Server.HtmlEncode(UserInfoCookieCollection["uname"]);
                vid = Encryptr.Decrypt(vid);
                return vid;
            }
            return "0";
        }

        public static int GetCookieUserId()
        {
            if (HttpContext.Current.Request.Cookies["userid"] != null)
            {
                System.Collections.Specialized.NameValueCollection UserInfoCookieCollection;
                UserInfoCookieCollection = HttpContext.Current.Request.Cookies["userid"].Values;
                string Uid = HttpContext.Current.Server.HtmlEncode(UserInfoCookieCollection["uid"]);
                Uid = Encryptr.Decrypt(Uid);
                return Convert.ToInt32(Uid);
            }
            return 0;
        }

        public static string GetName()
        {
            int uid = GetCookieUserId();
            ithelpdeskEntities _db = new ithelpdeskEntities();
            return _db.AdminTables.FirstOrDefault(x => x.AdminId == uid).AdminName;
        }
     
    }
}