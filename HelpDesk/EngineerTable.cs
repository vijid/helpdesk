//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HelpDesk
{
    using System;
    using System.Collections.Generic;
    
    public partial class EngineerTable
    {
        public EngineerTable()
        {
            this.AdminTables = new HashSet<AdminTable>();
            this.TicketTables = new HashSet<TicketTable>();
        }
    
        public int EngineerId { get; set; }
        public string EngineerName { get; set; }
        public int UnitId { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string ContactNo { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual UnitTable UnitTable { get; set; }
        public virtual ICollection<AdminTable> AdminTables { get; set; }
        public virtual ICollection<TicketTable> TicketTables { get; set; }
    }
}
