﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDDl();
            }
            //LoginUserTxt.Text = Encryptr.Decrypt("1Wp2kRJ8VhfPUSJSdfusZA==");    //==>  Precotco@123        
            //LoginUserTxt.Text = Encryptr.Decrypt("34oeDmR7A6Od/Om32ghS+Q==");    //WHICH WAS 1234
            //LoginUserTxt.Text = Encryptr.Decrypt("psda6iwr5ZW0JsI7S1ZMTA==");    //==>  precota   
            //LoginUserTxt.Text = Encryptr.Decrypt("fx+S6DIauJy/Zzi/eb3KJQ==");    //==>  precoth 
            //LoginUserTxt.Text = Encryptr.Decrypt("rVpDAlkymcOa7Ee4+6gKvA==");    //==>  precotco
        }
        ithelpdeskEntities db = new ithelpdeskEntities();
        public void BindDDl()
        {
            try
            {
                var UnitDt = db.UnitTables.Where(x => x.IsActive == true).OrderBy(x => x.UnitName).ToList();
                UnitDDL.DataSource = UnitDt;
                UnitDDL.DataValueField = "UnitId";
                UnitDDL.DataTextField = "UnitName";
                UnitDDL.DataBind();

                PrinterDDL.Items.Clear();
                var PrinterDt = db.PrinterTypeTables.Where(x => x.IsActive == true).OrderBy(x => x.PrinterName).ToList();
                PrinterDDL.DataSource = PrinterDt;
                PrinterDDL.Items.Insert(0, "Select A Printer");
                PrinterDDL.Items.Insert(1, "No Printer");
                PrinterDDL.DataValueField = "PrinterTypeId";
                PrinterDDL.DataTextField = "PrinterName";
                PrinterDDL.DataBind();
            }
            catch { }
        }
        protected void SignUpSubmitBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string UsrName = "";
                UsrName = SignupUsernametxt.Text;
                var user = db.UsersTables.FirstOrDefault(x => x.UserName == UsrName);
                if (user == null)
                {
                    int PrinterTypeId = 0;
                    UsersTable UsrTab = new UsersTable();
                    UsrTab.Name = SignUpNameTxt.Text;
                    UsrTab.Designation = SignUpDesignationTxt.Text;
                    UsrTab.Department = SignUpDeptTxt.Text;
                    if (SignUpEmailTxt.Text != "")
                    {
                        UsrTab.EmailId = SignUpEmailTxt.Text;
                    }
                    UsrTab.UserName = SignupUsernametxt.Text;
                    UsrTab.UnitId = Convert.ToInt32(UnitDDL.SelectedValue);
                    UsrTab.Password = Encryptr.Encrypt(SignUpCPasswrdTxt.Text);
                    UsrTab.AssetNo = SignUpAssetNoTxt.Text;
                    if (PrinterDDL.SelectedValue != "0")
                    {
                        PrinterTypeId = 0;
                        if (PrinterDDL.SelectedItem.Text != "No Printer")
                        {
                            PrinterTypeId = Convert.ToInt32(PrinterDDL.SelectedValue);
                        }
                    }
                    UsrTab.PrinterTypeId = PrinterTypeId;
                    UsrTab.PrinterAssetNo = SignUpPrinterTxt.Text;
                    UsrTab.IsActive = true;
                    db.UsersTables.Add(UsrTab);
                    db.SaveChanges();
                    clear();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Welcome To Help Desk!! Please Login with Username and Password!!'); });", true);
                }
                else 
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('UserName Already Registered, Please provide different User Name!!'); });", true);
                }
            }
            catch { }
        }


        public void clear()
        {
            SignUpNameTxt.Text = "";
            SignUpDesignationTxt.Text = "";
            SignUpDeptTxt.Text = "";
            SignUpAssetNoTxt.Text = "";
            SignUpPrinterTxt.Text = "";
            UnitDDL.SelectedIndex = 0;
            PrinterDDL.SelectedIndex = 0;
            SignUpAssetNoTxt.Text = "";
            SignupUsernametxt.Text = "";
        }

        protected void LoginSubmitBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (LoginUserTxt.Text.Length == 0 || LoginPasswordTxt.Text.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "model1", "$(function() { alert('Kindly Enter Both Username And Password.')});", true);
                }
                else
                {
                    string UserName = LoginUserTxt.Text;
                    string Password = Encryptr.Encrypt(LoginPasswordTxt.Text);

                    var userDt = db.UsersTables.FirstOrDefault(x => (x.UserName == UserName) && (x.Password == Password) && (x.IsActive == true) && (x.UnitTable.IsActive == true));
                    if (userDt != null)
                    {
                        Session["UserId"] = userDt.UserId.ToString();
                        Session["Role"] = "User";
                        Response.Redirect("User/DashBoard.aspx", false);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "model1", "$(function() { alert('Kindly Check your Username And Password. (OR) Contact Administrator!')});", true);
                    }
                }
            }
            catch { }
        }

        protected void PrinterDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (PrinterDDL.SelectedItem.Text == "No Printer" || PrinterDDL.SelectedItem.Text == "Select A Printer")
                {
                    SignUpPrinterTxt.Enabled = false;
                }
                else
                {
                    SignUpPrinterTxt.Enabled = true;
                }
            }
            catch { }
        }
    }
}