﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="HelpDesk.Admin.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Login</title>
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <!-- bootstrap framework -->
    <link href="<%=ResolveClientUrl("~/assets/bootstrap/css/bootstrap.min.css")%>" rel="stylesheet" />
    <link href="<%=ResolveClientUrl("~/assets/css/style.css")%>" rel="stylesheet" media="screen" />
    <!-- google webfonts -->
    <link href="<%=ResolveClientUrl("~/assets/icons/font-awesome/css/font-awesome.min.css")%>" rel="stylesheet" media="screen" />
    <link href="<%=ResolveClientUrl("~/assets/css/login.css")%>" rel="stylesheet" />
    <link href="<%=ResolveClientUrl("~/assets/jalert/jAlert-v2-min.css")%>" rel="stylesheet" />
    <!-- bootstrap js plugins -->
    <%--<link rel="icon" href="<%=ResolveClientUrl("~/assets/img/favicon.ico")%>" /> --%>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <header class="navbar navbar-fixed-top" role="banner">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="logo_style"> <img runat="server" src="~/assets/img/precot_logo.png" style="height:38px; margin-top:-10px" /> Help Desk</div>
                    <%-- <a runat="server" href="Default.aspx" class="navbar-brand">
                        <img src="assets/img/blank.gif" alt="StayPro" /></a>--%>
                </div>
            </div>
        </header>
        <asp:UpdatePanel ID="loginUpdatePanel" runat="server">
            <ContentTemplate>
                <div class="login_container row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div id="login_form" class="form">
                                <h3 class="login_heading">Administrator Login</h3>
                                <div class="form-group">
                                    <label for="login_username">EmailId</label>
                                    <input type="text" class="form-control" runat="server" placeholder="EmailId" id="login_username" />
                                </div>
                                <div class="form-group">
                                    <label for="login_password">Password</label>
                                    <input type="password" class="form-control" runat="server" placeholder="password" id="login_password" />
                                </div>
                                <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="loginUpdatePanel" runat="server">
                                    <ProgressTemplate>
                                        <div class="loader">
                                            <center>
                                                 <img runat="server" src="~/assets/loader.gif" />
                                             </center>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <div class="submit_section">
                                    <asp:Button CssClass="btn btn-lg btn-success btn-block" Text="Continue" runat="server" ID="loginBtn" OnClick="loginBtn_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

        <nav class="navbar navbar-default navbar-fixed-bottom">
            <div class="container-fluid">
                <div class="copyright">
                    Copyright &copy; <%=DateTime.Now.Year %>  Precot Meridian.
                </div>
            </div>
        </nav>
        <!-- jQuery -->
        <script src="<%=ResolveClientUrl("~/assets/js/jquery.min.js")%>"></script>
        <script src="<%=ResolveClientUrl("~/assets/jalert/jAlert-v2-min.js")%>"></script>

        <script src="<%=ResolveClientUrl("~/assets/bootstrap/js/bootstrap.min.js")%>"></script>
        <!-- parsley.js validation -->
        <script src="<%=ResolveClientUrl("~/assets/lib/Parsley.js/dist/parsley.min.js")%>"></script>
        <!-- form validation functions -->
        <script src="<%=ResolveClientUrl("~/assets/js/apps/validation.js")%>"></script>
    </form>
</body>
</html>

