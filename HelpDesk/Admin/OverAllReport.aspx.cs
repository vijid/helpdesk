﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class OverAllReport : System.Web.UI.Page
    {
        int userid = 0;
        ithelpdeskEntities db = new ithelpdeskEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDDl();
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                BindData();
            }
        }

        public class unitListReportData
        {
            public int ReportedCalls { get; set; }
            public int CompletedCalls { get; set; }
            public int Completeclosed { get; set; }
            public int PendingCalls { get; set; }
            public int UnAttendedCalls { get; set; }
            public int ReOccurInProgress { get; set; }
            public int ReOccurCompleted { get; set; }
        }
        public void BindDDl()
        {
            try
            {
                UnitDDl.Items.Clear();
                var unitDt = db.UnitTables.Where(x => x.IsActive == true).OrderBy(x => x.UnitName).ToList();
                UnitDDl.DataSource = unitDt;
                UnitDDl.Items.Insert(0, "All Units");
                UnitDDl.DataTextField = "UnitName";
                UnitDDl.DataValueField = "UnitId";
                UnitDDl.DataBind();
            }
            catch { }
        }
        public void BindData()
        {
            try
            {
                int UnitId = 0;
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
                List<unitListReportData> UnitLst = new List<unitListReportData>();

                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {


                    ////////////NO OF CALLS////////////
                    var noofcalls = db.TicketTables.Where(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal).ToList();
                    //noofcallreportlbl.Text = noofcalls.Count.ToString();

                    /////////////NO OF CALLS COMPLETED/////////////
                    int completed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId == 5 && x.IsClosed==false);
                    //noofcallscompletedlbl.Text = Convert.ToInt32(completed).ToString("0");


                    //////////////NO OF CALLS PENDING////////////////
                    int pending = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId != 5 && x.AttendedDate != null && x.IsClosed == false);
                    //noofcallspendinglbl.Text = Convert.ToInt32(pending).ToString("0");

                    ////////////////NO OF CALLS CLOSED////////////////
                    int closed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == true);
                    //completeClosedLbl.Text = Convert.ToInt32(closed).ToString("0");

                    /////////////////NO OF CALLS INPROGRESS//////////////
                    //int inprogress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == false);
                    //noofcallsinprogresslbl.Text = Convert.ToInt32(inprogress).ToString("0");

                    //////////////////NO OF CALLS UNATTENDED////////////////////////
                    int unattend = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.AttendedDate == null);
                    //noofcallsunattendedlbl.Text = Convert.ToInt32(unattend).ToString("0");

                    ///////////////NO OF CALLS REOCCURED COMPLETED///////////
                    int ReOccur = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId == 5);
                    //reoccuredCompletecallsLbl.Text = Convert.ToInt32(ReOccur).ToString("0");

                    /////////////////NO OF CALLS REOCCURED INPROGRESS/////////////
                    int ReOccurProgress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId != 5 && x.IsClosed != true);
                    //ReoccuerInProgressLbl.Text = Convert.ToInt32(ReOccurProgress).ToString("0");


                    UnitLst.Add(new unitListReportData
                    {
                        ReportedCalls = noofcalls.Count(),
                        CompletedCalls = completed,
                        Completeclosed = closed,
                        PendingCalls = pending,
                        UnAttendedCalls = unattend,
                        ReOccurInProgress = ReOccurProgress,
                        ReOccurCompleted = ReOccur
                    });
                    ViewStatusListView.DataSource = UnitLst.ToList();
                    ViewStatusListView.DataBind();

                }
                else
                {


                    ////////////NO OF CALLS////////////
                    var noofcalls = db.TicketTables.Where(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.UsersTable.UnitId == UnitId).ToList();
                    //noofcallreportlbl.Text = noofcalls.Count.ToString();

                    /////////////NO OF CALLS COMPLETED/////////////
                    int completed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId == 5 && x.UsersTable.UnitId == UnitId && x.IsClosed == false);
                    //noofcallscompletedlbl.Text = Convert.ToInt32(completed).ToString("0");


                    //////////////NO OF CALLS PENDING////////////////
                    int pending = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId != 5 && x.AttendedDate != null && x.IsClosed == false && x.UsersTable.UnitId == UnitId);
                    //noofcallspendinglbl.Text = Convert.ToInt32(pending).ToString("0");

                    ////////////////NO OF CALLS CLOSED////////////////
                    int closed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == true && x.UsersTable.UnitId == UnitId);
                    //completeClosedLbl.Text = Convert.ToInt32(closed).ToString("0");

                    /////////////////NO OF CALLS INPROGRESS//////////////
                    //int inprogress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == false && x.UsersTable.UnitId==UnitId);
                    //noofcallsinprogresslbl.Text = Convert.ToInt32(inprogress).ToString("0");

                    //////////////////NO OF CALLS UNATTENDED////////////////////////
                    int unattend = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.AttendedDate == null && x.UsersTable.UnitId == UnitId);
                    //noofcallsunattendedlbl.Text = Convert.ToInt32(unattend).ToString("0");

                    ///////////////NO OF CALLS REOCCURED COMPLETED///////////
                    int ReOccur = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId == 5 && x.UsersTable.UnitId == UnitId);
                    //reoccuredCompletecallsLbl.Text = Convert.ToInt32(ReOccur).ToString("0");

                    /////////////////NO OF CALLS REOCCURED INPROGRESS/////////////
                    int ReOccurProgress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId != 5 && x.IsClosed != true && x.UsersTable.UnitId == UnitId);
                    //ReoccuerInProgressLbl.Text = Convert.ToInt32(ReOccurProgress).ToString("0");


                    UnitLst.Add(new unitListReportData
                    {
                        ReportedCalls = noofcalls.Count(),
                        CompletedCalls = completed,
                        Completeclosed = closed,
                        PendingCalls = pending,
                        UnAttendedCalls = unattend,
                        ReOccurInProgress = ReOccurProgress,
                        ReOccurCompleted = ReOccur
                    });
                    ViewStatusListView.DataSource = UnitLst.ToList();
                    ViewStatusListView.DataBind();
                }
            }
            catch { }

        }

       

        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    ViewStatusListView.DataSource = null;
                    ViewStatusListView.DataBind();
                }
                catch { }
            }
            catch { }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch { }
        }

        
    }
}