﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class EditingCalls : System.Web.UI.Page
    {
        int userid = 0;
        ithelpdeskEntities db = new ithelpdeskEntities();
        Int32 ticketid = 0, statusId=0;
        protected string StatusHtml = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //UserId = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            
            try
            {
                ticketid = Convert.ToInt32(Request.QueryString["ticketid"]);
                
                if (!IsPostBack)
                {
                    getticketdetails();
                    getstatus();
                    getticketprocess();
                    AttendDateText.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                }

            }
            catch { }
        }

        public void getticketdetails()
        {
            try
            {
                ticketid = Convert.ToInt32(Request.QueryString["ticketid"]);
                var ticket_det = db.TicketTables.SingleOrDefault(x => x.TicketId == ticketid);
                if (ticket_det != null)
                {
                    StatusHtml =ticket_det.StatusId!=0? ticket_det.StatusTable.StatusName:"Not Attended";

                    lblticketno.Text = ticket_det.TicketNo;
                    lblusername.Text = ticket_det.UsersTable.Name + " ( "+ ticket_det.UsersTable.UnitTable.UnitName +" )";
                    lblemail.Text = ticket_det.UsersTable.EmailId;
                    lblpartname.Text = ticket_det.PartMasterId != 499 ? ticket_det.PartMasterTable.PartName : ticket_det.PartEntryText;
                    lblassetno.Text = ticket_det.AssetTable.AssetNo;
                    lblproblem.Text = ticket_det.ProblemId != 499 ? ticket_det.ProblemMasterTable.Description : ticket_det.ProblemText;
                    lblremarks.Text = ticket_det.Remarks;
                    lbldate.Text = Convert.ToDateTime(ticket_det.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt");
                    AttendDateText.Text = ticket_det.AttendedDate != null ? Convert.ToDateTime(ticket_det.AttendedDate).ToString("dd/MM/yyyy hh:mm") : "-";

                    if (ticket_det.IsClosed != null)
                    {
                        ddlisclosed.SelectedValue = ticket_det.IsClosed == true ? "true" : "false";

                        if (ticket_det.IsClosed == true)
                        {
                            ticketcloseddiv.Visible = true;
                            lblcloseddate.Text = "Closed On " + Convert.ToDateTime(ticket_det.ClosedDate).ToString("dd-MMM-yyyy hh:mm tt");
                            AttendDateText.Enabled = false;
                            AttendDateBtn.Enabled = false;
                            ddlisclosed.Enabled = false;
                            ClosedBtn.Enabled = false;
                            StatusDDL.Enabled = false;
                            remarkstxt.Enabled = false;
                            UpdateStatusBtn.Enabled = false;
                        }
                        else
                        {
                            ticketcloseddiv.Visible = false;
                        }
                    }
                }
            }
            catch { }
        }

        public void getticketprocess()
        {
            ticketid = Convert.ToInt32(Request.QueryString["ticketid"]);
            //db.TicketProcessTables.OrderByDescending(x => x.TicketProcessId).Where(x => x.TicketId == ticketid).ToList()
            var status = (from s in db.TicketProcessTables
                          where s.TicketId == ticketid
                          select new
                          {
                              TicketProcessId = s.TicketProcessId,
                              ProcessDate = s.ProcessDate,
                              StatusName = s.StatusTable.StatusName,
                              Remarks = s.Remarks,
                          }).OrderByDescending(x => x.TicketProcessId).ToList();

            StatusListView.DataSource = status;
            StatusListView.DataBind();
        }

        public void getstatus()
        {
            try
            {
                var status = db.StatusTables.Where(x => x.IsActive == true).ToList();

                StatusDDL.Items.Clear();
                StatusDDL.Items.Insert(0, new ListItem("Select Status", "0"));
                StatusDDL.DataSource = status;
                StatusDDL.DataValueField = "StatusId";
                StatusDDL.DataTextField = "StatusName";
                StatusDDL.DataBind();
            }
            catch { }
        }

        protected void AttendDateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                ticketid = Convert.ToInt32(Request.QueryString["ticketid"]);

                //string AttendedDateValstr = DateTime.ParseExact(AttendDateText.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString("dd-MM-yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                //DateTime attenddate = Convert.ToDateTime(AttendedDateValstr);

                var TicketAttendDt = db.TicketTables.FirstOrDefault(x => x.TicketId == ticketid);
                TicketAttendDt.AttendedDate = DateTime.Now;
                TicketAttendDt.AttendedBy = Convert.ToInt32(Session["UserId"]);
                db.SaveChanges();
                getticketdetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Ticket Details Updated Successfully!!'); });", true);
            }
            catch { }
        }

        protected void UpdateStatusBtn_Click(object sender, EventArgs e)
        {
            try
            {
                ticketid = Convert.ToInt32(Request.QueryString["ticketid"]);
                Int32 statusid = Convert.ToInt32(StatusDDL.SelectedValue);
                string remarks = remarkstxt.Text;

                TicketProcessTable ticket_det = new TicketProcessTable();
                ticket_det.TicketId = ticketid;
                ticket_det.StatusId = statusid;
                ticket_det.Remarks = remarks;
                ticket_det.ProcessDate = DateTime.Now;
                db.TicketProcessTables.Add(ticket_det);
                db.SaveChanges();

                var TicketStatusDb = db.TicketTables.FirstOrDefault(x => x.TicketId == ticketid);
                if (TicketStatusDb.AttendedBy == 0)
                {
                    //string AttendedDateValstr = DateTime.ParseExact(AttendDateText.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString("dd-MM-yyyy hh:mm tt", CultureInfo.InvariantCulture);
                    //DateTime attenddate = Convert.ToDateTime(AttendedDateValstr);
                    TicketStatusDb.AttendedBy = userid;
                    TicketStatusDb.AttendedDate = DateTime.Now;
                }
                TicketStatusDb.StatusId = statusid;
                db.SaveChanges();
                getticketdetails();
                getticketprocess();
                StatusListView.DataBind();
                StatusDDL.SelectedValue = "0";
                remarkstxt.Text = "";
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Status Added Successfully!!'); });", true);
            }
            catch { }
        }

        protected void ClosedBtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool isclosed = Convert.ToBoolean(ddlisclosed.SelectedValue);
                ticketid = Convert.ToInt32(Request.QueryString["ticketid"]);
                var ticket_det = db.TicketTables.SingleOrDefault(x => x.TicketId == ticketid);
                if (ticket_det != null)
                {

                    ticket_det.IsClosed = isclosed;
                    ticket_det.ClosedDate = DateTime.Now;
                    db.SaveChanges();
                    getticketdetails();
                    getticketprocess();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Ticket Status Updated Successfully!!'); });", true);
                }
                if (isclosed == true)
                {
                    ticketcloseddiv.Visible = true;
                }
            }
            catch { }
        }

    }
}