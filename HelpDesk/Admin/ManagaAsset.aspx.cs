﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class ManagaAsset : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDDL();
            }
            BindListData();
        }
        ithelpdeskEntities db = new ithelpdeskEntities();
        public void BindDDL()
        {
            try
            {
                var AssetType = db.AssetTypeTables.Where(x => x.IsActive == true).OrderBy(x => x.AssetName).ToList();


                AssetTypeDDL.DataSource = AssetType;
                AssetTypeDDL.DataValueField = "AssetTypeId";
                AssetTypeDDL.DataTextField = "AssetName";
                AssetTypeDDL.DataBind();


                EditAssetTypeDDL.DataSource = AssetType;
                EditAssetTypeDDL.DataValueField = "AssetTypeId";
                EditAssetTypeDDL.DataTextField = "AssetName";
                EditAssetTypeDDL.DataBind();
            }
            catch { }
        }
        public void BindListData()
        {
            try
            {
                AssetListView.DataSource = null;
                AssetListView.DataBind();
                var GetAssetData = (from lst in db.AssetTables
                                    where lst.IsActive == true
                                    orderby lst.AssetTypeId
                                    select new
                                    {
                                        AssetId = lst.AssetId,
                                        AssetNo = lst.AssetNo,
                                        AssetType = lst.AssetTypeTable.AssetName
                                    }).ToList();

                AssetListView.DataSource = GetAssetData;
                AssetListView.DataBind();
            }
            catch { }
        }
        //public IEnumerable<AssetTable> getAsset()
        //{
        //    //return null;
        //    var GetAssetData = (from lst in db.AssetTables
        //                        where lst.IsActive == true orderby lst.AssetTypeId
        //                        select new {
        //                            AssetNo = lst.AssetNo,
        //                            AssetType = lst.AssetTypeTable.AssetName
        //                        }).ToList();
        //    return GetAssetData;
        //    //return db.AssetTables.Where(x => x.IsActive == true).OrderBy(x => x.AssetTypeId).ToList();
        //}
        protected void AssetListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int AssetId = Convert.ToInt32(e.CommandArgument);
                    var AssetDt = db.AssetTables.FirstOrDefault(x => x.AssetId == AssetId);
                    EditAssetNoText.Text = AssetDt.AssetNo.ToString();
                    EditAssetTypeDDL.SelectedValue = AssetDt.AssetTypeId.ToString();
                    //EditIsActiveDDL.SelectedValue = AssetDt.IsActive.ToString();
                    AssetIdhidden.Value = AssetDt.AssetId.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowDelete")
                {
                    int AssetId = Convert.ToInt32(e.CommandArgument);
                    var EngineerDt = db.AssetTables.FirstOrDefault(x => x.AssetId == AssetId);
                    EngineerDt.IsActive = false;
                    db.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Asset Details Deleted Successfully!!'); });", true);
                    AssetListView.DataBind();
                    BindListData();
                }
            }
            catch { }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                AssetTable AstTab = new AssetTable();
                AstTab.AssetNo = AssetNoText.Text;
                AstTab.AssetTypeId = Convert.ToInt32(AssetTypeDDL.SelectedValue);
                AstTab.IsActive = true; //Convert.ToBoolean(IsActiveDDL.SelectedItem.Text)
                db.AssetTables.Add(AstTab);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                AssetListView.DataBind();
                Clear();
                BindListData();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Asset Details Added Successfully!!'); });", true);
            }
            catch { }
        }
        public void Clear()
        {
            AssetNoText.Text = "";
            AssetTypeDDL.SelectedIndex = 0;
            EditAssetNoText.Text = "";
            EditAssetTypeDDL.SelectedIndex = 0;
        }
        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int AssetID = Convert.ToInt32(AssetIdhidden.Value);
                var editAssetDt = db.AssetTables.FirstOrDefault(x => x.AssetId == AssetID);
                editAssetDt.AssetNo = EditAssetNoText.Text;
                editAssetDt.AssetTypeId = Convert.ToInt32(EditAssetTypeDDL.SelectedValue);
                //editAssetDt.IsActive = Convert.ToBoolean(EditIsActiveDDL.SelectedItem.Text);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                AssetListView.DataBind();
                Clear();
                BindListData();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Asset Details Updated Successfully!!'); });", true);
            }
            catch { }
        }
    }
}