﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HelpDesk.Repositories;

namespace HelpDesk.Admin
{
    public partial class ManageStatus : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
        }

        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<StatusTable> getStatus()
        {
            return db.StatusTables.Where(x => x.IsActive == true).OrderBy(x => x.StatusId).ToList();
        }

        protected void AssetListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int sid = Convert.ToInt32(e.CommandArgument);
                    var status = db.StatusTables.FirstOrDefault(x => x.StatusId == sid);
                    statushf.Value = status.StatusId.ToString();
                    EditStatusNameText.Text = status.StatusName;
                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowDelete")
                {
                    int sid = Convert.ToInt32(e.CommandArgument);
                    var status = db.StatusTables.FirstOrDefault(x => x.StatusId == sid);
                    status.IsActive = false;
                    db.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Status Deleted Successfully!!'); });", true);
                    StatusListView.DataBind();
                }
            }
            catch { }
        }

        public void Clear()
        {
            StatusNameText.Text = "";
            EditStatusNameText.Text = "";
            statushf.Value = "";
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string status = StatusNameText.Text;
                StatusTable status_det = new StatusTable();
                status_det.StatusName = status;
                status_det.IsActive = true;
                db.StatusTables.Add(status_det);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                StatusListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Status Added Successfully!!'); });", true);
            }
            catch { }
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int sid = Convert.ToInt32(statushf.Value);
                string status = EditStatusNameText.Text;
                var status_det = db.StatusTables.FirstOrDefault(x => x.StatusId == sid);
                status_det.StatusName = status;
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                StatusListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Status Updated Successfully!!'); });", true);
            }
            catch { }
        }
    }
}