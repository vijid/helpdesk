﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ManagaAsset.aspx.cs" Inherits="HelpDesk.Admin.ManagaAsset" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            $(document).ready(function () {
                $('#prod_table').DataTable();
            });
        }

        function Validation() {
            var assetno = document.getElementById('<%=AssetNoText.ClientID%>').value;
            if (assetno == '') {
                alert('Kindly Enter Asset Number');
                return false;
            }
            if (document.getElementById('<%=AssetTypeDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Asset Type Name");
                return false;
            }
            return true;
        }

        function ValidationUpdate() {
            var assetno = document.getElementById('<%=EditAssetNoText.ClientID%>').value;
            if (assetno == '') {
                alert('Kindly Enter Asset Number');
                return false;
            }
            if (document.getElementById('<%=EditAssetTypeDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Asset Type Name");
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageAssetupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageAssetprogress" AssociatedUpdatePanelID="ManageAssetupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="page_title">Manage Asset</h1>
                    </div>
                    <div class="col-md-2 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#bs_modal_regular">Add Asset</a>
                    </div>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table info_table" id="prod_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Asset No</th>
                                                <th class="sub_col">Asset Type</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                          <asp:ListView ID="AssetListView" runat="server" OnItemCommand="AssetListView_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("AssetNo")%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("AssetType")%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:LinkButton ID="lkbtnedit" runat="server" class="btn btn-inverse btn-sm" CommandName="RowEdit" CommandArgument='<%#Eval("AssetId")%>'><span class="fa fa-pencil-square-o fa-lg"></span>Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtndelete" runat="server" class="btn btn-danger btn-sm" CommandName="RowDelete" CommandArgument='<%#Eval("AssetId")%>' OnClientClick="javascript:return confirm('Are you sure you want to delete this record?')"><span class="fa fa-pencil-square-o fa-lg"></span>Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                      <%--  <asp:ListView ID="AssetListView" ItemType="HelpDesk.AssetTable" SelectMethod="getAsset" runat="server" OnItemCommand="AssetListView_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.AssetNo%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.AssetTypeTable.AssetName%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:LinkButton ID="lkbtnedit" runat="server" class="btn btn-inverse btn-sm" CommandName="RowEdit" CommandArgument='<%#:Item.AssetId%>'><span class="fa fa-pencil-square-o fa-lg"></span>Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtndelete" runat="server" class="btn btn-danger btn-sm" CommandName="RowDelete" CommandArgument='<%#:Item.AssetId%>' OnClientClick="javascript:return confirm('Are you sure you want to delete this record?')"><span class="fa fa-pencil-square-o fa-lg"></span>Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>--%>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
    <div class="modal fade" id="bs_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Add Asset</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="AddAssetUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="AddUpdateProgress" AssociatedUpdatePanelID="AddAssetUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="EngineerName">Asset Type</label>
                                                <asp:DropDownList ID="AssetTypeDDL" data-placeholder="Select Asset Type" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server" SelectMethod="">
                                                    <asp:ListItem Value="0">Select Asset Type</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <label for="EngineerName">Asset No</label>
                                                <asp:TextBox runat="server" ID="AssetNoText" CssClass="form-control" />
                                            </div>
                                            <%-- <div class="form-group">
                                                <label for="EngineerName">Is Active</label>
                                                <asp:DropDownList ID="IsActiveDDL" data-placeholder="Select IsActive" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Selected="True" Text="True" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="False" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="saveBtn" CssClass="btn btn-success" runat="server" Text="Save" OnClick="saveBtn_Click" OnClientClick="return Validation()" />
                                        <asp:Button ID="cancelbutton" CssClass="btn btn-default" Text="Cancel" data-dismiss="modal" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Edit_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Edit Asset</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="editAssetUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="EditUpdateProgress" AssociatedUpdatePanelID="editAssetUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="EngineerName">Asset Type</label>
                                                <asp:DropDownList ID="EditAssetTypeDDL" data-placeholder="Select Asset Type" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server" SelectMethod="">
                                                    <asp:ListItem Value="0">Select Asset Type</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <label for="EngineerName">Asset No</label>
                                                <asp:TextBox runat="server" ID="EditAssetNoText" CssClass="form-control" />
                                                <asp:HiddenField ID="AssetIdhidden" runat="server" />
                                            </div>
                                            <%--  <div class="form-group">
                                                <label for="EngineerName">Is Active</label>
                                                <asp:DropDownList ID="EditIsActiveDDL" data-placeholder="Select IsActive" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Text="True" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="False" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="updateBtn" CssClass="btn btn-success" runat="server" Text="Update" OnClick="updateBtn_Click" OnClientClick="return ValidationUpdate()" />
                                        <input type="reset" class="btn btn-default" value="Cancel" data-dismiss="modal" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
