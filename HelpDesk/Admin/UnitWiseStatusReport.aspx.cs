﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class UnitWiseStatusReport : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDDl();
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                //BindData();
            }
        }

        ithelpdeskEntities db = new ithelpdeskEntities();
        public void BindDDl()
        {
            try
            {
                UnitDDl.Items.Clear();
                var unitDt = db.UnitTables.Where(x => x.IsActive == true).OrderBy(x => x.UnitName).ToList();
                UnitDDl.DataSource = unitDt;
                UnitDDl.Items.Insert(0, "Select Unit Name");
                UnitDDl.DataTextField = "UnitName";
                UnitDDl.DataValueField = "UnitId";
                UnitDDl.DataBind();

                StatusDDL.Items.Clear();
                var StatusDt = db.StatusTables.Where(x => x.IsActive == true).OrderBy(x => x.StatusName).ToList();
                StatusDDL.DataSource = StatusDt;
                StatusDDL.Items.Insert(0, "Pending");
                StatusDDL.DataTextField = "StatusName";
                StatusDDL.DataValueField = "StatusId";
                StatusDDL.DataBind();
            }
            catch { }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch { }
        }

        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ViewStatusListView.DataSource = null;
                ViewStatusListView.DataBind();
            }
            catch { }
        }
        public void BindData()
        {
            try
            {
                ViewStatusListView.DataSource = null;
                ViewStatusListView.DataBind();
                int UnitId = 0, statusId = 0;
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);

                UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                if (StatusDDL.SelectedValue == "Pending")
                {
                    statusId = 0;
                }
                else
                {
                    statusId = Convert.ToInt32(StatusDDL.SelectedValue);
                }
                //statusId = Convert.ToInt32(StatusDDL.SelectedValue);
                var StatusDetails = (from tck in db.TicketTables
                                     where
                                         tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal && tck.UsersTable.UnitTable.UnitId == UnitId
                                         && tck.StatusId == statusId
                                     select
                                         new
                                         {
                                             ticketId = tck.TicketId,
                                             TicketNo = tck.TicketNo,
                                             PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                             UserName = tck.UsersTable.Name,
                                             UserDetail = tck.UsersTable.EmailId,
                                             AssetNo = tck.AssetTable.AssetNo,
                                             ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                             Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                             Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                             StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                             CreatedDate = tck.CreatedDate,
                                             AttendedDate = tck.AttendedDate,
                                             AttendedBy = tck.AttendedBy != 0 ? db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName : "-",
                                             ClosedDate = tck.ClosedDate
                                         }).ToList();

                if (StatusDetails.Count() != 0)
                {
                    ViewStatusListView.DataSource = StatusDetails;
                    ViewStatusListView.DataBind();
                }
            }
            catch { }
        }

        protected void ViewStatusListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                Label createdate_lbl = (Label)e.Item.FindControl("lblcreatedate");
                Label closeddate_lbl = (Label)e.Item.FindControl("lblcloseddate");
                Label Duration_Label = (Label)e.Item.FindControl("durationLbl");
                DateTime startTime = Convert.ToDateTime(createdate_lbl.Text);
                DateTime endTime = DateTime.Now;
                if (closeddate_lbl.Text != "-")
                {
                    endTime = Convert.ToDateTime(closeddate_lbl.Text);
                }

                TimeSpan span = endTime.Subtract(startTime);
                //string year = "";
                //string month = "";
                string days = "";
                string hours = "";
                string mins = "";
                string sec = "";

                if (span.Days > 0)
                {
                    days = span.Days.ToString() + " Days ";
                }
                if (span.Hours > 0)
                {
                    hours = span.Hours.ToString() + " Hours ";
                }
                if (span.Minutes > 0)
                {
                    mins = span.Minutes.ToString() + " Minutes ";
                }
                if (span.Seconds > 0)
                {
                    sec = span.Seconds.ToString() + " Seconds ";
                }
                Duration_Label.Text = days + hours + mins + sec;
            }
            catch { }
        }
    }
}