﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class ManageUnitMaster : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
        }

        ithelpdeskEntities db = new ithelpdeskEntities();
        public IEnumerable<UnitTable> GetUnit()
        {
            return db.UnitTables.OrderBy(x => x.UnitName).ToList();
        }
        protected void UnitListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int UnitId = Convert.ToInt32(e.CommandArgument);
                    var UnitDt = db.UnitTables.FirstOrDefault(x => x.UnitId == UnitId);
                    EditUnitText.Text = UnitDt.UnitName.ToString();
                    EditUnitAddressText.Text = UnitDt.UnitAddress.ToString();
                    // EditIsActiveDDL.SelectedValue = Convert.ToBoolean(UnitDt.IsActive).ToString();
                    EditIsActiveDDL.ClearSelection();
                    EditIsActiveDDL.Items.FindByText(UnitDt.IsActive.ToString()).Selected = true;
                    Unithidden.Value = UnitDt.UnitId.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowDelete")
                {
                    int UnitId = Convert.ToInt32(e.CommandArgument);
                    var UnitDt = db.UnitTables.FirstOrDefault(x => x.UnitId == UnitId);

                    if (UnitDt.IsActive.ToString() == "True")
                    {
                        UnitDt.IsActive = false;
                        db.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Unit Details Deactivated Successfully!!'); });", true);
                    }
                    else
                    {
                        UnitDt.IsActive = true;
                        db.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Unit Details Activated Successfully!!'); });", true);
                    }
                    //UnitDt.IsActive = false;
                    //db.SaveChanges();
                    //ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Unit Details Deleted Successfully!!'); });", true);
                    UnitListView.DataBind();
                }
            }
            catch { }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                UnitTable unit = new UnitTable();
                unit.UnitName = UnitNameText.Text;
                unit.UnitAddress = UnitAddresstext.Text;
                unit.IsActive = true; //Convert.ToBoolean(IsActiveDDL.SelectedItem.Text.ToString())
                db.UnitTables.Add(unit);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                UnitListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Unit Details Added Successfully!!'); });", true);
            }
            catch { }
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int UnitId = Convert.ToInt32(Unithidden.Value);
                var Unitdt = db.UnitTables.FirstOrDefault(x => x.UnitId == UnitId);
                Unitdt.UnitName = EditUnitText.Text;
                Unitdt.UnitAddress = EditUnitAddressText.Text;
                Unitdt.IsActive = Convert.ToBoolean(EditIsActiveDDL.SelectedItem.Text.ToString());
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                UnitListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Unit Details Updated Successfully!!'); });", true);
            }
            catch { }
        }

        public void Clear()
        {
            UnitNameText.Text = "";
            UnitAddresstext.Text = "";
            EditUnitText.Text = "";
            EditUnitAddressText.Text = "";
        }

        protected void UnitListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                HiddenField Unitid_hf = (HiddenField)e.Item.FindControl("hfunitid");



                Int32 UnitId = Convert.ToInt32(Unitid_hf.Value);
                var Unit_det = db.UnitTables.SingleOrDefault(x => x.UnitId == UnitId);
                if (Unit_det != null)
                {
                    if (Unit_det.IsActive == false)
                    {
                        HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                        Cell.Style.Add("background-color", "#FE642E");            //#56c1bd
                    }
                    else
                    { 
                        
                    }
                }
            }
            catch { }
        }
    }
}