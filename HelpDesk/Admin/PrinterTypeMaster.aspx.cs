﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HelpDesk.Repositories;

namespace HelpDesk.Admin
{
    public partial class PrinterTypeMaster : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
        }

        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<PrinterTypeTable> getPrinter()
        {
            return db.PrinterTypeTables.Where(x => x.IsActive == true).OrderBy(x => x.PrinterTypeId).ToList();
        }
        protected void PrinterListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int PrinterId = Convert.ToInt32(e.CommandArgument);
                    var Printer_Det = db.PrinterTypeTables.FirstOrDefault(x => x.PrinterTypeId == PrinterId);
                    EditPrinterNameText.Text = Printer_Det.PrinterName;
                    // EditIsActiveDDL.SelectedValue = Printer_Det.IsActive.ToString();
                    PrinterIdhidden.Value = Printer_Det.PrinterTypeId.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowDelete")
                {
                    int PrinterId = Convert.ToInt32(e.CommandArgument);
                    var Printer_Det = db.PrinterTypeTables.FirstOrDefault(x => x.PrinterTypeId == PrinterId);
                    Printer_Det.IsActive = false;
                    db.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Printer Details Deleted Successfully!!'); });", true);
                    PrinterListView.DataBind();
                }
            }
            catch { }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PrinterTypeTable Printer_Det = new PrinterTypeTable();
                Printer_Det.PrinterName = PrinterNameText.Text;
                Printer_Det.IsActive = true;
                db.PrinterTypeTables.Add(Printer_Det);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                PrinterListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Printer Details Added Successfully!!'); });", true);
            }
            catch { }
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int PrinterId = Convert.ToInt32(PrinterIdhidden.Value);
                var Printer_Det = db.PrinterTypeTables.FirstOrDefault(x => x.PrinterTypeId == PrinterId);
                Printer_Det.PrinterName = EditPrinterNameText.Text;
                //  Printer_Det.IsActive = Convert.ToBoolean(EditIsActiveDDL.SelectedItem.Text);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                PrinterListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Printer Details Updated Successfully!!'); });", true);
            }
            catch { }
        }

        public void Clear()
        {
            PrinterNameText.Text = "";
            // IsActiveDDL.SelectedIndex = 0;
            EditPrinterNameText.Text = "";
            // EditIsActiveDDL.SelectedIndex = 0;
            PrinterIdhidden.Value = "";
        }
    }
}