﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="EditingCalls.aspx.cs" Inherits="HelpDesk.Admin.EditingCalls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="<%=ResolveClientUrl("~/assets/js/jquery.maskedinput.js")%>"></script>
    <script type="text/javascript">
        function pageLoad() {
            $(document).ready(function () {
                $('#prod_table').DataTable();
            });


            $(document).ready(
       function () {
           $('#<%=AttendDateText.ClientID %>').click(
                function () {
                    $("#<%=AttendDateText.ClientID %>").mask("99/99/9999 99:99");
                });
       });
   }
   function Validation() {
       var status = document.getElementById('<%=StatusDDL.ClientID%>').selectedIndex;
            var remarks = document.getElementById('<%=remarkstxt.ClientID%>').value;
            if (status == "0" || remarks == null || remarks == "") {
                alert('Kindly Enter All Fields');
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageTicketupdate" runat="server">
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="ManageTicketprogress" AssociatedUpdatePanelID="ManageTicketupdate" runat="server">
        <ProgressTemplate>
            <div class="update"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <%-- <div class="page_bar clearfix">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">View Ticket Details</h1>
            </div>
        </div>
    </div>--%>
    <div class="page_content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <asp:UpdatePanel ID="ViewTicketUpd" runat="server">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="AddUpdateProgress" AssociatedUpdatePanelID="ViewTicketUpd" runat="server">
                                <ProgressTemplate>
                                    <div class="update"></div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <fieldset>
                                            <div class="col-lg-12">
                                                <div class="col-lg-9"><legend><span>View Ticket Details</span></legend></div>
                                                <div class="col-lg-3 text-right"><legend><span>Status : <b><%=StatusHtml %></b></span></legend></div>
                                            </div>                                            
                                        </fieldset>
                                        <div class="col-lg-4">
                                            <div class="table-responsive ">
                                                <table class="table tabel-striped borderstyle">
                                                    <%--<div class="table-responsive custom_table">
                                                <table class="table tabel-striped borderstyle">--%>
                                                    <tr>
                                                        <th>Ticket No:</th>
                                                        <td>
                                                            <asp:Label ID="lblticketno" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <th>User Name:</th>
                                                        <td>
                                                            <asp:Label ID="lblusername" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Email Id:</th>
                                                        <td>
                                                            <asp:Label ID="lblemail" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Part Name:</th>
                                                        <td>
                                                            <asp:Label ID="lblpartname" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Asset No:</th>
                                                        <td>
                                                            <asp:Label ID="lblassetno" runat="server"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <%-- <div class="form-group">
                                                <label for="AssetType">Ticket No: </label>
                                                <asp:Label ID="lblticketno" runat="server"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="AssetType">User Name: </label>
                                                <asp:Label ID="lblusername" runat="server"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="AssetType">Part Name:</label>
                                                <asp:Label ID="lblpartname" runat="server"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="AssetType">Asset No:</label>
                                                <asp:Label ID="lblassetno" runat="server"></asp:Label>
                                            </div>--%>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="table-responsive">
                                                <table class="table tabel-striped borderstyle">
                                                    <tr>
                                                        <th class="col-lg-6">Problem Details:</th>
                                                        <td class="col-lg-6">
                                                            <asp:Label ID="lblproblem" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <th class="col-lg-6">Remarks:</th>
                                                        <td class="col-lg-6">
                                                            <asp:Label ID="lblremarks" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <th class="col-lg-6">Submission Date:</th>
                                                        <td class="col-lg-6">
                                                            <asp:Label ID="lbldate" runat="server"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <th class="col-lg-6">Attended Date:</th>
                                                        <td class="col-lg-6">
                                                            <asp:TextBox ID="AttendDateText" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy hh:mm"></asp:TextBox>
                                                            <%-- <div id="AttnDtDiv" runat="server" class="input-group date ts_datepicker col-lg-5" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                                <asp:TextBox ID="AttendDateText" runat="server" CssClass="form-control" type="text"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th class="col-lg-6"> </th>
                                                        <td class="col-lg-6">
                                                            <asp:Label ID="AttendDatelbl" Text="dd/MM/yyyy HH:mm Ex:(17/02/2016 15:34)" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>


                                            <%-- <div class="form-group">
                                                <label for="AssetType">Problem Details:</label>
                                                <asp:Label ID="lblproblem" runat="server"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="AssetType">Remarks:</label>
                                                <asp:Label ID="lblremarks" runat="server"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="AssetType">Submission Date:</label>
                                                <asp:Label ID="lbldate" runat="server"></asp:Label>
                                            </div>
                                            <div class="form-group">
                                                <label for="AssetType">Attended Date:</label>
                                                <div id="AttnDtDiv" runat="server" class="input-group date ts_datepicker col-lg-5" data-date-format="dd-mm-yyyy" data-date-autoclose="true">
                                                    <asp:TextBox ID="AttendDateText" runat="server" CssClass="form-control" type="text"></asp:TextBox>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>--%>

                                            <%-- <div class="form-group">
                                                    <label for="AssetType">Staus:</label>
                                                    <asp:Label ID="lblstatus" runat="server"></asp:Label>
                                                </div>--%>

                                            <div class="form-group text-right">
                                                <asp:Button ID="AttendDateBtn" CssClass="btn btn-primary" Text="Update Attended Status" runat="server" OnClick="AttendDateBtn_Click" />
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div id="ticketcloseddiv" runat="server" class="closedticket color_g" visible="false">
                                                <i class="fa fa-check"></i>
                                                <asp:Label ID="lblcloseddate" runat="server" Style="font-size: 18px;"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <fieldset>
                                            <legend><span>Update Process Status</span></legend>
                                        </fieldset>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="AssetType">Status: </label>
                                                <asp:DropDownList ID="StatusDDL" data-placeholder="Select Status" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Selected="True" Text="Select Status" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <label for="AssetType">Remarks: </label>
                                                <asp:TextBox ID="remarkstxt" runat="server" CssClass="form-control" TextMode="MultiLine" Text="-"></asp:TextBox>
                                            </div>
                                            <div class="form-group text-right">
                                                <asp:Button ID="UpdateStatusBtn" CssClass="btn btn-warning" runat="server" Text="Update Process Status" OnClick="UpdateStatusBtn_Click" OnClientClick="return Validation()" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="table-responsive">
                                                <table class="table info_table" id="prod_table">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Status DateTime</th>
                                                            <th class="sub_col">Status</th>
                                                            <th class="sub_col">Remarks</th>
                                                        </tr>
                                                    </thead>
                                                    <asp:ListView ID="StatusListView" runat="server">
                                                        <%--ItemType="HelpDesk.TicketProcessTable" SelectMethod="getticketprocess"--%>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <%# Container.DataItemIndex + 1%>
                                                                </td>
                                                                <td class="sub_col">
                                                                    <%# Convert.ToDateTime(Eval("ProcessDate")).ToString("dd-MMM-yyyy hh:mm tt")%>
                                                                </td>
                                                                <td class="sub_col">
                                                                    <%#Eval("StatusName") %>
                                                                </td>
                                                                <td class="sub_col"><%#Eval("Remarks") %></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <fieldset>
                                            <legend><span>Update Ticked Closed Status</span></legend>
                                        </fieldset>
                                        <div class="col-lg-12 form-horizontal">
                                            <div class="col-lg-offset-4 col-lg-6">
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Is Closed:</label>
                                                    <div class="col-lg-4">
                                                        <label class="radio_css">
                                                            <asp:RadioButtonList ID="ddlisclosed" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Value="true">Yes</asp:ListItem>
                                                                <asp:ListItem Value="false">No</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group text-right">
                                                    <div class="col-lg-3">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <asp:Button ID="ClosedBtn" CssClass="btn btn-success" runat="server" Text="Update Closed Status" OnClick="ClosedBtn_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
