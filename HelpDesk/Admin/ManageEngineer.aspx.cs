﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class ManageEngineer : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                bindDDL();
            }
            BindEnggDetails();
        }
        ithelpdeskEntities db = new ithelpdeskEntities();
        //public IEnumerable<AdminTable> GetEngineer()
        //{
        //    return db.AdminTables.Where(x => x.RoleId == 2 && x.IsActive == true).OrderBy(x => x.AdminName).ToList();
        //}

        //public IEnumerable<EngineerTable> GetEngineer()
        //{
        //    return db.EngineerTables.Where(x => x.IsActive == true).OrderBy(x => x.EngineerName).ToList();
        //}

        public void BindEnggDetails()
        {
            try
            {
                EngineerListView.DataSource = null;
                EngineerListView.DataBind();
                string UnitId = UnitDDl.SelectedValue;
                if (UnitId == "All Unit")
                {
                    var EnggDt = (from lst in db.EngineerTables
                                  //where lst.IsActive == true
                                  orderby lst.EngineerName
                                  select new
                                  {
                                      EngineerId = lst.EngineerId,
                                      EngineerName = lst.EngineerName,
                                      EmailId = lst.EmailId,
                                      UnitName = lst.UnitTable.UnitName,
                                      Password = lst.Password,
                                      ContactNo = lst.ContactNo,
                                      IsActive = lst.IsActive
                                  }
                                      ).ToList();
                    EngineerListView.DataSource = EnggDt;
                    EngineerListView.DataBind();
                }
                else
                {
                    int UntId = Convert.ToInt32(UnitDDl.SelectedValue);
                    var EnggDt = (from lst in db.EngineerTables
                                  //where lst.IsActive == true && lst.UnitId == UntId
                                  where lst.UnitId == UntId
                                  orderby lst.EngineerName
                                  select new
                                  {
                                      EngineerId = lst.EngineerId,
                                      EngineerName = lst.EngineerName,
                                      EmailId = lst.EmailId,
                                      UnitName = lst.UnitTable.UnitName,
                                      Password = lst.Password,
                                      ContactNo = lst.ContactNo,
                                      IsActive = lst.IsActive
                                  }
                                    ).ToList();
                    EngineerListView.DataSource = EnggDt;
                    EngineerListView.DataBind();
                }
            }
            catch { }
        }
        public void bindDDL()
        {
            try
            {
                var UnitDetail = db.UnitTables.Where(x => x.IsActive == true).OrderBy(x => x.UnitName).ToList();

                UnitDDl.Items.Clear();
                UnitDDl.DataSource = UnitDetail;
                UnitDDl.Items.Insert(0, "All Unit");
                UnitDDl.DataTextField = "UnitName";
                UnitDDl.DataValueField = "UnitId";
                UnitDDl.DataBind();

                UnitDDList.Items.Clear();
                UnitDDList.Items.Insert(0, "Choose an Unit");
                UnitDDList.DataSource = UnitDetail;
                UnitDDList.DataValueField = "UnitId";
                UnitDDList.DataTextField = "UnitName";
                UnitDDList.DataBind();

                EditUnitDDL.Items.Clear();
                EditUnitDDL.Items.Insert(0, "Choose an Unit");
                EditUnitDDL.DataSource = UnitDetail;
                EditUnitDDL.DataValueField = "UnitId";
                EditUnitDDL.DataTextField = "UnitName";
                EditUnitDDL.DataBind();
            }
            catch { }
        }
        protected void EngineerListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int EngineerId = Convert.ToInt32(e.CommandArgument);
                    var EngineerDt = db.EngineerTables.FirstOrDefault(x => x.EngineerId == EngineerId);
                    EditEnggNameText.Text = EngineerDt.EngineerName.ToString();
                    EditContactText.Text = EngineerDt.ContactNo.ToString();
                    EditEmailText.Text = EngineerDt.EmailId.ToString();
                    EditPasswordTxt.Text = Encryptr.Decrypt(EngineerDt.Password);
                    EditUnitDDL.SelectedValue = EngineerDt.UnitId.ToString();
                    //if (EngineerDt.IsActive != true)
                    //{
                    EditIsActiveDDl.ClearSelection();
                    EditIsActiveDDl.Items.FindByText(EngineerDt.IsActive.ToString()).Selected = true;

                    //    EditIsActiveDDl.SelectedItem.Text = EngineerDt.IsActive.ToString();
                    //}

                    //EditIsActiveDDl.SelectedItem = EngineerDt.IsActive.ToString();
                    Engineerhidden.Value = EngineerDt.EngineerId.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowDelete")
                {
                    int EngineerId = Convert.ToInt32(e.CommandArgument);
                    var EngineerDt = db.EngineerTables.FirstOrDefault(x => x.EngineerId == EngineerId);
                    EngineerDt.IsActive = false;
                    db.SaveChanges();
                    var AdminDt = db.AdminTables.FirstOrDefault(x => x.EngineerId == EngineerId);
                    AdminDt.IsActive = false;
                    db.SaveChanges();
                    BindEnggDetails();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Engineer Details Deleted Successfully!!'); });", true);
                    EngineerListView.DataBind();
                }
            }
            catch { }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                EngineerTable EngTab = new EngineerTable();
                EngTab.EngineerName = EngineerNameText.Text;
                EngTab.ContactNo = ContactNotext.Text;
                EngTab.EmailId = EmailIdTxt.Text;
                EngTab.Password = Encryptr.Encrypt(EmailIdTxt.Text);
                EngTab.UnitId = Convert.ToInt32(UnitDDList.SelectedValue);
                EngTab.IsActive = Convert.ToBoolean(IsActiveDDl.SelectedItem.Text);
                db.EngineerTables.Add(EngTab);

                AdminTable admnTab = new AdminTable();
                admnTab.AdminName = EngTab.EngineerName;
                admnTab.EngineerId = EngTab.EngineerId;
                admnTab.EmailId = EngTab.EmailId;
                admnTab.Password = EngTab.Password;
                admnTab.ContactNo = EngTab.ContactNo;
                admnTab.RoleId = 2;
                admnTab.IsActive = Convert.ToBoolean(IsActiveDDl.SelectedItem.Text);
                db.AdminTables.Add(admnTab);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                EngineerListView.DataBind();
                Clear();
                BindEnggDetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Engineer Details Added Successfully!!'); });", true);
            }
            catch { }
        }
        public void Clear()
        {
            EngineerNameText.Text = "";
            ContactNotext.Text = "";
            EmailIdTxt.Text = "";
            UnitDDList.SelectedIndex = 0;
            EditEnggNameText.Text = "";
            EditContactText.Text = "";
            EditEmailText.Text = "";
            EditPasswordTxt.Text = "";
            EditUnitDDL.SelectedIndex = 0;
        }
        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int EngineerId = Convert.ToInt32(Engineerhidden.Value);
                var EnggDt = db.EngineerTables.FirstOrDefault(x => x.EngineerId == EngineerId);
                EnggDt.EngineerName = EditEnggNameText.Text;
                EnggDt.ContactNo = EditContactText.Text;
                EnggDt.EmailId = EditEmailText.Text;
                EnggDt.Password = Encryptr.Encrypt(EditPasswordTxt.Text);
                EnggDt.UnitId = Convert.ToInt32(EditUnitDDL.SelectedValue);
                EnggDt.IsActive = Convert.ToBoolean(EditIsActiveDDl.SelectedItem.Text);
                db.SaveChanges();
                var adminTab = db.AdminTables.FirstOrDefault(x => x.EngineerId == EngineerId);
                adminTab.AdminName = EnggDt.EngineerName;
                adminTab.EngineerId = EnggDt.EngineerId;
                adminTab.EmailId = EnggDt.EmailId;
                adminTab.Password = EnggDt.Password;
                adminTab.ContactNo = EnggDt.ContactNo;
                //adminTab.RoleId = 2;
                adminTab.IsActive = Convert.ToBoolean(EditIsActiveDDl.SelectedItem.Text);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                EngineerListView.DataBind();
                Clear();
                BindEnggDetails();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Engineer Details Updated Successfully!!'); });", true);
            }
            catch { }
        }

        protected void UnitDDl_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEnggDetails();
            }
            catch { }
        }
    }
}