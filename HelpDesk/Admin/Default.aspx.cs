﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HelpDesk.Repositories;

namespace HelpDesk.Admin
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void loginBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (login_username.Value.Length == 0 || login_password.Value.Length == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "model1", "$(function() { alert('Kindly Enter Both EmailId And Password.')});", true);
                }
                else
                {
                    string uname = login_username.Value.Trim();
                    string pass = login_password.Value.Trim();
                    string password = Encryptr.Encrypt(pass);
                    ithelpdeskEntities db = new ithelpdeskEntities();
                    var user = db.AdminTables.FirstOrDefault(x => (x.EmailId == uname) && (x.Password == password) && (x.IsActive == true));

                    if (user != null)
                    {
                        if (user.RoleTable.RoleName.ToLower() == "admin")
                        {
                            Session["UserId"] = user.AdminId.ToString();
                            Session["Role"] = "admin";
                            Response.Redirect("~/Admin/DashBoard.aspx", false);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "model1", "$(function() { alert('Kindly Check Your EmailId And Password')});", true);
                    }
                }
            }
            catch { }
        }
    }
}