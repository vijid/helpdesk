﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class MyProfile : System.Web.UI.Page
    {
        int userid = 0;

        ithelpdeskEntities db = new ithelpdeskEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if(!IsPostBack)
            {
                getAdminprofile();
            }
        }

        public void getAdminprofile()
        {
            var admin_det = db.AdminTables.FirstOrDefault(x => x.AdminId == userid);
            txtAdminname.Text = admin_det.AdminName.ToString();
            txtemailid.Text = admin_det.EmailId.ToString();          
            txtcontactno.Text = admin_det.ContactNo.ToString();
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            string adminname = txtAdminname.Text;
            string email = txtemailid.Text;           
            string contactno = txtcontactno.Text;
            var adm_del = db.AdminTables.FirstOrDefault(x => x.AdminId == userid);
            adm_del.AdminName = adminname;
            adm_del.EmailId = email;            
            adm_del.ContactNo = contactno;
            db.SaveChanges();
            Clear();
            ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('My Profile Updated Successfully!!'); });", true);
            getAdminprofile();
        }


        public void Clear()
        {
            txtAdminname.Text = "";
            txtemailid.Text = "";           
            txtcontactno.Text = "";
        }
    }
}