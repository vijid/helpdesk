﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class ConsolidatedAssetTypeReport : System.Web.UI.Page
    {
        int userid = 0;
        protected double ProcessPercentage = 0.00;
        protected double TotalCalls = 0.00, TypeCount = 0.00, NotAttendedCalls = 0.00;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDDl();
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                //BindData();
            }
        }
        ithelpdeskEntities db = new ithelpdeskEntities();
        public void BindDDl()
        {
            try
            {
                AssetTypeDDL.Items.Clear();
                var AssetType = db.AssetTypeTables.Where(x => x.IsActive == true).OrderBy(x => x.AssetName).ToList();
                AssetTypeDDL.Items.Insert(0, "Select Asset Type");
                AssetTypeDDL.DataSource = AssetType;
                AssetTypeDDL.DataTextField = "AssetName";
                AssetTypeDDL.DataValueField = "AssetTypeId";
                AssetTypeDDL.DataBind();
            }
            catch { }
        }
        protected void AssetTypeDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int AssetTypeId = 0;
                AssetTypeId = Convert.ToInt32(AssetTypeDDL.SelectedValue);
                var AssetDt = db.AssetTables.Where(x => x.AssetTypeId == AssetTypeId && x.IsActive == true).OrderBy(x => x.AssetNo).ToList();
                AssetDDL.Items.Clear();
                AssetDDL.Items.Insert(0, "Select Asset No");
                AssetDDL.DataSource = AssetDt;
                AssetDDL.DataTextField = "AssetNo";
                AssetDDL.DataValueField = "AssetId";
                AssetDDL.DataBind();
            }
            catch { }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch { }
        }

        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ViewStatusListView.DataSource = null;
                ViewStatusListView.DataBind();
            }
            catch { }
        }
        public void BindData()
        {
            try
            {
                ViewStatusListView.DataSource = null;
                ViewStatusListView.DataBind();
                ProcessPercentage = 0.00;
                TypeCount = 0.00;
                NotAttendedCalls = 0.00;
                int AssetValue = 0;
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);

                AssetValue = Convert.ToInt32(AssetDDL.SelectedValue);

                TotalCalls = db.TicketTables.Where(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal).Count();



                if (TypeDDL.SelectedItem.Text == "Completed")
                {
                    var StatusDetails = (from tck in db.TicketTables
                                         where
                                             tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal && tck.AssetId == AssetValue &&
                                             tck.IsClosed == true
                                         select
                                             new
                                             {
                                                 ticketId = tck.TicketId,
                                                 TicketNo = tck.TicketNo,
                                                 PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                                 UnitName = tck.UsersTable.UnitTable.UnitName,
                                                 UnitAddress = tck.UsersTable.UnitTable.UnitAddress,
                                                 UserName = tck.UsersTable.Name,
                                                 UserDetail = tck.UsersTable.EmailId,
                                                 AssetNo = tck.AssetTable.AssetNo,
                                                 ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                                 Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                                 Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                                 StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                                 CreatedDate = tck.CreatedDate,
                                                 AttendedDate = tck.AttendedDate,
                                                 AttendedBy = tck.AttendedBy != 0 ? db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName : "-",
                                                 ClosedDate = tck.ClosedDate
                                             }).ToList();
                    TypeCount = StatusDetails.Count();
                    NotAttendedCalls = StatusDetails.Where(x => x.AttendedBy == "-").Count();
                    ProcessPercentage = (TypeCount / TotalCalls) * 100;

                    if (StatusDetails.Count() != 0)
                    {
                        ViewStatusListView.DataSource = StatusDetails;
                        ViewStatusListView.DataBind();
                    }
                }
                else if (TypeDDL.SelectedItem.Text == "Pending")
                {
                    var StatusDetails = (from tck in db.TicketTables
                                         where
                                             tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal && tck.AssetId == AssetValue
                                         && tck.IsClosed == false
                                         select
                                             new
                                             {
                                                 ticketId = tck.TicketId,
                                                 TicketNo = tck.TicketNo,
                                                 PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                                 UnitName = tck.UsersTable.UnitTable.UnitName,
                                                 UnitAddress = tck.UsersTable.UnitTable.UnitAddress,
                                                 UserName = tck.UsersTable.Name,
                                                 UserDetail = tck.UsersTable.EmailId,
                                                 AssetNo = tck.AssetTable.AssetNo,
                                                 ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                                 Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                                 Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                                 StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                                 CreatedDate = tck.CreatedDate,
                                                 AttendedDate = tck.AttendedDate,
                                                 AttendedBy = tck.AttendedBy != 0 ? db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName : "-",
                                                 ClosedDate = tck.ClosedDate
                                             }).ToList();
                    TypeCount = StatusDetails.Count();
                    NotAttendedCalls = StatusDetails.Where(x => x.AttendedBy == "-").Count();
                    ProcessPercentage = (TypeCount / TotalCalls) * 100;

                    if (StatusDetails.Count() != 0)
                    {
                        ViewStatusListView.DataSource = StatusDetails;
                        ViewStatusListView.DataBind();
                    }
                }
                else
                {
                    var StatusDetails = (from tck in db.TicketTables
                                         where
                                             tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal && tck.AssetId == AssetValue &&
                                             tck.IsClosed == false && tck.IsRecurring == true
                                         select
                                             new
                                             {
                                                 ticketId = tck.TicketId,
                                                 TicketNo = tck.TicketNo,
                                                 PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                                 UnitName = tck.UsersTable.UnitTable.UnitName,
                                                 UnitAddress = tck.UsersTable.UnitTable.UnitAddress,
                                                 UserName = tck.UsersTable.Name,
                                                 UserDetail = tck.UsersTable.EmailId,
                                                 AssetNo = tck.AssetTable.AssetNo,
                                                 ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                                 Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                                 Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                                 StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                                 CreatedDate = tck.CreatedDate,
                                                 AttendedDate = tck.AttendedDate,
                                                 AttendedBy = tck.AttendedBy != 0 ? db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName : "-",
                                                 ClosedDate = tck.ClosedDate
                                             }).ToList();
                    TypeCount = StatusDetails.Count();
                    NotAttendedCalls = StatusDetails.Where(x => x.AttendedBy == "-").Count();
                    ProcessPercentage = (TypeCount / TotalCalls) * 100;

                    if (StatusDetails.Count() != 0)
                    {
                        ViewStatusListView.DataSource = StatusDetails;
                        ViewStatusListView.DataBind();
                    }
                }
            }
            catch { }
        }
    }
}