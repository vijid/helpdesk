﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="PartMaster.aspx.cs" Inherits="HelpDesk.Admin.PartMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            $(document).ready(function () {
                $('#prod_table').DataTable();
            });
        }
    </script>
    <script type="text/javascript">
        function Validation() {
            var partname = document.getElementById('<%=PartNameText.ClientID%>').value;
            if (partname == '') {
                alert('Kindly Enter Part Name');
                return false;
            }
            return true;
        }
    </script>
    <script type="text/javascript">
        function ValidationUpdate() {
            var partname = document.getElementById('<%=EditPartNameText.ClientID%>').value;
            if (partname == '') {
                alert('Kindly Enter Part Name');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManagePartUpdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManagePartprogress" AssociatedUpdatePanelID="ManagePartUpdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="page_title">Manage Part Details</h1>
                    </div>
                    <div class="col-md-2 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#bs_modal_regular">Add Part Details</a>
                    </div>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-offset-1 col-lg-10">
                            <div class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table info_table" id="prod_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Part Name</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="PartListView" ItemType="HelpDesk.PartMasterTable" SelectMethod="GetPart" runat="server" OnItemCommand="PartListView_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.PartName%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:LinkButton ID="lkbtnedit" runat="server" class="btn btn-inverse btn-sm" CommandName="RowEdit" CommandArgument='<%#:Item.PartMasterId%>'><span class="fa fa-pencil-square-o fa-lg"></span>Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtndelete" runat="server" class="btn btn-danger btn-sm" CommandName="RowDelete" CommandArgument='<%#:Item.PartMasterId%>' OnClientClick="javascript:return confirm('Are you sure you want to delete this record?')"><span class="fa fa-pencil-square-o fa-lg"></span>Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
    <div class="modal fade" id="bs_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Add Part Details</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="AddPartUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="AddUpdateProgress" AssociatedUpdatePanelID="AddPartUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="EngineerName">Part Name</label>
                                                <asp:TextBox runat="server" ID="PartNameText" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="saveBtn" CssClass="btn btn-success" runat="server" Text="Save" OnClick="saveBtn_Click" OnClientClick="return Validation()" />
                                        <asp:Button ID="cancelbutton" CssClass="btn btn-default" Text="Cancel" data-dismiss="modal" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Edit_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Edit Part Details</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="editPartUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="EditUpdateProgress" AssociatedUpdatePanelID="editPartUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="EngineerName">Part Name</label>
                                                <asp:TextBox runat="server" ID="EditPartNameText" CssClass="form-control" />
                                                <asp:HiddenField ID="partMasterHdn" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="updateBtn" CssClass="btn btn-success" runat="server" Text="Update" OnClick="updateBtn_Click" OnClientClick="return ValidationUpdate()" />
                                        <input type="reset" class="btn btn-default" value="Cancel" data-dismiss="modal" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
