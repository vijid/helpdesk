﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace HelpDesk.Admin
{
    public partial class DashBoard : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDDl();
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                BindData();
                getTickets();
            }
        }

        ithelpdeskEntities db = new ithelpdeskEntities();

        public void BindDDl()
        {
            try
            {
                UnitDDl.Items.Clear();
                var unitDt = db.UnitTables.Where(x => x.IsActive == true).OrderBy(x => x.UnitName).ToList();
                UnitDDl.DataSource = unitDt;
                UnitDDl.Items.Insert(0, "All Units");
                UnitDDl.DataTextField = "UnitName";
                UnitDDl.DataValueField = "UnitId";
                UnitDDl.DataBind();

                //ListUnitDDL.Items.Clear();
                //ListUnitDDL.DataSource = unitDt;
                //ListUnitDDL.Items.Insert(0, "All Units");
                //ListUnitDDL.DataTextField = "UnitName";
                //ListUnitDDL.DataValueField = "UnitId";
                //ListUnitDDL.DataBind();

                //StatustypeDDL.Items.Clear();
                //var StatusDt = db.StatusTables.Where(x => x.IsActive == true).OrderBy(x => x.StatusName).ToList();
                //StatustypeDDL.Items.Insert(0, "Pending");
                //StatustypeDDL.DataSource = StatusDt;
                //StatustypeDDL.DataTextField = "StatusName";
                //StatustypeDDL.DataValueField = "StatusId";
                //StatustypeDDL.DataBind();
            }
            catch { }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                BindData();
                getTickets();
            }
            catch { }
        }

        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                    ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                }
                catch { }
            }
            catch { }
        }


        public void BindData()
        {
            try
            {
                int UnitId = 0;
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);

                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {
                    ////////////NO OF CALLS////////////
                    var noofcalls = db.TicketTables.Where(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal).ToList();
                    //noofcallreportlbl.Text = noofcalls.Count.ToString();
                    noofcallreportlnk.Text = noofcalls.Count.ToString();

                    /////////////NO OF CALLS COMPLETED/////////////
                    int completed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId == 5 && x.IsClosed == false);
                    //noofcallscompletedlbl.Text = Convert.ToInt32(completed).ToString("0");
                    noofcallscompletedlnk.Text = Convert.ToInt32(completed).ToString("0");

                    //////////////NO OF CALLS PENDING////////////////
                    //int pending = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId != 5 && x.AttendedDate != null && x.IsClosed == false);
                    int pending = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId != 5 && x.ClosedDate == null && x.IsClosed == false);
                    //noofcallspendinglbl.Text = Convert.ToInt32(pending).ToString("0");
                    noofcallspendinglnk.Text = Convert.ToInt32(pending).ToString("0");

                    ////////////////NO OF CALLS CLOSED////////////////
                    int closed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == true);
                    //completeClosedLbl.Text = Convert.ToInt32(closed).ToString("0");
                    completeClosedLnk.Text = Convert.ToInt32(closed).ToString("0");
                    /////////////////NO OF CALLS INPROGRESS//////////////
                    //int inprogress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == false);
                    //noofcallsinprogresslbl.Text = Convert.ToInt32(inprogress).ToString("0");

                    //////////////////NO OF CALLS UNATTENDED////////////////////////
                    int unattend = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.AttendedDate == null);
                    //noofcallsunattendedlbl.Text = Convert.ToInt32(unattend).ToString("0");
                    noofcallsunattendedlnk.Text = Convert.ToInt32(unattend).ToString("0");
                    ///////////////NO OF CALLS REOCCURED COMPLETED///////////
                    int ReOccur = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId == 5);
                    //reoccuredCompletecallsLbl.Text = Convert.ToInt32(ReOccur).ToString("0");
                    reoccuredCompletecallsLnk.Text = Convert.ToInt32(ReOccur).ToString("0");

                    /////////////////NO OF CALLS REOCCURED INPROGRESS/////////////
                    int ReOccurProgress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId != 5 && x.IsClosed != true);
                    //ReoccuerInProgressLbl.Text = Convert.ToInt32(ReOccurProgress).ToString("0");
                    ReoccuerInProgressLnk.Text = Convert.ToInt32(ReOccurProgress).ToString("0");
                }
                else
                {
                    ////////////NO OF CALLS////////////
                    var noofcalls = db.TicketTables.Where(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.UsersTable.UnitId == UnitId).ToList();
                    //noofcallreportlbl.Text = noofcalls.Count.ToString();
                    noofcallreportlnk.Text = noofcalls.Count.ToString();

                    /////////////NO OF CALLS COMPLETED/////////////
                    int completed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId == 5 && x.UsersTable.UnitId == UnitId && x.IsClosed == false);
                    //noofcallscompletedlbl.Text = Convert.ToInt32(completed).ToString("0");
                    noofcallscompletedlnk.Text = Convert.ToInt32(completed).ToString("0");

                    //////////////NO OF CALLS PENDING////////////////
                    //int pending = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId != 5 && x.AttendedDate != null && x.IsClosed == false && x.UsersTable.UnitId == UnitId);
                    int pending = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId != 5 && x.ClosedDate == null && x.IsClosed == false && x.UsersTable.UnitId == UnitId);
                    //noofcallspendinglbl.Text = Convert.ToInt32(pending).ToString("0");
                    noofcallspendinglnk.Text = Convert.ToInt32(pending).ToString("0");

                    ////////////////NO OF CALLS CLOSED////////////////
                    int closed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == true && x.UsersTable.UnitId == UnitId);
                    //completeClosedLbl.Text = Convert.ToInt32(closed).ToString("0");
                    completeClosedLnk.Text = Convert.ToInt32(closed).ToString("0");

                    /////////////////NO OF CALLS INPROGRESS//////////////
                    //int inprogress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == false && x.UsersTable.UnitId==UnitId);
                    //noofcallsinprogresslbl.Text = Convert.ToInt32(inprogress).ToString("0");

                    //////////////////NO OF CALLS UNATTENDED////////////////////////
                    int unattend = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.AttendedDate == null && x.UsersTable.UnitId == UnitId);
                    //noofcallsunattendedlbl.Text = Convert.ToInt32(unattend).ToString("0");
                    noofcallsunattendedlnk.Text = Convert.ToInt32(unattend).ToString("0");
                    ///////////////NO OF CALLS REOCCURED COMPLETED///////////
                    int ReOccur = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId == 5 && x.UsersTable.UnitId == UnitId);
                    //reoccuredCompletecallsLbl.Text = Convert.ToInt32(ReOccur).ToString("0");
                    reoccuredCompletecallsLnk.Text = Convert.ToInt32(ReOccur).ToString("0");

                    /////////////////NO OF CALLS REOCCURED INPROGRESS/////////////
                    int ReOccurProgress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId != 5 && x.IsClosed != true && x.UsersTable.UnitId == UnitId);
                    //ReoccuerInProgressLbl.Text = Convert.ToInt32(ReOccurProgress).ToString("0");
                    ReoccuerInProgressLnk.Text = Convert.ToInt32(ReOccurProgress).ToString("0");
                }
            }
            catch { }
        }

        public void getTickets()
        {

            #region old code
            //string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
            //string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
            //DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
            //DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
            //int Uid = 0;
            //int statusid = 0;
            //if (StatustypeDDL.SelectedValue == "Pending")
            //{
            //    statusid = 0;
            //}
            //else
            //{
            //    statusid = Convert.ToInt32(StatustypeDDL.SelectedValue);
            //}

            //if (ListUnitDDL.SelectedValue == "All Units")
            //{
            //    Uid = 0;
            //    if (statusid == 0)
            //    {
            //        //return db.TicketTables.OrderByDescending(x => x.TicketId).ToList();
            //        var ticketLst = (from u in db.TicketTables
            //                         where
            //                                 u.StatusId == statusid && u.ClosedDate == null && u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal
            //                         select new
            //                          {
            //                              TicketId = u.TicketId,
            //                              TicketNo = u.TicketNo,
            //                              UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
            //                              UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
            //                              PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
            //                              ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
            //                              CreatedDate = u.CreatedDate,
            //                              AttendedDate = u.AttendedDate,
            //                              AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
            //                              ClosedDate = u.ClosedDate,
            //                              Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
            //                              StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

            //                          }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


            //        TicketListView.DataSource = ticketLst;
            //        TicketListView.DataBind();
            //    }
            //    else
            //    {
            //        var ticketLst = (from u in db.TicketTables
            //                         where
            //                                 u.StatusId == statusid && u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal
            //                         select new
            //                         {
            //                             TicketId = u.TicketId,
            //                             TicketNo = u.TicketNo,
            //                             UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
            //                             UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
            //                             PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
            //                             ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
            //                             CreatedDate = u.CreatedDate,
            //                             AttendedDate = u.AttendedDate,
            //                             AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
            //                             ClosedDate = u.ClosedDate,
            //                             Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
            //                             StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

            //                         }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


            //        TicketListView.DataSource = ticketLst;
            //        TicketListView.DataBind();
            //    }
            //}
            //else
            //{
            //    Uid = Convert.ToInt32(ListUnitDDL.SelectedValue);


            //    if (statusid == 0)
            //    {
            //        //return db.TicketTables.Where(x => x.UsersTable.UnitId == Uid).OrderByDescending(x => x.TicketId).ToList();
            //        var ticketLst = (from u in db.TicketTables
            //                         where u.UsersTable.UnitId == Uid && u.StatusId == statusid && u.ClosedDate == null && u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal
            //                         select new
            //                         {
            //                             TicketId = u.TicketId,
            //                             TicketNo = u.TicketNo,
            //                             UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
            //                             UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
            //                             PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
            //                             ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
            //                             CreatedDate = u.CreatedDate,
            //                             AttendedDate = u.AttendedDate,
            //                             AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
            //                             ClosedDate = u.ClosedDate,
            //                             Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
            //                             StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

            //                         }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();
            //        TicketListView.DataSource = ticketLst;
            //        TicketListView.DataBind();
            //    }
            //    else
            //    {
            //        var ticketLst = (from u in db.TicketTables
            //                         where u.UsersTable.UnitId == Uid && u.StatusId == statusid && u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal
            //                         select new
            //                         {
            //                             TicketId = u.TicketId,
            //                             TicketNo = u.TicketNo,
            //                             UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
            //                             UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
            //                             PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
            //                             ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
            //                             CreatedDate = u.CreatedDate,
            //                             AttendedDate = u.AttendedDate,
            //                             AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
            //                             ClosedDate = u.ClosedDate,
            //                             Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
            //                             StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

            //                         }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();
            //        TicketListView.DataSource = ticketLst;
            //        TicketListView.DataBind();
            //    }
            //}
            #endregion
        }
        protected void TicketListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowView")
                {
                    int ticketid = Convert.ToInt32(e.CommandArgument);
                    Response.Redirect("~/Admin/EditingCalls.aspx?ticketid=" + ticketid + "");
                }
            }
            catch { }
        }

        protected void TicketListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                HiddenField ticketid_hf = (HiddenField)e.Item.FindControl("hfticketid");
                Label createdate_lbl = (Label)e.Item.FindControl("lblcreatedate");
                Label closeddate_lbl = (Label)e.Item.FindControl("lblcloseddate");
                Label Duration_Label = (Label)e.Item.FindControl("durationLbl");
                DateTime currentdate = DateTime.Now.Date;

                if (currentdate == Convert.ToDateTime(createdate_lbl.Text).Date)
                {
                    HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                    Cell.Style.Add("background-color", "#99CCFF");
                }
                if (currentdate.AddDays(-1) == Convert.ToDateTime(createdate_lbl.Text).Date)
                {
                    HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                    Cell.Style.Add("background-color", "#FF0000");
                }

                if (Convert.ToDateTime(createdate_lbl.Text).Date <= currentdate.AddDays(-2))
                {
                    HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                    Cell.Style.Add("background-color", "#FFD58A");
                }
                DateTime startTime = Convert.ToDateTime(createdate_lbl.Text);
                DateTime endTime = DateTime.Now;
                if (closeddate_lbl.Text != "-")
                {
                    endTime = Convert.ToDateTime(closeddate_lbl.Text);
                }

                TimeSpan span = endTime.Subtract(startTime);
                //string year = "";
                //string month = "";
                string days = "";
                string hours = "";
                string mins = "";
                string sec = "";

                if (span.Days > 0)
                {
                    days = span.Days.ToString() + " Days ";
                }
                if (span.Hours > 0)
                {
                    hours = span.Hours.ToString() + " Hours ";
                }
                if (span.Minutes > 0)
                {
                    mins = span.Minutes.ToString() + " Minutes ";
                }
                if (span.Seconds > 0)
                {
                    sec = span.Seconds.ToString() + " Seconds ";
                }
                Duration_Label.Text = days + hours + mins + sec;
                Int32 ticketid = Convert.ToInt32(ticketid_hf.Value);
                var ticket_det = db.TicketTables.SingleOrDefault(x => x.TicketId == ticketid);
                if (ticket_det != null)
                {
                    if (ticket_det.IsClosed != null)
                    {
                        if (ticket_det.IsClosed == true)
                        {
                            HtmlTableRow Cell = (HtmlTableRow)e.Item.FindControl("RowVal");
                            Cell.Style.Add("background-color", "#56c1bd");
                        }
                    }
                }
            }
            catch { }
        }

        protected void ListUnitDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                getTickets();
            }
            catch { }
        }

        protected void StatustypeDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                getTickets();
            }
            catch { }
        }

        protected void noofcallspendinglnk_Click(object sender, EventArgs e)
        {
            try
            {
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
                int UnitId = 0;
                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {

                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal &&u.StatusId != 5 && u.ClosedDate == null && u.IsClosed == false
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
                else
                {
                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.UsersTable.UnitId == UnitId && u.StatusId != 5 && u.ClosedDate == null && u.IsClosed == false
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
            }
            catch { }
        }

        protected void noofcallreportlnk_Click(object sender, EventArgs e)
        {
            try 
            {
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
                int UnitId = 0;
                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {

                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
                else {
                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.UsersTable.UnitId == UnitId
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();

                }
            }
            catch { }
        }

        protected void noofcallscompletedlnk_Click(object sender, EventArgs e)
        {
            try 
            {
                //int completed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.StatusId == 5 && x.UsersTable.UnitId == UnitId && x.IsClosed == false);
                
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
                int UnitId = 0;
                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {

                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.StatusId ==5 &&u.IsClosed==false
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
                else
                {
                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.UsersTable.UnitId == UnitId && u.StatusId == 5 && u.IsClosed == false
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();

                }
            }
            catch { }
        }

        protected void completeClosedLnk_Click(object sender, EventArgs e)
        {
            try 
            {
                //int closed = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsClosed == true && x.UsersTable.UnitId == UnitId);
                
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
                int UnitId = 0;
                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {

                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.IsClosed == true
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
                else
                {
                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.UsersTable.UnitId == UnitId && u.IsClosed == true
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
            }
            catch { }
        }

        protected void noofcallsunattendedlnk_Click(object sender, EventArgs e)
        {
            try 
            {
                //int unattend = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.AttendedDate == null && x.UsersTable.UnitId == UnitId);
                
                
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
                int UnitId = 0;
                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {

                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.AttendedDate == null
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
                else
                {
                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.UsersTable.UnitId == UnitId && u.AttendedDate == null
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
            }
            catch { }
        }

        protected void ReoccuerInProgressLnk_Click(object sender, EventArgs e)
        {
            try 
            {
                //int ReOccurProgress = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId != 5 && x.IsClosed != true && x.UsersTable.UnitId == UnitId);

                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
                int UnitId = 0;
                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {

                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.IsRecurring == true && u.StatusId != 5 && u.IsClosed != true
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
                else
                {
                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.UsersTable.UnitId == UnitId && u.IsRecurring == true && u.StatusId != 5 && u.IsClosed != true
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
            }
            catch { }
        }

        protected void reoccuredCompletecallsLnk_Click(object sender, EventArgs e)
        {
            try 
            {
                //int ReOccur = db.TicketTables.Count(x => x.CreatedDate >= FromDateVal && x.CreatedDate <= ToDateVal && x.IsRecurring == true && x.StatusId == 5 && x.UsersTable.UnitId == UnitId);
                
                
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);
                int UnitId = 0;
                if (UnitDDl.SelectedValue == "All Units")
                {
                    UnitId = 0;
                }
                else
                {
                    UnitId = Convert.ToInt32(UnitDDl.SelectedValue);
                }
                if (UnitId == 0)
                {

                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.IsRecurring == true && u.StatusId == 5
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
                else
                {
                    var ticketLst = (from u in db.TicketTables
                                     where
                                             u.CreatedDate >= FromDateVal && u.CreatedDate <= ToDateVal && u.UsersTable.UnitId == UnitId && u.IsRecurring == true && u.StatusId == 5 
                                     select new
                                     {
                                         TicketId = u.TicketId,
                                         TicketNo = u.TicketNo,
                                         UserName = u.UsersTable == null ? "-" : u.UsersTable.UserName,
                                         UnitName = u.UsersTable == null ? "-" : u.UsersTable.UnitTable.UnitName,
                                         PartName = u.PartMasterId != 499 ? u.PartMasterTable.PartName : u.PartEntryText,
                                         ProblemDetails = u.ProblemId != 499 ? u.ProblemId == 0 ? "" : u.ProblemMasterTable.Description : u.ProblemText,
                                         CreatedDate = u.CreatedDate,
                                         AttendedDate = u.AttendedDate,
                                         AttendedBy = u.AttendedBy != 0 ? (u.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == u.AttendedBy).EngineerName) : "-",
                                         ClosedDate = u.ClosedDate,
                                         Status = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.StatusTable.StatusName,
                                         StatusRemarks = u.StatusId == 0 ? u.ClosedDate == null ? "Pending" : "Closed" : u.TicketProcessTables.FirstOrDefault(x => x.TicketId == u.TicketId && x.StatusId == u.StatusId).Remarks,

                                     }).OrderByDescending(x => x.TicketId).ThenBy(x => x.TicketNo).ToList();


                    TicketListView.DataSource = ticketLst;
                    TicketListView.DataBind();
                }
            }
            catch { }
        }
    }

}