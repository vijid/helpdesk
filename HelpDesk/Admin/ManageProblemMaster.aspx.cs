﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class ManageProblemMaster : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDDL();
            }
            BindProblemDetail();
        }
        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<ProblemMasterTable> getProblems()
        {
            return db.ProblemMasterTables.Where(x => x.IsActive==true).ToList();
        }

        public void BindProblemDetail()
        {
            try 
            {
                ProblemListView.DataSource = null;
                ProblemListView.DataBind();
                var Dt = (from lst in db.ProblemMasterTables
                          where
                              lst.IsActive == true
                          orderby lst.ProblemId
                          select new
                          {
                              ProblemId = lst.ProblemId,
                              Description = lst.Description,
                              PartName = lst.PartMasterTable.PartName
                          }
                              ).ToList();

                ProblemListView.DataSource = Dt;
                ProblemListView.DataBind();
            }
            catch { }
        }

        public void BindDDL()
        {
            try
            {
                var PartTypeDt = db.PartMasterTables.Where(x => x.IsActive == true).OrderBy(x => x.PartName).ToList();

                PartTypeDDL.DataSource = PartTypeDt;
                PartTypeDDL.DataValueField = "PartMasterId";
                PartTypeDDL.DataTextField = "PartName";
                PartTypeDDL.DataBind();
                EditPartDDL.DataSource = PartTypeDt;
                EditPartDDL.DataValueField = "PartMasterId";
                EditPartDDL.DataTextField = "PartName";
                EditPartDDL.DataBind();
            }
            catch { }
        }
        protected void ProblemListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int ProblemId = Convert.ToInt32(e.CommandArgument);
                    var Problem_det = db.ProblemMasterTables.FirstOrDefault(x => x.ProblemId == ProblemId);
                    EditProblemIdHdn.Value = Problem_det.ProblemId.ToString();
                    EditPartDDL.SelectedValue = Problem_det.PartMasterId.ToString();
                    EditDescriptionText.Text = Problem_det.Description;
                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowDelete")
                {
                    int ProblemId = Convert.ToInt32(e.CommandArgument);
                    var Prob_det = db.ProblemMasterTables.FirstOrDefault(x => x.ProblemId == ProblemId);
                    Prob_det.IsActive = false;
                    db.SaveChanges();
                    BindProblemDetail();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Problem Details Deleted Successfully!!'); });", true);
                    ProblemListView.DataBind();
                }
            }
            catch { }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                ProblemMasterTable ProbMasterTab = new ProblemMasterTable();
                ProbMasterTab.PartMasterId = Convert.ToInt32(PartTypeDDL.SelectedValue);
                ProbMasterTab.Description = DescriptionText.Text;
                ProbMasterTab.IsActive = true;
                db.ProblemMasterTables.Add(ProbMasterTab);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                ProblemListView.DataBind();
                Clear();
                BindProblemDetail();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Problem Detail Added Successfully!!'); });", true);
            }
            catch { }
        }
        public void Clear()
        {
            PartTypeDDL.SelectedIndex = 0;
            DescriptionText.Text = "";
            EditPartDDL.SelectedIndex = 0;
            EditDescriptionText.Text = "";
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int ProblemId = Convert.ToInt32(EditProblemIdHdn.Value);
                var ProblemDet = db.ProblemMasterTables.FirstOrDefault(x => x.ProblemId == ProblemId);
                ProblemDet.PartMasterId = Convert.ToInt32(EditPartDDL.SelectedValue);
                ProblemDet.Description = EditDescriptionText.Text;
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                ProblemListView.DataBind();
                Clear();
                BindProblemDetail();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Problem Details Updated Successfully!!'); });", true);
            }
            catch { }
        }
    }
}