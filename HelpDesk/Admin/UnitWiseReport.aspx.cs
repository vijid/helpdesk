﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class UnitWiseReport : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                GetUnits();
            }
        }

        protected string TotalTicket = "", TotUnitCall = "", Completed = "";

        ithelpdeskEntities db = new ithelpdeskEntities();

        public void GetUnits()
        {
            try
            {
                var UnitDetail = db.UnitTables.Where(x => x.IsActive == true).ToList();

                UnitDDList.Items.Clear();
                UnitDDList.Items.Insert(0, new ListItem("Select Unit", "0"));
                UnitDDList.DataSource = UnitDetail;
                UnitDDList.DataValueField = "UnitId";
                UnitDDList.DataTextField = "UnitName";
                UnitDDList.DataBind();
            }
            catch { }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 unitid = Convert.ToInt32(UnitDDList.SelectedValue);
                Int32 status = Convert.ToInt32(StatustypeDDL.SelectedValue);
                TotUnitCall = ""; Completed = "";
                string FromDateValstr = DateTime.ParseExact(fromdateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(ToDateTimeTxt.Text, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);

                // db.TicketTables.Where(x => x.UsersTable.UnitId == unitid && x.CreatedDate >= FromDateVal && x.CreatedDate<=ToDateVal ).ToList();

                var tickets = (from tck in db.TicketTables
                               where tck.UsersTable.UnitId == unitid && tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal
                               select new
                               {
                                   ticketId = tck.TicketId,
                                   TicketNo = tck.TicketNo,
                                   UserName = tck.UsersTable.Name,
                                   PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                   AssetNo = tck.AssetTable.AssetNo,
                                   ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                   Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                   Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                   StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                   CreatedDate = tck.CreatedDate,
                                   AttendedDate = tck.AttendedDate,
                                   AttendedBy = tck.AttendedBy != 0 ? db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName : "-",
                                   ClosedDate = tck.ClosedDate,
                                   IsClosed = tck.IsClosed,
                               }).OrderByDescending(x => x.ticketId).ToList();

                double totalticket = tickets.Count();
                //int completedticket = 0, pendingticket = 0;
                double completedticket = 0.00, pendingticket = 0.00;
                if (status == 1)
                {
                    tickets = tickets.Where(x => x.IsClosed == false).ToList();

                    pendingticket = tickets.Where(x => x.IsClosed != true).Count();
                    double tottickets = Convert.ToDouble(((pendingticket / totalticket) * 100));
                    TotalTicket = Convert.ToDouble(tottickets).ToString("0") + " % Pending Tickets ";
                    TotUnitCall = "Total Calls :" + totalticket.ToString();
                    Completed = "Processed Calls :" + pendingticket.ToString();
                }
                else if (status == 2)
                {
                    tickets = tickets.Where(x => x.IsClosed == true).ToList();

                    completedticket = tickets.Where(x => x.IsClosed == true).Count();
                    double tottickets = Convert.ToDouble(((completedticket / totalticket) * 100));
                    TotalTicket = Convert.ToDouble(tottickets).ToString("0") + " % Completed Tickets";
                    TotUnitCall = "Total Calls :" + totalticket.ToString();
                    Completed = "Processed Calls :" + completedticket.ToString();
                }

                ViewTicketListView.DataSource = tickets;
                ViewTicketListView.DataBind();
            }
            catch { }
        }

        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            UnitDDList.SelectedValue = "0";
            fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            StatustypeDDL.SelectedValue = "0";
        }

        protected void ViewTicketListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                Label createdate_lbl = (Label)e.Item.FindControl("lblcreatedate");
                Label closeddate_lbl = (Label)e.Item.FindControl("lblcloseddate");
                Label Duration_Label = (Label)e.Item.FindControl("durationLbl");
                DateTime startTime = Convert.ToDateTime(createdate_lbl.Text);
                DateTime endTime = DateTime.Now;
                if (closeddate_lbl.Text != "-")
                {
                    endTime = Convert.ToDateTime(closeddate_lbl.Text);
                }

                TimeSpan span = endTime.Subtract(startTime);
                //string year = "";
                //string month = "";
                string days = "";
                string hours = "";
                string mins = "";
                string sec = "";

                if (span.Days > 0)
                {
                    days = span.Days.ToString() + " Days ";
                }
                if (span.Hours > 0)
                {
                    hours = span.Hours.ToString() + " Hours ";
                }
                if (span.Minutes > 0)
                {
                    mins = span.Minutes.ToString() + " Minutes ";
                }
                if (span.Seconds > 0)
                {
                    sec = span.Seconds.ToString() + " Seconds ";
                }
                Duration_Label.Text = days + hours + mins + sec;
            }
            catch { }
        }
    }
}