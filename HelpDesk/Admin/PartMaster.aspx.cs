﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class PartMaster : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
        }
        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<PartMasterTable> GetPart()
        {
            return db.PartMasterTables.Where(x => x.IsActive == true).OrderBy(x => x.PartName).ToList();
        }
        protected void PartListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int PartId = Convert.ToInt32(e.CommandArgument);
                    var PartDt = db.PartMasterTables.FirstOrDefault(x => x.PartMasterId == PartId);
                    EditPartNameText.Text = PartDt.PartName.ToString();
                    partMasterHdn.Value = PartDt.PartMasterId.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowDelete")
                {
                    int PartId = Convert.ToInt32(e.CommandArgument);
                    var PartDt = db.PartMasterTables.FirstOrDefault(x => x.PartMasterId == PartId);
                    PartDt.IsActive = false;
                    db.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Part Details Deleted Successfully!!'); });", true);
                    PartListView.DataBind();
                }
            }
            catch { }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                PartMasterTable pmtab = new PartMasterTable();
                pmtab.PartName = PartNameText.Text;
                pmtab.IsActive = true;
                db.PartMasterTables.Add(pmtab);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                PartListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Part Details Added Successfully!!'); });", true);
            }
            catch { }
        }
        public void Clear()
        {
            PartNameText.Text = "";
            EditPartNameText.Text = "";
        }
        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int PartId = Convert.ToInt32(partMasterHdn.Value);
                var PartDt = db.PartMasterTables.FirstOrDefault(x => x.PartMasterId == PartId);
                PartDt.PartName = EditPartNameText.Text;
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                PartListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Part Details Updated Successfully!!'); });", true);
            }
            catch { }
        }
    }
}