﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="HelpDesk.Admin.DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- dashboard functions -->
    <script src="<%=ResolveClientUrl("~/assets/js/apps/dashboard.js")%>"></script>
    <script src="<%=ResolveClientUrl("~/assets/js/jquery.maskedinput.js")%>"></script>
    <script type="text/javascript">

        <%--function Validation() {
            if (document.getElementById('<%=StatustypeDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Status Type Name");
                return false;
            }
            return true;
        }--%>
        function pageLoad() {
            $('#dt_reportTable').DataTable({
                "sDom":
                    '<"well well-sm"<"row"<"col-md-4 clearfix"l><"col-md-8 clearfix"fT>r>>' +
                    't' +
                    '<"row"<"col-md-5 clearfix"i><"col-md-7 clearfix"p>>',
                tableTools: {
                    "sSwfPath": "../../assets/lib/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
                }
            });
            $(document).ready(function () {
                $('table.display').dataTable();
            });


            $(document).ready(
         function () {
             $('#<%=fromdateTimeTxt.ClientID %>').click(
                function () {
                    $("#<%=fromdateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
                });

             $('#<%=fromdateTimeTxt.ClientID %>').keydown(
              function (e) {
                  var code = e.keyCode || e.which;
                  if (code === 9) {
                      $("#<%=fromdateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
                      $("#<%=ToDateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
                  }
              });
         });
          $(document).ready(
     function () {
         $('#<%=ToDateTimeTxt.ClientID %>').click(
            function () {
                $("#<%=ToDateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
            });

         $('#<%=ToDateTimeTxt.ClientID %>').keydown(
              function (e) {
                  var code = e.keyCode || e.which;
                  if (code === 9) {
                      $("#<%=ToDateTimeTxt.ClientID %>").mask("99/99/9999 99:99");
                  }
              });
     });
      }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="dashupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageAssetprogress" AssociatedUpdatePanelID="dashupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page_title">Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="page_content">

                <div class="panel panel-default">
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <label>Select Unit Name</label>
                            <asp:DropDownList ID="UnitDDl" data-placeholder="Select Unit Name"
                                AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                <asp:ListItem Selected="True" Value="0">All Units</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <label>From Date Time(Ex: 01/01/2016 06:00)</label>
                            <asp:TextBox ID="fromdateTimeTxt" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy hh:mm"></asp:TextBox>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <label>To Date Time(Ex: 01/01/2016 23:00)</label>
                            <asp:TextBox ID="ToDateTimeTxt" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy hh:mm"></asp:TextBox>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-lg-3">
                            <label>&nbsp;</label>
                            <div id="btn" class="Input-group">
                                <asp:Button ID="SearchButton" CssClass="btn btn-success" runat="server" Text="Search" OnClick="SearchButton_Click" OnClientClick="return Validation()" />
                                <asp:Button ID="CancelBtn" CssClass="btn btn-default" runat="server" Text="Cancel" OnClick="CancelBtn_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_c"><i class="ion-ios7-box"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <%--<asp:Label ID="noofcallreportlbl" runat="server" /></span>--%>
                                                    <asp:LinkButton ID="noofcallreportlnk" runat="server" OnClick="noofcallreportlnk_Click"></asp:LinkButton>
                                                    <span class="stat_name">NO. OF CALLS REPORTED</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_a"><i class="ion-ios7-box"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <%--<asp:Label ID="noofcallscompletedlbl" runat="server" /></span>--%>
                                                    <asp:LinkButton ID="noofcallscompletedlnk" runat="server" OnClick="noofcallscompletedlnk_Click"></asp:LinkButton>
                                                    <span class="stat_name">NO.OF CALLS COMPLETED AND NOT CLOSED</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_g"><i class="ion-ios7-compose"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <%--<asp:Label ID="completeClosedLbl" runat="server" /></span>--%>
                                                    <asp:LinkButton ID="completeClosedLnk" runat="server" OnClick="completeClosedLnk_Click"></asp:LinkButton>
                                                    <span class="stat_name">NO. OF CALLS COMPLETED AND CLOSED</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_b"><i class="ion-android-social"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <%--<asp:Label ID="noofcallspendinglbl" runat="server" /></span>--%>
                                                    <asp:LinkButton ID="noofcallspendinglnk" OnClick="noofcallspendinglnk_Click" runat="server"></asp:LinkButton>
                                                    <span class="stat_name">NO. OF CALLS PENDING</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_f"><i class="fa fa-briefcase"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="noofcallsinprogresslbl" runat="server" /></span>
                                                <span class="stat_name">No. of Calls in Inprogress </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_g"><i class="ion-ios7-compose"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <%--<asp:Label ID="noofcallsunattendedlbl" runat="server" /></span>--%>
                                                    <asp:LinkButton ID="noofcallsunattendedlnk" runat="server" OnClick="noofcallsunattendedlnk_Click"></asp:LinkButton>
                                                    <span class="stat_name">NO. OF CALLS UNATTENDED</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_b"><i class="ion-android-social"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <%--<asp:Label ID="ReoccuerInProgressLbl" runat="server" /></span>--%>
                                                    <asp:LinkButton ID="ReoccuerInProgressLnk" runat="server" OnClick="ReoccuerInProgressLnk_Click"></asp:LinkButton>
                                                    <span class="stat_name">NO. OF CALLS RE-OCCUR INPROGRESS</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_b"><i class="ion-android-social"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <%--<asp:Label ID="reoccuredCompletecallsLbl" runat="server" /></span>--%>
                                                    <asp:LinkButton ID="reoccuredCompletecallsLnk" runat="server" OnClick="reoccuredCompletecallsLnk_Click"></asp:LinkButton>
                                                    <span class="stat_name">NO. OF CALLS RE-OCCUR COMPLETED</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--      <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_f"><i class="fa fa-briefcase"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="Label2" runat="server" /></span>
                                                <span class="stat_name">No. of Calls in Inprogress </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_g"><i class="ion-ios7-compose"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="Label3" runat="server" /></span>
                                                <span class="stat_name">No. of Calls UnAttended</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>   --%>
                            </div>

                            <%--<div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_d"><i class="fa fa-money"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="TotalamountinHandlbl" runat="server" />
                                                </span>
                                                <span class="stat_name">Cash In Hand</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_d"><i class="ion-card"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="TotalCardPaymentlbl" runat="server" /> / <asp:Label ID="TotalNetpayment" runat="server"></asp:Label>

                                                </span>
                                                <span class="stat_name">Credit / Net Payment</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_e"><i class="ion-locked"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="Blockroomlbl" runat="server" /></span>
                                                <span class="stat_name">Blocked Rooms</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_d"><i class="fa fa-money"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="totalincomeamtlbl" runat="server" /></span>
                                                <span class="stat_name">Total receiving</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_d"><i class="fa fa-money"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="refundamtlbl" runat="server" /></span>
                                                <span class="stat_name">Total Refund</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="panel panel-default">
                                        <div class="stat_box">
                                            <div class="stat_ico color_g"><i class="fa fa-share-square-o"></i></div>
                                            <div class="stat_content">
                                                <span class="stat_count">
                                                    <asp:Label ID="reservedroomslbl" runat="server" /></span>
                                                <span class="stat_name">Reserved Rooms (next three days)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>
                </div>
            </div>


            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <%--        <div class="col-lg-2">
                                &nbsp;
                            </div>
                            <div class="col-lg-4">
                                <h5>
                                    <asp:DropDownList ID="ListUnitDDL" CssClass="select-full-emp form-control" runat="server" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ListUnitDDL_SelectedIndexChanged">
                                        <asp:ListItem Text="All Units" Value="0"></asp:ListItem>
                                    </asp:DropDownList></h5>
                            </div>
                            <div class="col-lg-4">
                                <h5>
                                    <asp:DropDownList ID="StatustypeDDL" AutoPostBack="true"
                                        AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server" OnSelectedIndexChanged="StatustypeDDL_SelectedIndexChanged">
                                        <asp:ListItem Text="Pending" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </h5>
                            </div>
                            <div class="col-lg-2">
                                &nbsp;
                            </div>--%>
                            <div class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table info_table" id="dt_reportTable">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Ticket No</th>
                                                <th class="sub_col">User Name / Unit Name</th>
                                                <%--<th class="sub_col">Unit Name</th>--%>
                                                <th class="sub_col">Part Name</th>
                                                <th class="sub_col">Problem Details</th>
                                                <th class="sub_col">Created Date</th>
                                                <th class="sub_col">Attended Date</th>
                                                <th class="sub_col">Attended By</th>
                                                <th class="sub_col">Closed Date</th>
                                                <th class="sub_col">Status</th>
                                                <th class="sub_col">Status Remarks</th>
                                                <th class="sub_col">View Details</th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="TicketListView" runat="server" OnItemCommand="TicketListView_ItemCommand" OnItemDataBound="TicketListView_ItemDataBound">
                                            <ItemTemplate>
                                                <tr id="RowVal" runat="server">
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                        <asp:HiddenField ID="hfticketid" runat="server" Value='<%# Eval("TicketId") %>' />
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("TicketNo")%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("UserName")%> / <%#Eval("UnitName")%>
                                                    </td>
                                                    <%--<td class="sub_col">
                                                        <%#Eval("UnitName")%>
                                                    </td>--%>
                                                    <td class="sub_col">
                                                        <%#Eval("PartName")%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("ProblemDetails")%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:Label ID="lblcreatedate" runat="server" Text='<%#:Convert.ToDateTime(Eval("CreatedDate")).ToString("dd-MMM-yyyy hh:mm tt") %>'></asp:Label>
                                                    </td>
                                                    <td class="sub_col"><%#Eval("AttendedDate")!=null? Convert.ToDateTime(Eval("AttendedDate")).ToString("dd-MMM-yyyy hh:mm tt"):"-"%></td>
                                                    <td class="sub_col"><%#Eval("AttendedBy")%></td>
                                                    <td class="sub_col">
                                                        <asp:Label ID="lblcloseddate" runat="server" Text='<%#:Eval("ClosedDate")!=null? Convert.ToDateTime(Eval("ClosedDate")).ToString("dd-MMM-yyyy hh:mm tt") : "-"%>'></asp:Label>

                                                    </td>
                                                    <td class="sub_col">
                                                        <%# Eval("Status") %> By
                                                        <asp:Label ID="durationLbl" runat="server"></asp:Label>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%# Eval("StatusRemarks") %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:LinkButton ID="linkview" runat="server" CommandName="RowView" CommandArgument='<%# Eval("TicketId") %>' ForeColor="#111">View</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <%--<asp:ListView ID="TicketListView" ItemType="HelpDesk.TicketTable" SelectMethod="getTickets" runat="server" OnItemCommand="TicketListView_ItemCommand" OnItemDataBound="TicketListView_ItemDataBound">
                                            <ItemTemplate>
                                                <tr id="RowVal" runat="server">
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                        <asp:HiddenField ID="hfticketid" runat="server" Value='<%#:Item.TicketId %>' />
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.UsersTable == null?"-": Item.UsersTable.Name%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.PartMasterId!=499?Item.PartMasterTable.PartName:Item.PartEntryText%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.ProblemId!=499?Item.ProblemId==0?"":Item.ProblemMasterTable.Description:Item.ProblemText%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:Label ID="lblcreatedate" runat="server" Text='<%#:Convert.ToDateTime(Item.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt") %>'></asp:Label>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.StatusId==0 ? "-" : Item.StatusTable.StatusName %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:LinkButton ID="linkview" runat="server" CommandName="RowView" CommandArgument='<%#:Item.TicketId %>' ForeColor="#111">View</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>--%>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
