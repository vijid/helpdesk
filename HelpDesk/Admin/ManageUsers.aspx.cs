﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.ModelBinding;

namespace HelpDesk.Admin
{
    public partial class ManageUsers : System.Web.UI.Page
    {
        int UserId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //UserId = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                UserId = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDDL();
            }
        }
        public void BindDDL()
        {
            try
            {
                var UnitDetail = db.UnitTables.Where(x => x.IsActive == true).OrderBy(x => x.UnitName).ToList();

                //UnitDDl.Items.Clear();
                UnitDDl.DataSource = UnitDetail;
                //UnitDDl.Items.Insert(0, "All Unit");
                UnitDDl.DataTextField = "UnitName";
                UnitDDl.DataValueField = "UnitId";
                UnitDDl.DataBind();

                UnitNameDDL.DataSource = UnitDetail;
                UnitNameDDL.DataValueField = "UnitId";
                UnitNameDDL.DataTextField = "UnitName";
                UnitNameDDL.DataBind();

                PrinterDDL.Items.Clear();
                var PrinterDt = db.PrinterTypeTables.Where(x => x.IsActive == true).OrderBy(x => x.PrinterName).ToList();
                PrinterDDL.DataSource = PrinterDt;
                PrinterDDL.Items.Insert(0, "Select A Printer");
                PrinterDDL.Items.Insert(1, "No Printer");
                PrinterDDL.DataValueField = "PrinterTypeId";
                PrinterDDL.DataTextField = "PrinterName";

                PrinterDDL.DataBind();
            }
            catch { }
        }
        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<UsersTable> getUsers([Control]int? UnitDDl)
        {

            if (UnitDDl == -1)
            {
                return db.UsersTables.OrderBy(x => x.Name).ToList();
            }
            else
            {
                return db.UsersTables.Where(x => x.UnitId == UnitDDl).OrderBy(x => x.Name).ToList();
            }
        }
        protected void UserListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowUpdate")
                {
                    int userid = Convert.ToInt32(e.CommandArgument);

                    var usr_det = db.UsersTables.FirstOrDefault(x => x.UserId == userid);
                    EditNameTxt.Text = usr_det.Name.ToString();
                    EditDesignationTxt.Text = usr_det.Designation.ToString();
                    EditDeptTxt.Text = usr_det.Department.ToString();
                    if (usr_det.EmailId != null)
                    {
                        EditEmailTxt.Text = usr_det.EmailId.ToString();
                    }
                    EditUserNametxt.Text = usr_det.UserName.ToString();
                    EditPasswordTxt.Text = Encryptr.Decrypt(usr_det.Password.ToString());
                    UnitNameDDL.SelectedValue = usr_det.UnitTable.UnitId.ToString();
                    PrinterDDL.SelectedValue = usr_det.PrinterTypeId == 0 ? "No Printer" : usr_det.PrinterTypeTable.PrinterTypeId.ToString();
                    EditAssetNoTxt.Text = usr_det.AssetNo.ToString();
                    EditPrinterNoTxt.Text = usr_det.PrinterAssetNo.ToString();

                    userhidden.Value = userid.ToString();

                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowEdit")
                {
                    int UserId = Convert.ToInt32(e.CommandArgument);

                    var userDtActive = db.UsersTables.FirstOrDefault(x => x.UserId == UserId);

                    if (userDtActive.IsActive.ToString() == "True")
                    {
                        userDtActive.IsActive = false;
                        db.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('User Details Deactivated Successfully!!'); });", true);
                    }
                    else
                    {
                        userDtActive.IsActive = true;
                        db.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('User Details Activated Successfully!!'); });", true);
                    }
                    UserListView.DataBind();
                    //ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
            }
            catch { }
        }

        protected void UserListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                HiddenField hfUserHdnr = (HiddenField)e.Item.FindControl("PasswordHdn");
                Label lblPassword = (Label)e.Item.FindControl("PasswordLbl");
                int Userid = Convert.ToInt32(hfUserHdnr.Value);

                string Password = db.UsersTables.FirstOrDefault(x => x.UserId == Userid).Password;
                lblPassword.Text = Encryptr.Decrypt(Password);
            }
            catch { }
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int PrinterTypeId = 0;
                int userid = Convert.ToInt32(userhidden.Value);
                var usr_det = db.UsersTables.FirstOrDefault(x => x.UserId == userid);
                usr_det.Name = EditNameTxt.Text;
                usr_det.Designation = EditDesignationTxt.Text;
                usr_det.Department = EditDeptTxt.Text;
                usr_det.Password = Encryptr.Encrypt(EditPasswordTxt.Text);
                usr_det.UnitId = Convert.ToInt32(UnitNameDDL.SelectedValue);
                if (EditEmailTxt.Text != "")
                {
                    usr_det.EmailId = EditEmailTxt.Text;
                }
                usr_det.UserName = EditUserNametxt.Text;
                usr_det.AssetNo = EditAssetNoTxt.Text;
                usr_det.PrinterAssetNo = EditPrinterNoTxt.Text;
                if (PrinterDDL.SelectedValue != "0")
                {
                    PrinterTypeId = 0;
                    if (PrinterDDL.SelectedItem.Text != "No Printer")
                    {
                        PrinterTypeId = Convert.ToInt32(PrinterDDL.SelectedValue);
                    }
                }
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                UserListView.DataBind();
                clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('User Updated Successfully!!'); });", true);
            }
            catch { }
        }
        public void clear()
        {
            EditPrinterNoTxt.Text = "";
            EditNameTxt.Text = "";
            EditDesignationTxt.Text = "";
            EditDeptTxt.Text = "";
            EditPasswordTxt.Text = "";
            EditEmailTxt.Text = "";
            PrinterDDL.SelectedIndex = 0;
            UnitNameDDL.SelectedIndex = 0;
            EditAssetNoTxt.Text = "";
            EditUserNametxt.Text = "";
        }

        protected void PrinterDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (PrinterDDL.SelectedItem.Text == "No Printer" || PrinterDDL.SelectedItem.Text == "Select A Printer")
                {
                    EditPrinterNoTxt.Enabled = false;
                }
                else
                {
                    EditPrinterNoTxt.Enabled = true;
                }
            }
            catch { }
        }
    }
}