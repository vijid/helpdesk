﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="ManageEngineer.aspx.cs" Inherits="HelpDesk.Admin.ManageEngineer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function pageLoad() {
            $(document).ready(function () {
                $('#prod_table').DataTable();
            });
        }
    </script>
    <script type="text/javascript">
        function Validation() {
            var engineername = document.getElementById('<%=EngineerNameText.ClientID%>').value;
            var ContactNo = document.getElementById('<%=ContactNotext.ClientID%>').value;
            var email = document.getElementById('<%=EmailIdTxt.ClientID%>').value;
            if (engineername == '' && ContactNo == '' && email == '') {
                alert('Kindly Enter All Fields');
                return false;
            }
            if (engineername == '') {
                alert('Kindly Enter Engineer Name');
                return false;
            }

            if (ContactNo == '') {
                alert('Kindly Enter Contact Number');
                return false;
            }
            //if (ContactNo != '') {
            //    var mob = /^[1-9]{1}[0-9]{9}$/;
            //    if (mob.test(ContactNo) == false) {
            //        alert("Please Enter Valid 10 Digit Mobile Number");
            //        return false;
            //    }
            //}
            if (email == '') {
                alert('Kindly Enter Email Id');
                return false;
            }

            var x = document.getElementById('<%=EmailIdTxt.ClientID%>').value;
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                alert("Kindly Enter a valid Email Id!!");
                return false;
            }

            if (document.getElementById('<%=UnitDDList.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Unit Name");
                return false;
            }

            return true;
        }
    </script>
    <script type="text/javascript">
        function ValidationUpdate() {
            var engineername = document.getElementById('<%=EditEnggNameText.ClientID%>').value;
            var ContactNo = document.getElementById('<%=EditContactText.ClientID%>').value;
            var email = document.getElementById('<%=EditEmailText.ClientID%>').value;
            var password = document.getElementById('<%=EditPasswordTxt.ClientID%>').value;
            if (engineername == '' && ContactNo == '' && email == '' && password == '') {
                alert('Kindly Enter All Fields');
                return false;
            }
            if (engineername == '') {
                alert('Kindly Enter Engineer Name');
                return false;
            }

            if (ContactNo == '') {
                alert('Kindly Enter Contact Number');
                return false;
            }
            //if (ContactNo != '') {
            //    var mob = /^[1-9]{1}[0-9]{9}$/;
            //    if (mob.test(ContactNo) == false) {
            //        alert("Please Enter Valid 10 Digit Mobile Number");
            //        return false;
            //    }
            //}
            if (email == '') {
                alert('Kindly Enter Email Id');
                return false;
            }

            var x = document.getElementById('<%=EditEmailText.ClientID%>').value;
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                alert("Kindly Enter a valid Email Id!!");
                return false;
            }

            if (password == '') {
                alert('Kindly Enter Password');
                return false;
            }
            if (document.getElementById('<%=EditUnitDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Unit Name");
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageEnggupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageEnggprogress" AssociatedUpdatePanelID="ManageEnggupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="page_title">Manage Engineer</h1>
                    </div>
                    <div class="col-md-2 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#bs_modal_regular">Add Engineer</a>
                    </div>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="row form-horizontal">
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">Select Unit Name</label>
                                        <div class="col-xs-4">
                                            <asp:DropDownList ID="UnitDDl" data-placeholder="Select Unit Name" AutoPostBack="true" OnSelectedIndexChanged="UnitDDl_SelectedIndexChanged"
                                                AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                <asp:ListItem Value="0">All Unit</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="table-responsive">
                                    <table class="table info_table" id="prod_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Engineer Name</th>
                                                <th class="sub_col">Email Id</th>
                                                <th class="sub_col">Unit Name</th>
                                                <th class="sub_col">Password</th>
                                                <th class="sub_col">Contact No</th>
                                                <th class="sub_col">Is Active</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="EngineerListView" runat="server"
                                            OnItemCommand="EngineerListView_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("EngineerName")%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("EmailId") %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("UnitName") %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#HelpDesk.Repositories.Encryptr.Decrypt(Eval("Password").ToString()) %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#Eval("ContactNo") %>
                                                    </td>
                                                     <td class="sub_col">
                                                        <%#Eval("IsActive").ToString()=="True"?"Active":"InActive" %>
                                                    </td>
                                                    <td class="sub_col">
                                                        <asp:LinkButton ID="lkbtnedit" runat="server" class="btn btn-inverse btn-sm" CommandName="RowEdit" CommandArgument='<%#Eval("EngineerId")%>'><span class="fa fa-pencil-square-o fa-lg"></span>Edit</asp:LinkButton>
                                                        <asp:LinkButton ID="lkbtndelete" runat="server" class="btn btn-danger btn-sm" CommandName="RowDelete" CommandArgument='<%#Eval("EngineerId")%>' OnClientClick="javascript:return confirm('Are you sure you want to delete this record?')"><span class="fa fa-pencil-square-o fa-lg"></span>Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
    <div class="modal fade" id="bs_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Add Engineer</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="AddengineerUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="AddUpdateProgress" AssociatedUpdatePanelID="AddengineerUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="EngineerName">Engineer Name</label>
                                                <asp:TextBox runat="server" ID="EngineerNameText" CssClass="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label for="Contactno">Contact No</label>
                                                <asp:TextBox runat="server" ID="ContactNotext" CssClass="form-control" onkeypress="OnlyNumericEntry()" TextMode="Number" />
                                            </div>
                                               <div class="form-group">
                                                <label for="isactive">Is Active</label>
                                                <asp:DropDownList ID="IsActiveDDl" data-placeholder="Select Type" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Text="True" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="False" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <label for="email_Id">Email Id</label>
                                                <asp:TextBox ID="EmailIdTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label for="Unit_type">Select Unit Name</label>
                                                <asp:DropDownList ID="UnitDDList" data-placeholder="Select an Unit" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Selected="True" Text="Select an Unit" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="saveBtn" CssClass="btn btn-success" runat="server" Text="Save" OnClick="saveBtn_Click" OnClientClick="return Validation()" />
                                        <asp:Button ID="cancelbutton" CssClass="btn btn-default" Text="Cancel" data-dismiss="modal" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="Edit_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <fieldset>
                                <legend><span>Edit Engineer</span></legend>
                            </fieldset>
                            <asp:UpdatePanel ID="editEngineerUpd" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="EditUpdateProgress" AssociatedUpdatePanelID="editEngineerUpd" runat="server">
                                        <ProgressTemplate>
                                            <div class="update"></div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="EngineerName">Engineer Name</label>
                                                <asp:TextBox runat="server" ID="EditEnggNameText" CssClass="form-control" />
                                            </div>
                                            <div class="form-group">
                                                <label for="Contactno">Contact No</label>
                                                <asp:TextBox runat="server" ID="EditContactText" CssClass="form-control" onkeypress="OnlyNumericEntry()" TextMode="Number" />
                                                <asp:HiddenField ID="Engineerhidden" runat="server" />
                                            </div>
                                             <div class="form-group">
                                                <label for="isactive">Is Active</label>
                                                <asp:DropDownList ID="EditIsActiveDDl" data-placeholder="Select Type" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Text="True" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="False" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <label for="email_Id">Email Id</label>
                                                <asp:TextBox ID="EditEmailText" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <asp:TextBox ID="EditPasswordTxt" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label for="Unit_type">Select Unit Name</label>
                                                <asp:DropDownList ID="EditUnitDDL" data-placeholder="Select an Unit" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                    <asp:ListItem Text="Select an Unit" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Button ID="updateBtn" CssClass="btn btn-success" runat="server" Text="Update" OnClick="updateBtn_Click" OnClientClick="return ValidationUpdate()" />
                                        <input type="reset" class="btn btn-default" value="Cancel" data-dismiss="modal" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
