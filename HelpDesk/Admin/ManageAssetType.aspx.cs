﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.Admin
{
    public partial class ManageAssetType : System.Web.UI.Page
    {
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                //Role = Session["Role"].ToString();
                if (Session["Role"].ToString() != "admin")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Admin/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Admin/Default.aspx");
            }
        }

        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<AssetTypeTable> getAssetType()
        {
            return db.AssetTypeTables.Where(x => x.IsActive == true).OrderBy(x => x.AssetTypeId).ToList();
        }

        protected void AssetTypeListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int AssetTypeId = Convert.ToInt32(e.CommandArgument);
                    var asset_det = db.AssetTypeTables.FirstOrDefault(x => x.AssetTypeId == AssetTypeId);
                    AssetTypeIdhidden.Value = asset_det.AssetTypeId.ToString();
                    EditAssetTypeText.Text = asset_det.AssetName;
                    ScriptManager.RegisterStartupScript(this, GetType(), "model", "$(function() { $('#Edit_modal_regular').modal('show'); });", true);
                }
                if (e.CommandName == "RowDelete")
                {

                    int AssetTypeId = Convert.ToInt32(e.CommandArgument);
                    int AssetTicketCnt = 0;
                    var asset_det = db.AssetTypeTables.FirstOrDefault(x => x.AssetTypeId == AssetTypeId);
                    int Asset = db.AssetTables.Where(x => x.AssetTypeId == AssetTypeId).Count();
                    var AssetDt = db.AssetTables.Where(x => x.AssetTypeId == AssetTypeId).ToList();
                    foreach (var dt in AssetDt)
                    {
                        if (db.TicketTables.Where(x => x.AssetId == dt.AssetId).Count() != 0)
                        {
                            AssetTicketCnt += 1;
                        }
                    }

                    if (AssetTicketCnt > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Sorry!! Cannot Deleted, Ticket Details Created for this Asset Type!!'); });", true);
                    }
                    else
                    {
                        //db.AssetTypeTables.Remove(asset_det);
                        asset_det.IsActive = false;
                        db.SaveChanges();
                        ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Asset Type Deleted Successfully!!'); });", true);
                    }
                    AssetTypeListView.DataBind();
                }
            }
            catch { }
        }

        public void Clear()
        {
            AssetTypeText.Text = "";
            EditAssetTypeText.Text = "";
            AssetTypeIdhidden.Value = "";
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string assettype = AssetTypeText.Text;
                AssetTypeTable asset_det = new AssetTypeTable();
                asset_det.AssetName = assettype;
                asset_det.IsActive = true;
                db.AssetTypeTables.Add(asset_det);
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                AssetTypeListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Asset Type Added Successfully!!'); });", true);
            }
            catch { }
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                int AssetTypeId = Convert.ToInt32(AssetTypeIdhidden.Value);
                string assettype = EditAssetTypeText.Text;
                var asset_det = db.AssetTypeTables.FirstOrDefault(x => x.AssetTypeId == AssetTypeId);
                asset_det.AssetName = assettype;
                db.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#Edit_modal_regular').modal('hide'); });", true);
                AssetTypeListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Asset Type Updated Successfully!!'); });", true);
            }
            catch { }
        }
    }
}