﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.User
{
    public partial class User : System.Web.UI.MasterPage
    {
        int userId = 0;        
        protected string UserName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] != null)
            {
                userId = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "User")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx");
                }
                UserName = db.UsersTables.FirstOrDefault(x => x.UserId == userId).Name;
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
        ithelpdeskEntities db = new ithelpdeskEntities();
        protected void logoutBtn_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            Response.Redirect("~/Default.aspx");
        }
    }
}