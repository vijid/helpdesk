﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.User
{
    public partial class ViewDetails : System.Web.UI.Page
    {
        int userId = 0;
        int TicketId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userId = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userId = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "User")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            BindDataValue();
        }
        ithelpdeskEntities db = new ithelpdeskEntities();

        public void BindDataValue()
        {
            try
            {
                TicketId = Convert.ToInt32(Request.QueryString["TickId"]);

                var TicketDt = db.TicketTables.FirstOrDefault(x => x.TicketId == TicketId);
                AssetNoLbl.Text = TicketDt.AssetTable.AssetNo.ToString();
                PartTypeLbl.Text = TicketDt.PartMasterId != 499 ? TicketDt.PartMasterTable.PartName : TicketDt.PartEntryText;
                ProblemTypeLbl.Text = TicketDt.ProblemId != 499 ? TicketDt.ProblemMasterTable.Description : TicketDt.ProblemText;
                EditRemarkslbl.Text = TicketDt.Remarks;
                isRecurringLbl.Text = TicketDt.IsRecurring != true ? "Yes" : "No";
                CreatedDateLbl.Text = Convert.ToDateTime(TicketDt.CreatedDate).ToString("dd-MM-yyyy hh:mm tt");
                AttendedDateLbl.Text = TicketDt.AttendedDate != null ? Convert.ToDateTime(TicketDt.AttendedDate).ToString("dd-MM-yyyy hh:mm tt") : "-";
                AttendedByLbl.Text = TicketDt.AttendedBy != 0 ? TicketDt.EngineerTable.EngineerName : "-";
            }
            catch { }
        }

        protected void BackBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/User/Dashboard.aspx", false);
            }
            catch { }
        }
    }
}