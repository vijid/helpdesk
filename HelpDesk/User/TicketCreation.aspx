﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="TicketCreation.aspx.cs" Inherits="HelpDesk.User.TicketCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function pageLoad() {
            $(document).ready(function () {
                $('#prod_table').DataTable();
            });
        }
        function Validation() {

            if (document.getElementById('<%=AssetTypeDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Asset Type Name");
                return false;
            }
            if (document.getElementById('<%=AssetNoDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Asset Number");
                return false;
            }
            if (document.getElementById('<%=PartTypeDDL.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Part Type");
                return false;
            }
            if (document.getElementById('<%=ProblemTypeDDl.ClientID%>').selectedIndex == 0) {
                alert("Kindly Select Problem Type");
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageTicketupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageTicketprogress" AssociatedUpdatePanelID="ManageTicketupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="page_title">Create Ticket</h1>
                    </div>
                    <%-- <div class="col-md-2 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#bs_modal_regular">Create Ticket</a>
                    </div>--%>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3">&nbsp;</div>
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <fieldset>
                                        <legend><span>Add Ticket</span></legend>
                                    </fieldset>
                                    <asp:UpdatePanel ID="AddTicketUpd" runat="server">
                                        <ContentTemplate>
                                            <asp:UpdateProgress ID="AddUpdateProgress" AssociatedUpdatePanelID="AddTicketUpd" runat="server">
                                                <ProgressTemplate>
                                                    <div class="update"></div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                            <div class="row">

                                                <div class="col-lg-12 form-horizontal">
                                                    <div class="form-group">
                                                        <label for="AssetType" class="col-lg-3 control-label">Asset Type</label>
                                                        <div class="col-lg-8">
                                                            <asp:DropDownList ID="AssetTypeDDL" data-placeholder="Select Asset Type" AutoPostBack="true" OnSelectedIndexChanged="AssetTypeDDL_SelectedIndexChanged"
                                                                AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                                <asp:ListItem Value="0">Select Asset Type</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="AssetNo" class="col-lg-3 control-label">Asset No</label>
                                                        <div class="col-lg-8">
                                                            <asp:DropDownList ID="AssetNoDDL" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="PartType" class="col-lg-3 control-label">Part Type</label>
                                                        <div class="col-lg-8">
                                                            <asp:DropDownList ID="PartTypeDDL" data-placeholder="Select Part Type" AutoPostBack="true" OnSelectedIndexChanged="PartTypeDDL_SelectedIndexChanged"
                                                                AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server">
                                                                <asp:ListItem Value="0">Select Part Type</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label CssClass="col-lg-3 control-label" Style="font-weight: 700; font-size: 12px" Visible="false" ID="OtherPartTypelbl" runat="server" Text="Other Part"></asp:Label>
                                                        <div class="col-lg-8">
                                                            <asp:TextBox ID="OtherPartTypeTxt" Visible="false" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="problemType" class="col-lg-3 control-label">Problem Type</label>
                                                        <div class="col-lg-8">
                                                            <asp:DropDownList Enabled="false" ID="ProblemTypeDDl" data-placeholder="Select Problem Type" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control chosen-select" runat="server" OnSelectedIndexChanged="ProblemTypeDDl_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label CssClass="col-lg-3 control-label" Style="font-weight: 700; font-size: 12px" Visible="false" ID="OtherProblemlbl" runat="server" Text="Other Problem"></asp:Label>
                                                        <div class="col-lg-8">
                                                            <asp:TextBox ID="OtherProblemTxt" Visible="false" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Remarks" class="col-lg-3 control-label">Remarks</label>
                                                        <div class="col-lg-8">
                                                            <asp:TextBox ID="RemarksTxt" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-3"></div>
                                                        <div class="col-lg-8">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" runat="server" value="Is Recurring" name="isRecurrChk" id="isRecurrChk" />
                                                                    Is Recurring
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group pull-right">
                                                <asp:Button ID="saveBtn" CssClass="btn btn-success" runat="server" Text="Save" OnClick="saveBtn_Click" OnClientClick="return Validation()" />
                                                <asp:Button ID="cancelbutton" CssClass="btn btn-default" Text="Clear" OnClick="cancelbutton_Click" runat="server"  />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <%--<div class="table-responsive">
                                    <table class="table info_table" id="prod_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th class="sub_col">Ticket No</th>
                                                <th class="sub_col">Part Name</th>
                                                <th class="sub_col">Asset No</th>
                                                <th class="sub_col">Problem Details</th>
                                                <th class="sub_col">Remarks</th>
                                                <th class="sub_col">Status</th>
                                                <th class="sub_col">Created Date</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="TicketListView" ItemType="HelpDesk.TicketTable" SelectMethod="getTicket" runat="server" OnItemCommand="TicketListView_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%# Container.DataItemIndex + 1%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.TicketNo%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.PartMasterId!=0?Item.PartMasterTable.PartName:Item.PartEntryText%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.AssetId!=0? Item.AssetTable.AssetNo: "-"%>
                                                    </td>

                                                    <td class="sub_col">
                                                        <%#:Item.ProblemId!=0?Item.ProblemMasterTable.Description:Item.ProblemText%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.Remarks%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Item.StatusId!=0?Item.StatusTable.StatusName:"-"%>
                                                    </td>
                                                    <td class="sub_col">
                                                        <%#:Convert.ToDateTime(Item.CreatedDate).ToString("dd-MMM-yyyy hh:mm:ss tt") %>
                                                    </td>
                                                    <td class="sub_col">                                                      
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
    <div class="modal fade" id="bs_modal_regular">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="panel panel-default">
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
