﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.User
{
    public partial class DashBoard : System.Web.UI.Page
    {
        int userId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userId = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userId = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "User")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
        ithelpdeskEntities db = new ithelpdeskEntities();

        public IEnumerable<TicketTable> getTicket()
        {
            return db.TicketTables.Where(x => x.UserId == userId && x.AttendedBy == 0).OrderByDescending(x => x.TicketId).ToList();
        }

        protected void TicketListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "RowEdit")
                {
                    int TicketID = Convert.ToInt32(e.CommandArgument);

                    Response.Redirect("~/User/ViewDetails.aspx?TickId=" + TicketID + "", false);
                }
            }
            catch { }
        }

        protected void TicketListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                HiddenField ticketid_hf = (HiddenField)e.Item.FindControl("hfticketid");
                Label StatusRemarksLbl = (Label)e.Item.FindControl("lblStatusRemark");
                Int32 ticketid = Convert.ToInt32(ticketid_hf.Value);
                var ticket_det = db.TicketTables.SingleOrDefault(x => x.TicketId == ticketid);
                if (ticket_det != null)
                {
                    StatusRemarksLbl.Text = ticket_det.StatusId == 0 ? ticket_det.ClosedDate == null ? "Pending" : "Closed" : ticket_det.TicketProcessTables.FirstOrDefault(x => x.TicketId == ticket_det.TicketId && x.StatusId == ticket_det.StatusId).Remarks;
                }
                Label createdate_lbl = (Label)e.Item.FindControl("lblcreatedate");
                //Label closeddate_lbl = (Label)e.Item.FindControl("lblcloseddate");
                Label Duration_Label = (Label)e.Item.FindControl("durationLbl");
                DateTime startTime = Convert.ToDateTime(createdate_lbl.Text);
                DateTime endTime = DateTime.Now;
                //if (closeddate_lbl.Text != "-")
                //{
                //    endTime = Convert.ToDateTime(closeddate_lbl.Text);
                //}

                TimeSpan span = endTime.Subtract(startTime);
                //string year = "";
                //string month = "";
                string days = "";
                string hours = "";
                string mins = "";
                string sec = "";

                if (span.Days > 0)
                {
                    days = span.Days.ToString() + " Days ";
                }
                if (span.Hours > 0)
                {
                    hours = span.Hours.ToString() + " Hours ";
                }
                if (span.Minutes > 0)
                {
                    mins = span.Minutes.ToString() + " Minutes ";
                }
                if (span.Seconds > 0)
                {
                    sec = span.Seconds.ToString() + " Seconds ";
                }
                Duration_Label.Text = days + hours + mins + sec;
            }
            catch { }
        }

    }
}