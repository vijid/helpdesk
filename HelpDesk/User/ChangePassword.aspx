﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="HelpDesk.User.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function Validation() {
            var currentpassword = document.getElementById('<%= txtcurrentpswd.ClientID%>').value;
            var newpassword = document.getElementById('<%= txtnewpswd.ClientID%>').value;
            var confimpassword = document.getElementById('<%= txtconfirmpswd.ClientID%>').value;

            if (currentpassword == null || currentpassword == "" || newpassword == null || newpassword == "" || confimpassword == null || confimpassword == "") {
                alert('Kindly fill all the details');
                return false;
            }

            if (newpassword != confimpassword) {
                alert('There is a mismatch between your New Password and Confirm Password.');
                return false;
            }
            return true;
        }
    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page_bar clearfix">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page_title">Change Password</h1>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="col-lg-offset-3 col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <asp:UpdatePanel ID="addadminprofileUpd" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="addadminprofileUpd">
                                    <ProgressTemplate>
                                        <div class="update"></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <div class="form-group">
                                    <label>Current Password</label>
                                    <asp:TextBox ID="txtcurrentpswd" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <asp:TextBox ID="txtnewpswd" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <asp:TextBox ID="txtconfirmpswd" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <div class="form-actions text-right">
                                        <asp:Button ID="updateBtn" runat="server" CssClass="btn btn-warning btn_radius" OnClientClick="return Validation()" Text="Update" OnClick="updateBtn_Click" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
