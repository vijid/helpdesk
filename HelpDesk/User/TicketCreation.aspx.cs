﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.User
{
    public partial class TicketCreation : System.Web.UI.Page
    {
        int userId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userId = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userId = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "User")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
            if (!IsPostBack)
            {
                BindDDL();
            }
        }
        ithelpdeskEntities db = new ithelpdeskEntities();
        public void BindDDL()
        {
            try
            {
                var AssetType = db.AssetTypeTables.Where(x => x.IsActive == true).OrderBy(x => x.AssetName).ToList();
                AssetTypeDDL.DataSource = AssetType;
                AssetTypeDDL.DataValueField = "AssetTypeId";
                AssetTypeDDL.DataTextField = "AssetName";
                AssetTypeDDL.DataBind();

                string assetdt = db.UsersTables.FirstOrDefault(x => x.UserId == userId && x.IsActive == true).AssetNo;
                int AssetTypeId = 0;
                var assetTypeNo = db.AssetTables.FirstOrDefault(x => x.AssetNo == assetdt);
                if (assetTypeNo != null)
                {
                    AssetTypeId = Convert.ToInt32(assetTypeNo.AssetTypeId);
                }
                AssetTypeDDL.SelectedValue = AssetTypeId.ToString();
                BindAsset(AssetTypeId);
                var PartDt = db.PartMasterTables.Where(x => x.IsActive == true).OrderBy(x => x.PartName).ToList();
                PartTypeDDL.Items.Insert(1, new ListItem("Others", "499"));
                PartTypeDDL.DataSource = PartDt;
                PartTypeDDL.DataValueField = "PartMasterId";
                PartTypeDDL.DataTextField = "PartName";
                PartTypeDDL.DataBind();
            }
            catch { }
        }

        public void BindAsset(int AssetTypeIds)
        {
            try
            {

                AssetNoDDL.Items.Clear();
                AssetNoDDL.Items.Insert(0, "Select Asset No");
                string assetdt = db.UsersTables.FirstOrDefault(x => x.UserId == userId && x.IsActive == true).AssetNo;
                string PrinterAssetNo = db.UsersTables.FirstOrDefault(x => x.UserId == userId && x.IsActive == true).PrinterAssetNo;
                if (assetdt != "")
                {
                    //AssetNoDDL.SelectedItem.Text = assetdt;
                    try
                    {
                        var AssetNoDt = db.AssetTables.Where(x => x.AssetTypeId == AssetTypeIds && x.IsActive == true && x.AssetNo == assetdt).ToList();

                        AssetNoDDL.DataSource = AssetNoDt;
                        //AssetNoDDL.Items.Insert(0, "Select Asset No");
                        AssetNoDDL.DataValueField = "AssetId";
                        AssetNoDDL.DataTextField = "AssetNo";
                        AssetNoDDL.DataBind();
                        AssetNoDDL.Items.FindByText(assetdt.ToString()).Selected = true;
                    }
                    catch { }
                }
                if (PrinterAssetNo != "")
                {
                    try
                    {
                       
                        var AssetNoDt = db.AssetTables.Where(x => x.AssetTypeId == AssetTypeIds && x.IsActive == true && x.AssetNo == PrinterAssetNo).ToList();

                        AssetNoDDL.DataSource = AssetNoDt;
                        //AssetNoDDL.Items.Insert(0, "Select Asset No");
                        AssetNoDDL.DataValueField = "AssetId";
                        AssetNoDDL.DataTextField = "AssetNo";
                        AssetNoDDL.DataBind();
                        AssetNoDDL.Items.FindByText(PrinterAssetNo.ToString()).Selected = true;
                    }
                    catch { }
                }

            }
            catch { }
        }
        public IEnumerable<TicketTable> getTicket()
        {
            return db.TicketTables.Where(x => x.UserId == userId).OrderByDescending(x => x.TicketId).ToList();
        }
        protected void TicketListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {

            }
            catch { }
        }

        protected void AssetTypeDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int AssetTypeId = Convert.ToInt32(AssetTypeDDL.SelectedValue);
                BindAsset(AssetTypeId);
                //AssetNoDDL.Items.Clear();
                //var AssetNoDt = db.AssetTables.Where(x => x.AssetTypeId == AssetTypeId && x.IsActive == true).OrderBy(x => x.AssetNo).ToList();

                //AssetNoDDL.DataSource = AssetNoDt;
                //AssetNoDDL.Items.Insert(0, "Select Asset No");
                //AssetNoDDL.DataValueField = "AssetId";
                //AssetNoDDL.DataTextField = "AssetNo";
                //AssetNoDDL.DataBind();
            }
            catch { }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                bool recurFlag = false;
                int TicketCnt = 0, PartTypeIDVal = 499, ProblemIdVal = 499;
                string PartTypestr = null, ProblemTypeStr = null;
                TicketCnt = db.TicketTables.OrderBy(x => x.TicketId).ToList().Count();

                TicketCnt += 1;

                TicketTable TcktTab = new TicketTable();
                TcktTab.TicketNo = TicketCnt.ToString();
                TcktTab.UserId = userId;
                TcktTab.AssetId = Convert.ToInt32(AssetNoDDL.SelectedValue);
                if (OtherPartTypeTxt.Text != "")
                {
                    PartTypestr = OtherPartTypeTxt.Text;
                }
                else
                {
                    PartTypeIDVal = Convert.ToInt32(PartTypeDDL.SelectedValue);
                }
                TcktTab.PartMasterId = PartTypeIDVal;
                TcktTab.PartEntryText = PartTypestr;
                if (OtherProblemTxt.Text != "")
                {
                    ProblemTypeStr = OtherProblemTxt.Text;
                }
                else
                {
                    ProblemIdVal = Convert.ToInt32(ProblemTypeDDl.SelectedValue);
                }
                TcktTab.ProblemId = ProblemIdVal;
                TcktTab.ProblemText = ProblemTypeStr;
                TcktTab.CreatedDate = DateTime.Now;
                TcktTab.Remarks = RemarksTxt.Text;

                if (isRecurrChk.Checked)
                {
                    recurFlag = true;
                }
                TcktTab.IsClosed = false;
                TcktTab.IsRecurring = recurFlag;
                db.TicketTables.Add(TcktTab);
                db.SaveChanges();
                //ScriptManager.RegisterStartupScript(this, GetType(), "model5", "$(function() { $('#bs_modal_regular').modal('hide'); });", true);
                //TicketListView.DataBind();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Ticket Created Successfully!!'); });", true);
            }
            catch { }
        }
        public void Clear()
        {
            AssetTypeDDL.SelectedIndex = 0;
            AssetNoDDL.SelectedIndex = 0;
            AssetNoDDL.Items.Clear();
            AssetNoDDL.Items.Insert(0, "Select Asset No");
            PartTypeDDL.SelectedIndex = 0;
            ProblemTypeDDl.SelectedIndex = 0;
            RemarksTxt.Text = "";
            OtherPartTypelbl.Visible = false;
            OtherPartTypeTxt.Visible = false;
            OtherProblemlbl.Visible = false;
            OtherProblemTxt.Visible = false;

            OtherPartTypeTxt.Text = "";
            OtherProblemTxt.Text = "";
            isRecurrChk.Checked = false;
        }

        protected void PartTypeDDL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int PartId = Convert.ToInt32(PartTypeDDL.SelectedValue);
                ProblemTypeDDl.Enabled = true;
                ProblemTypeDDl.Items.Clear();
                ProblemTypeDDl.Items.Insert(0, new ListItem("Select Problem Type", "0"));
                if (PartId != 0)
                {
                    if (PartId == 499)
                    {
                        ProblemTypeDDl.Items.Insert(1, new ListItem("Others", "499"));
                        ProblemTypeDDl.DataBind();
                        OtherPartTypelbl.Visible = true;
                        OtherPartTypeTxt.Visible = true;
                    }
                    else
                    {
                        var ProblemDt = db.ProblemMasterTables.Where(x => x.IsActive == true && x.PartMasterId == PartId).OrderBy(x => x.Description).ToList();
                        ProblemTypeDDl.DataSource = ProblemDt;
                        ProblemTypeDDl.Items.Insert(1, new ListItem("Others", "499"));
                        ProblemTypeDDl.DataValueField = "ProblemId";
                        ProblemTypeDDl.DataTextField = "Description";
                        ProblemTypeDDl.DataBind();

                        OtherPartTypelbl.Visible = false;
                        OtherPartTypeTxt.Visible = false;
                    }
                }
                else
                {
                    ProblemTypeDDl.Enabled = false;
                    OtherPartTypelbl.Visible = false;
                    OtherPartTypeTxt.Visible = false;
                    OtherProblemlbl.Visible = false;
                    OtherProblemTxt.Visible = false;
                }

            }
            catch { }
        }

        protected void ProblemTypeDDl_SelectedIndexChanged(object sender, EventArgs e)
        {
            int PartId = Convert.ToInt32(ProblemTypeDDl.SelectedValue);

            if (PartId == 499)
            {
                OtherProblemlbl.Visible = true;
                OtherProblemTxt.Visible = true;
            }
            else
            {
                OtherProblemlbl.Visible = false;
                OtherProblemTxt.Visible = false;
            }
        }

        protected void cancelbutton_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch
            {

            }
        }
    }
}