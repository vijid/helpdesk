﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.User
{
    public partial class ViewTicketDetails : System.Web.UI.Page
    {
        int userId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userId = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userId = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "User")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }

            if (!IsPostBack)
            {
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                BindListData(fromdateTimeTxt.Text, ToDateTimeTxt.Text);
            }
        }
        public class TicketDetails
        {
            public int TicketId { get; set; }
            public int TicketNo { get; set; }
            public string PartName { get; set; }
            public string AssetNo { get; set; }
            public string ProblemDetails { get; set; }
            public string Remarks { get; set; }
            public string Status { get; set; }
            public string StatusRemarks { get; set; }
            public string CreatedDate { get; set; }
            public string AttendedDate { get; set; }
            public string AttendedBy { get; set; }
            public string ClosedDate { get; set; }
        }

        public List<TicketDetails> TicketLst = new List<TicketDetails>();
        protected void SearchButton_Click(object sender, EventArgs e)
        {
            try
            {
                BindListData(fromdateTimeTxt.Text, ToDateTimeTxt.Text);
            }
            catch
            {

            }
        }

        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                fromdateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                ToDateTimeTxt.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                TicketLst.Clear();
                ViewTicketListView.DataSource = TicketLst;
                ViewTicketListView.DataBind();
            }
            catch { }
        }

        protected void ViewTicketListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                Label createdate_lbl = (Label)e.Item.FindControl("lblcreatedate");
                Label closeddate_lbl = (Label)e.Item.FindControl("lblcloseddate");
                Label Duration_Label = (Label)e.Item.FindControl("durationLbl");
                DateTime startTime = Convert.ToDateTime(createdate_lbl.Text);
                DateTime endTime = DateTime.Now;
                if (closeddate_lbl.Text != "-")
                {
                    endTime = Convert.ToDateTime(closeddate_lbl.Text);
                }

                TimeSpan span = endTime.Subtract(startTime);
                //string year = "";
                //string month = "";
                string days = "";
                string hours = "";
                string mins = "";
                string sec = "";

                if (span.Days > 0)
                {
                    days = span.Days.ToString() + " Days ";
                }
                if (span.Hours > 0)
                {
                    hours = span.Hours.ToString() + " Hours ";
                }
                if (span.Minutes > 0)
                {
                    mins = span.Minutes.ToString() + " Minutes ";
                }
                if (span.Seconds > 0)
                {
                    sec = span.Seconds.ToString() + " Seconds ";
                }
                Duration_Label.Text = days + hours + mins + sec;
            }
            catch { }
        }
        ithelpdeskEntities db = new ithelpdeskEntities();
        public void BindListData(string FromDate, string Todate)
        {
            try
            {
                TicketLst.Clear();
                string FromDateValstr = DateTime.ParseExact(FromDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                string ToDateValstr = DateTime.ParseExact(Todate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).ToString();
                DateTime FromDateVal = Convert.ToDateTime(FromDateValstr);
                DateTime ToDateVal = Convert.ToDateTime(ToDateValstr);

                if (TickettypeDDL.SelectedValue == "0")
                {
                    var AllTicketDetails = (from tck in db.TicketTables
                                            where
                                                tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal && tck.UserId == userId
                                            select
                                                new
                                                {
                                                    ticketId = tck.TicketId,
                                                    TicketNo = tck.TicketNo,
                                                    PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                                    AssetNo = tck.AssetTable.AssetNo,
                                                    ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                                    Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                                    Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                                    StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                                    CreatedDate = tck.CreatedDate,
                                                    AttendedDate = tck.AttendedDate,
                                                    //AttendedBy = tck.AttendedBy != 0 ? tck.EngineerTable.EngineerName : "-",
                                                    AttendedBy = tck.AttendedBy != 0 ? (tck.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName) : "-",
                                                    ClosedDate = tck.ClosedDate
                                                }).ToList();
                    if (AllTicketDetails.Count() != 0)
                    {
                        foreach (var dt in AllTicketDetails)
                        {
                            TicketLst.Add(new TicketDetails()
                            {
                                TicketId = Convert.ToInt32(dt.ticketId),
                                TicketNo = Convert.ToInt32(dt.TicketNo),
                                PartName = dt.PartName,
                                AssetNo = dt.AssetNo,
                                ProblemDetails = dt.ProblemDetails,
                                Remarks = dt.Remarks,
                                StatusRemarks = dt.StatusRemarks,
                                Status = dt.Status,
                                CreatedDate = Convert.ToDateTime(dt.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt"),
                                AttendedDate = dt.AttendedDate != null ? Convert.ToDateTime(dt.AttendedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-",
                                AttendedBy = dt.AttendedBy,
                                ClosedDate = dt.ClosedDate != null ? Convert.ToDateTime(dt.ClosedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-"
                            });
                        }
                    }
                }
                else if (TickettypeDDL.SelectedValue == "1")
                {
                    var OpenTicketDetails = (from tck in db.TicketTables
                                             where
                                                 tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal && tck.AttendedBy == 0 && tck.UserId == userId
                                             select
                                                 new
                                                 {
                                                     ticketId = tck.TicketId,
                                                     TicketNo = tck.TicketNo,
                                                     PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                                     AssetNo = tck.AssetTable.AssetNo,
                                                     ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                                     Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                                     Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                                     StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                                     CreatedDate = tck.CreatedDate,
                                                     AttendedDate = tck.AttendedDate,
                                                     AttendedBy = tck.AttendedBy != 0 ? (tck.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName) : "-",
                                                     ClosedDate = tck.ClosedDate
                                                 }).ToList();
                    if (OpenTicketDetails.Count() != 0)
                    {
                        foreach (var dt in OpenTicketDetails)
                        {
                            TicketLst.Add(new TicketDetails()
                            {
                                TicketId = Convert.ToInt32(dt.ticketId),
                                TicketNo = Convert.ToInt32(dt.TicketNo),
                                PartName = dt.PartName,
                                AssetNo = dt.AssetNo,
                                ProblemDetails = dt.ProblemDetails,
                                Remarks = dt.Remarks,
                                Status = dt.Status,
                                StatusRemarks = dt.StatusRemarks,
                                CreatedDate = Convert.ToDateTime(dt.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt"),
                                AttendedDate = dt.AttendedDate != null ? Convert.ToDateTime(dt.AttendedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-",
                                AttendedBy = dt.AttendedBy,
                                ClosedDate = dt.ClosedDate != null ? Convert.ToDateTime(dt.ClosedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-"
                            });
                        }
                    }
                }
                else if (TickettypeDDL.SelectedValue == "2")
                {

                    var ClosedTicketDetails = (from tck in db.TicketTables
                                               where
                                                   tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal && tck.IsClosed == true && tck.UserId == userId
                                               select
                                                   new
                                                   {
                                                       ticketId = tck.TicketId,
                                                       TicketNo = tck.TicketNo,
                                                       PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                                       AssetNo = tck.AssetTable.AssetNo,
                                                       ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                                       Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                                       Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                                       StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                                       CreatedDate = tck.CreatedDate,
                                                       AttendedDate = tck.AttendedDate,
                                                       AttendedBy = tck.AttendedBy != 0 ? (tck.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName) : "-",
                                                       ClosedDate = tck.ClosedDate
                                                   }).ToList();
                    if (ClosedTicketDetails.Count() != 0)
                    {
                        foreach (var dt in ClosedTicketDetails)
                        {
                            TicketLst.Add(new TicketDetails()
                            {
                                TicketId = Convert.ToInt32(dt.ticketId),
                                TicketNo = Convert.ToInt32(dt.TicketNo),
                                PartName = dt.PartName,
                                AssetNo = dt.AssetNo,
                                ProblemDetails = dt.ProblemDetails,
                                Remarks = dt.Remarks,
                                Status = dt.Status,
                                StatusRemarks = dt.StatusRemarks,
                                CreatedDate = Convert.ToDateTime(dt.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt"),
                                AttendedDate = dt.AttendedDate != null ? Convert.ToDateTime(dt.AttendedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-",
                                AttendedBy = dt.AttendedBy,
                                ClosedDate = dt.ClosedDate != null ? Convert.ToDateTime(dt.ClosedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-"
                            });
                        }
                    }
                }
                else
                {
                    var PendingTicketDetails = (from tck in db.TicketTables
                                                where
                                                    tck.CreatedDate >= FromDateVal && tck.CreatedDate <= ToDateVal && tck.IsClosed == false && tck.AttendedBy != 0 && tck.UserId == userId
                                                select
                                                    new
                                                    {
                                                        ticketId = tck.TicketId,
                                                        TicketNo = tck.TicketNo,
                                                        PartName = tck.PartMasterId != 499 ? tck.PartMasterTable.PartName : tck.PartEntryText,
                                                        AssetNo = tck.AssetTable.AssetNo,
                                                        ProblemDetails = tck.ProblemId != 499 ? tck.ProblemMasterTable.Description : tck.ProblemText,
                                                        Remarks = tck.Remarks != null ? tck.Remarks : "-",
                                                        Status = tck.StatusId != 0 ? tck.StatusTable.StatusName : tck.ClosedDate == null ? "Pending" : "Closed",
                                                        StatusRemarks = tck.StatusId == 0 ? tck.ClosedDate == null ? "Pending" : "Closed" : tck.TicketProcessTables.FirstOrDefault(x => x.TicketId == tck.TicketId && x.StatusId == tck.StatusId).Remarks,
                                                        CreatedDate = tck.CreatedDate,
                                                        AttendedDate = tck.AttendedDate,
                                                        AttendedBy = tck.AttendedBy != 0 ? (tck.AttendedBy == 1 ? db.AdminTables.FirstOrDefault().AdminName : db.EngineerTables.FirstOrDefault(x => x.EngineerId == tck.AttendedBy).EngineerName) : "-",
                                                        ClosedDate = tck.ClosedDate
                                                    }).ToList();
                    if (PendingTicketDetails.Count() != 0)
                    {
                        foreach (var dt in PendingTicketDetails)
                        {
                            TicketLst.Add(new TicketDetails()
                            {
                                TicketId = Convert.ToInt32(dt.ticketId),
                                TicketNo = Convert.ToInt32(dt.TicketNo),
                                PartName = dt.PartName,
                                AssetNo = dt.AssetNo,
                                ProblemDetails = dt.ProblemDetails,
                                Remarks = dt.Remarks,
                                Status = dt.Status,
                                StatusRemarks = dt.StatusRemarks,
                                CreatedDate = Convert.ToDateTime(dt.CreatedDate).ToString("dd-MMM-yyyy hh:mm tt"),
                                AttendedDate = dt.AttendedDate != null ? Convert.ToDateTime(dt.AttendedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-",
                                AttendedBy = dt.AttendedBy,
                                ClosedDate = dt.ClosedDate != null ? Convert.ToDateTime(dt.ClosedDate).ToString("dd-MMM-yyyy hh:mm tt") : "-"
                            });
                        }
                    }
                }
                ViewTicketListView.DataSource = TicketLst;
                ViewTicketListView.DataBind();
            }
            catch { }
        }
    }
}