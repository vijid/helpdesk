﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/User.Master" AutoEventWireup="true" CodeBehind="ViewDetails.aspx.cs" Inherits="HelpDesk.User.ViewDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="ManageTicketupdate" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="ManageTicketprogress" AssociatedUpdatePanelID="ManageTicketupdate" runat="server">
                <ProgressTemplate>
                    <div class="update"></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div class="page_bar clearfix">
                <div class="row">
                    <div class="col-md-10">
                        <h1 class="page_title">Create Ticket</h1>
                    </div>
                    <%-- <div class="col-md-2 text-right">
                        <a class="btn btn-success" data-toggle="modal" data-target="#bs_modal_regular">Create Ticket</a>
                    </div>--%>
                </div>
            </div>
            <div class="page_content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <fieldset>
                                        <legend><span>View Details</span></legend>
                                    </fieldset>
                                    <asp:UpdatePanel ID="editTicketUpd" runat="server">
                                        <ContentTemplate>
                                            <asp:UpdateProgress ID="EditUpdateProgress" AssociatedUpdatePanelID="editTicketUpd" runat="server">
                                                <ProgressTemplate>
                                                    <div class="update"></div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="AssetNo">Asset No</label>
                                                        <asp:Label ID="AssetNoLbl" runat="server" CssClass="form-control"></asp:Label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="PartType">Part Type</label>
                                                        <asp:Label ID="PartTypeLbl" runat="server" CssClass="form-control"></asp:Label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Recurring">ReSubmitted</label>
                                                        <asp:Label ID="isRecurringLbl" runat="server" CssClass="form-control"></asp:Label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="CreatedType">Created Date</label>
                                                        <asp:Label ID="CreatedDateLbl" runat="server" CssClass="form-control"></asp:Label>
                                                    </div>

                                                </div>
                                                <div class="col-lg-6">

                                                    <div class="form-group">
                                                        <label for="problemType">Problem Type</label>
                                                        <asp:Label ID="ProblemTypeLbl" runat="server" CssClass="form-control"></asp:Label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Remarks">Remarks</label>
                                                        <asp:Label ID="EditRemarkslbl" runat="server" aria-multiline="true" CssClass="form-control"></asp:Label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="AttendedBy">Attended By</label>
                                                        <asp:Label ID="AttendedByLbl" runat="server" CssClass="form-control"></asp:Label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="AttendedDate">Attended Date</label>
                                                        <asp:Label ID="AttendedDateLbl" runat="server" CssClass="form-control"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Button ID="BackBtn" CssClass="btn btn-success" runat="server" Text="Go Back" OnClick="BackBtn_Click" />
                                                <%--<input type="reset" class="btn btn-success" value="Ok" data-dismiss="modal" />--%>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="BottomContentPlaceHolder" runat="server">
</asp:Content>
