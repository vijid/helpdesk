﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.User
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        ithelpdeskEntities db = new ithelpdeskEntities();
        int userid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //userId = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "User")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string currentpassword = txtcurrentpswd.Text;
                string newpassword = txtnewpswd.Text;
                string confirmpassword = txtconfirmpswd.Text;

                UsersTable user = db.UsersTables.FirstOrDefault(x => x.UserId == userid);

                if (Encryptr.Decrypt(user.Password) == currentpassword)
                {
                    user.Password = Encryptr.Encrypt(newpassword);
                    db.SaveChanges();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Your new password changed successfully.'); });", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('Please kindly enter the correct current password.'); });", true);
                }
            }
            catch { }
        }
    }
}