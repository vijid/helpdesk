﻿using HelpDesk.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpDesk.User
{
    public partial class MyProfile : System.Web.UI.Page
    {
        ithelpdeskEntities db = new ithelpdeskEntities();

        int userid = 0;
        string name, designation, department, email = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //userid = Convert.ToInt32(Session["UserId"].ToString());
            if (Session["UserId"] != null)
            {
                userid = Convert.ToInt32(Session["UserId"].ToString());
                if (Session["Role"].ToString() != "User")
                {
                    Session.Clear();
                    Session.Abandon();
                    Response.Redirect("~/Default.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }

            if (!IsPostBack)
            {
                getuserprofile();
            }
        }

        public void getuserprofile()
        {
            var user_det = db.UsersTables.FirstOrDefault(x => x.UserId == userid);
            txtname.Text = user_det.Name.ToString();
            txtDesignation.Text = user_det.Designation.ToString();
            txtDepartment.Text = user_det.Department.ToString();
            txtEmailId.Text = user_det.EmailId.ToString();
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                name = txtname.Text;
                designation = txtDesignation.Text;
                department = txtDepartment.Text;
                email = txtEmailId.Text;
                var user_del = db.UsersTables.FirstOrDefault(x => x.UserId == userid);
                user_del.Name = name;
                user_del.Designation = designation;
                user_del.Department = department;
                user_del.EmailId = email;
                db.SaveChanges();
                Clear();
                ScriptManager.RegisterStartupScript(this, GetType(), "modal", "$(function() { alert('My Profile Updated Successfully!!'); });", true);
                getuserprofile();
            }
            catch { }
        }

        public void Clear()
        {
            txtname.Text = "";
            txtDesignation.Text = "";
            txtDepartment.Text = "";
            txtEmailId.Text = "";
        }
    }
}